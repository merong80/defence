﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GPManager : Singleton<GPManager>
{
	public bool isLogin = false;
	protected override void Awake()
	{
		base.Awake ();
		DontDestroyOnLoad (this.gameObject);
	}
	void Start() {

		//listen for GooglePlayConnection events
		GooglePlayConnection.ActionPlayerConnected +=  OnPlayerConnected;
		GooglePlayConnection.ActionPlayerDisconnected += OnPlayerDisconnected;

		GooglePlayConnection.ActionConnectionResultReceived += OnConnectionResult;

		GooglePlaySavedGamesManager.ActionGameSaveLoaded += ActionGameSaveLoaded;
		GooglePlaySavedGamesManager.ActionConflict += ActionConflict;

        //achievement and leaderboard
        GooglePlayManager.ActionAchievementUpdated += OnAchievementUpdated;
        GooglePlayManager.ActionScoreSubmited += OnScoreSubmited;
        GooglePlayManager.ActionScoresListLoaded += OnScoreUpdated;
    }

    protected override void OnDestroy()
    {
        GooglePlayConnection.ActionPlayerConnected -= OnPlayerConnected;
        GooglePlayConnection.ActionPlayerDisconnected -= OnPlayerDisconnected;

        GooglePlayConnection.ActionConnectionResultReceived -= OnConnectionResult;

        GooglePlaySavedGamesManager.ActionGameSaveLoaded -= ActionGameSaveLoaded;
        GooglePlaySavedGamesManager.ActionConflict -= ActionConflict;

        if (!GooglePlayManager.IsDestroyed)
        {

            GooglePlayManager.ActionAchievementUpdated -= OnAchievementUpdated;
            GooglePlayManager.ActionScoreSubmited -= OnScoreSubmited;
            GooglePlayManager.ActionScoresListLoaded -= OnScoreUpdated;
        }
    }
	//--------------------------------------
	// PUBLIC METHODS
	//--------------------------------------
	public void Connect()
	{
		if (GooglePlayConnection.State != GPConnectionState.STATE_CONNECTED) {
			GooglePlayConnection.Instance.Connect ();
		}
	}

	public void CreateNewSnapshot() {
		if (GooglePlayConnection.State != GPConnectionState.STATE_CONNECTED) {
			PopupManager.Instance.Message (TextManager.Get (TextKey.connection_failed));
			return;
		}
		PopupManager.Instance.Loading ();
		StartCoroutine(MakeScreenshotAndSaveGameData());
	}

	public void LoadGame()
	{
		if (PlayerPrefs.HasKey (PrefsKey.PLAYERDATA)) {
			GameManager.Instance.SetPlayerData (SaveManager.Instance.LoadData ());
			return;
		}
		GooglePlaySavedGamesManager.ActionAvailableGameSavesLoaded += LoadedGames;
		GooglePlaySavedGamesManager.Instance.LoadAvailableSavedGames();
	}

	private void LoadedGames(GooglePlayResult res)
	{
		GooglePlaySavedGamesManager.ActionAvailableGameSavesLoaded -= LoadedGames;
		if (res.IsSucceeded) {
			if (GooglePlaySavedGamesManager.Instance.AvailableGameSaves.Count > 0) {
				GP_SnapshotMeta s = GooglePlaySavedGamesManager.Instance.AvailableGameSaves [0];
				GooglePlaySavedGamesManager.Instance.LoadSpanshotByName (s.Title);
			} else {
				//PopupManager.Instance.Message (TextManager.Get(TextKey.no_saved_data));
				GameManager.Instance.SetPlayerData(new PlayerData(GameManager.PlayerID));
			}
		} else {
			//PopupManager.Instance.Message (TextManager.Get(TextKey.connection_failed));
			GameManager.Instance.SetPlayerData(new PlayerData(GameManager.PlayerID));
		}

	}

	//--------------------------------------
	// EVENTS
	//--------------------------------------

	private void ActionGameSaveLoaded (GP_SpanshotLoadResult result) {

		Debug.Log("ActionGameSaveLoaded: " + result.Message);
		if (result.IsSucceeded) {

			Debug.Log ("Snapshot.Title: " + result.Snapshot.meta.Title);
			Debug.Log ("Snapshot.Description: " + result.Snapshot.meta.Description);
			Debug.Log ("Snapshot.CoverImageUrl): " + result.Snapshot.meta.CoverImageUrl);
			Debug.Log ("Snapshot.LastModifiedTimestamp: " + result.Snapshot.meta.LastModifiedTimestamp);

			Debug.Log ("Snapshot.stringData: " + result.Snapshot.stringData);
			Debug.Log ("Snapshot.bytes.Length: " + result.Snapshot.bytes.Length);

			//AndroidMessage.Create("Snapshot Loaded", "Data: " + result.Snapshot.stringData);

			string decryptSrc = result.Snapshot.stringData;
			string dataSrc = Util.Decrypt (decryptSrc);
			PlayerData data = Newtonsoft.Json.JsonConvert.DeserializeObject<PlayerData> (dataSrc);
			GameManager.Instance.SetPlayerData (data);
		} else {
			GameManager.Instance.SetPlayerData(new PlayerData(GameManager.PlayerID));
		}
	}

	private void ActionGameSaveResult (GP_SpanshotLoadResult result) {
		PopupManager.Instance.CloseLoading ();
		GooglePlaySavedGamesManager.ActionGameSaveResult -= ActionGameSaveResult;
		//Debug.Log("ActionGameSaveResult: " + result.Message);

		if(result.IsSucceeded) {
			PopupManager.Instance.Message (TextManager.Get (TextKey.success_save_data));
			//SA_StatusBar.text = "Games Saved: " + result.Snapshot.meta.Title;
		} else {
			PopupManager.Instance.Message (TextManager.Get (TextKey.fail_save_data));
			//SA_StatusBar.text = "Games Save Failed";
		}
	}	

	private void ActionConflict (GP_SnapshotConflict result) {

		Debug.Log("Conflict Detected: ");

		GP_Snapshot snapshot = result.Snapshot;
		GP_Snapshot conflictSnapshot = result.ConflictingSnapshot;

		// Resolve between conflicts by selecting the newest of the conflicting snapshots.
		GP_Snapshot mResolvedSnapshot = snapshot;

		if (snapshot.meta.LastModifiedTimestamp < conflictSnapshot.meta.LastModifiedTimestamp) {
			mResolvedSnapshot = conflictSnapshot;
		}

		result.Resolve(mResolvedSnapshot);
		PopupManager.Instance.CloseLoading ();
	}


	private void OnPlayerDisconnected() {
		//isLogin = false;
		Debug.Log("OnPlayerDisconnected");
		GameManager.PlayerID = SystemInfo.deviceUniqueIdentifier;
		GameManager.Instance.SetPlayerData (SaveManager.Instance.LoadData ());
		//GameManager.Instance.SetPlayerData (new PlayerData(GameManager.PlayerID));
	}

	private void OnPlayerConnected() {
		//isLogin = true;
		Debug.Log("OnPlayerConnected");
		GameManager.PlayerID = GooglePlayManager.Instance.player.playerId;
		//LoadGame ();
		LoadAchievements ();
	}

	private void OnConnectionResult(GooglePlayConnectionResult result) {
		Debug.Log ("OnConnectionResult   " + result.IsSuccess);
		if (result.IsSuccess) {
			GameManager.PlayerID = GooglePlayManager.Instance.player.playerId;
		} else {
			GameManager.PlayerID = SystemInfo.deviceUniqueIdentifier;
		}
	}



	//--------------------------------------
	// PRIVATE METHODS
	//--------------------------------------

	private IEnumerator MakeScreenshotAndSaveGameData() {


		yield return new WaitForEndOfFrame();
		// Create a texture the size of the screen, RGB24 format
		int width = Screen.width;
		int height = Screen.height;
		Texture2D Screenshot = new Texture2D( width, height, TextureFormat.RGB24, false );
		// Read screen contents into the texture
		Screenshot.ReadPixels( new Rect(0, 0, width, height), 0, 0 );
		Screenshot.Apply();

		long TotalPlayedTime = Util.CurrentTime ()-GameManager.Instance.GetPlayerGameStartTime ();
		string currentSaveName =  "SchoolFighter";
		string description  = "Modified data at: " + System.DateTime.Now.ToString("MM/dd/yyyy H:mm:ss");
		string saveData = SaveManager.Instance.GetSaveData ();
		Debug.Log ("MakeScreenShot  : " + currentSaveName + " | " + description + " | " + Screenshot + " | " + saveData + " | " + TotalPlayedTime);
		GooglePlaySavedGamesManager.ActionGameSaveResult += ActionGameSaveResult;
		GooglePlaySavedGamesManager.Instance.CreateNewSnapshot(currentSaveName,
			description,
			Screenshot,
			saveData,
			TotalPlayedTime);		
		Destroy(Screenshot);
	}

    //----------------------------------------
    //achievements and leaderboard
    //------------------------------------------
    private const string LEADERBOARD_STAGE = "CgkIxIbP_toZEAIQAg";
    private const string ACHIEVEMENT_STAGE = "CgkIxIbP_toZEAIQAQ";
    public void ShowLeaderBoardsUI()
    {
		if (GooglePlayConnection.State != GPConnectionState.STATE_CONNECTED) {
			//GooglePlayConnection.Instance.Connect ();
			Connect ();
			return;
		}
        GooglePlayManager.Instance.ShowLeaderBoardsUI();
    }

    public void SubmitScore(int stage)
    {
		if (GooglePlayConnection.State != GPConnectionState.STATE_CONNECTED) {
			return;
		}
		GooglePlayManager.Instance.SubmitScoreById(LEADERBOARD_STAGE, (long)stage);
    }

    public void ShowAchievementsUI()
    {
		if (GooglePlayConnection.State != GPConnectionState.STATE_CONNECTED) {
			//GooglePlayConnection.Instance.Connect ();
			Connect ();
			return;
		}
        GooglePlayManager.Instance.ShowAchievementsUI();
    }

    public void ReportAchievement()
    {
        //GooglePlayManager.Instance.UnlockAchievement("achievement_simple_achievement_example");
    }

	private int currentAchievementStage;
	public void UpdateAchievementStage(int stage)
    {
		if (GooglePlayConnection.State != GPConnectionState.STATE_CONNECTED) {
			return;
		}
		if( currentAchievementStage < stage )
			GooglePlayManager.Instance.IncrementAchievementById(ACHIEVEMENT_STAGE, stage-currentAchievementStage);
    }

    public void UpdateAchievementStep(string achievementId, int step)
    {
        if (GooglePlayConnection.State != GPConnectionState.STATE_CONNECTED)
        {
            return;
        }
        if(achievementSteps.ContainsKey(achievementId))
        {
            if (achievementSteps[achievementId] < step)
                GooglePlayManager.Instance.IncrementAchievementById(achievementId, step - achievementSteps[achievementId]);
        }
        else
        {
            GooglePlayManager.Instance.IncrementAchievementById(achievementId, step);
        }

    }

    public void revealAchievement()
    {
        //GooglePlayManager.Instance.RevealAchievement("achievement_hidden_achievement_example");
    }

    public void ResetAchievement()
    {
        //GooglePlayManager.Instance.ResetAchievement(INCREMENTAL_ACHIEVEMENT_ID);
    }

    public void ResetAllAchievements()
    {
        //GooglePlayManager.Instance.ResetAllAchievements();
    }

	public void LoadAchievements() {
		GooglePlayManager.ActionAchievementsLoaded += OnAchievementsLoaded;
		GooglePlayManager.Instance.LoadAchievements ();
	}

    private Dictionary<string, int> achievementSteps = new Dictionary<string, int>();

    private void OnAchievementsLoaded(GooglePlayResult result)
    {
        achievementSteps.Clear();
        GooglePlayManager.ActionAchievementsLoaded -= OnAchievementsLoaded;
        if (result.IsSucceeded)
        {
			foreach (GPAchievement achievement in GooglePlayManager.Instance.Achievements)
            {
				if (achievement.Id == ACHIEVEMENT_STAGE)
					currentAchievementStage = achievement.CurrentSteps;

                achievementSteps.Add(achievement.Id, achievement.CurrentSteps);
            }
        }
		LoadGame ();
    }

    private void OnAchievementUpdated(GP_AchievementResult result)
    {
		//SA_StatusBar.text = "Achievment Updated: Id: " + result.achievementId + "\n status: " + result.Message;
        //AN_PoupsProxy.showMessage("Achievment Updated ", "Id: " + result.achievementId + "\n status: " + result.Message);
    }



    private void OnLeaderBoardsLoaded(GooglePlayResult result)
    {
        /*GooglePlayManager.ActionLeaderboardsLoaded -= OnLeaderBoardsLoaded;

        if (result.IsSucceeded)
        {
            if (GooglePlayManager.Instance.GetLeaderBoard(LEADERBOARD_ID) == null)
            {
                AN_PoupsProxy.showMessage("Leader boards loaded", LEADERBOARD_ID + " not found in leader boards list");
                return;
            }


            SA_StatusBar.text = LEADERBOARD_NAME + "  score  " + GooglePlayManager.Instance.GetLeaderBoard(LEADERBOARD_ID).GetCurrentPlayerScore(GPBoardTimeSpan.ALL_TIME, GPCollectionType.FRIENDS).LongScore.ToString();
            AN_PoupsProxy.showMessage(LEADERBOARD_NAME + "  score", GooglePlayManager.Instance.GetLeaderBoard(LEADERBOARD_ID).GetCurrentPlayerScore(GPBoardTimeSpan.ALL_TIME, GPCollectionType.FRIENDS).LongScore.ToString());
        }
        else
        {
            SA_StatusBar.text = result.Message;
            AN_PoupsProxy.showMessage("Leader-Boards Loaded error: ", result.Message);
        }
        */
    }

    private void OnScoreSubmited(GP_LeaderboardResult result)
    {
        /*if (result.IsSucceeded)
        {
            SA_StatusBar.text = "Score Submited:  " + result.Message
                + " LeaderboardId: " + result.Leaderboard.Id
                    + " LongScore: " + result.Leaderboard.GetCurrentPlayerScore(GPBoardTimeSpan.ALL_TIME, GPCollectionType.FRIENDS).LongScore;
        }
        else
        {
            SA_StatusBar.text = "Score Submit Fail:  " + result.Message;
        }*/
    }

    private void OnScoreUpdated(GooglePlayResult res)
    {
        
    }
}

