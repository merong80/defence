﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Net;
using System.Net.Sockets;

public class Util
{
    public static string GetTimeString(int time)
	{
		int min = time / 60;
		int sec = time % 60;
		return string.Format ("{0:D2}", min) + ":" + string.Format ("{0:D2}", sec);
	}

    public static string GetTimeString(int time, string format)
    {
        int hour = time / 3600;
        int min = (time % 3600) / 60;
        int sec = time % 60;

        return string.Format(format, hour, min, sec);
    }


    public static long CurrentTime()
	{
        Debug.Log("CurrentTime");
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManager.Instance.Message(TextManager.Get(TextKey.no_network));
            return 0;
        }
        DateTime nowTime = GetNetworkTime();
        //return (DateTime.UtcNow.Ticks - 621355968000000000) / 10000000;
        return (nowTime.Ticks - 621355968000000000) / 10000000;
    }

	public static DateTime GetDateTime(long tick)
	{
		return DateTime.FromBinary((tick * 10000000) + 621355968000000000);
	}

    public static string GetToday()
    {
        Debug.Log("GetToday");
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManager.Instance.Message(TextManager.Get(TextKey.no_network));
            return "";
        }
        DateTime nowTime = GetNetworkTime();
        return nowTime.DayOfYear.ToString();
    }

    private static string encryptionKey = "i-eye_studio_go_to>Reach";
	public static string Encrypt(string textToEncrypt)
	{
		byte[] keyBytes = Encoding.UTF8.GetBytes(encryptionKey);
		byte[] keyIV = keyBytes;
		byte[] inputByteArray = Encoding.UTF8.GetBytes(textToEncrypt);

		TripleDESCryptoServiceProvider desProvider = new TripleDESCryptoServiceProvider();
		desProvider.Padding = PaddingMode.PKCS7;
		desProvider.Mode = CipherMode.ECB;

		MemoryStream memStream = new MemoryStream();
		CryptoStream crypStream = new CryptoStream(memStream, desProvider.CreateEncryptor(keyBytes, keyIV), CryptoStreamMode.Write);

		crypStream.Write(inputByteArray, 0, inputByteArray.Length);
		crypStream.FlushFinalBlock();

		return Convert.ToBase64String(memStream.ToArray());
	}

	public static string Decrypt(string input)
	{
		byte[] keyBytes = Encoding.UTF8.GetBytes(encryptionKey);
		byte[] keyIV = keyBytes;
		byte[] inputByteArray = Convert.FromBase64String(input);

		TripleDESCryptoServiceProvider desProvider = new TripleDESCryptoServiceProvider();
		desProvider.Padding = PaddingMode.PKCS7;
		desProvider.Mode = CipherMode.ECB;
		MemoryStream memStream = new MemoryStream();
		CryptoStream crypStream = new CryptoStream(memStream, desProvider.CreateDecryptor(keyBytes, keyIV), CryptoStreamMode.Write);

		crypStream.Write(inputByteArray, 0, inputByteArray.Length);
		crypStream.FlushFinalBlock();
		string returnStr = Encoding.UTF8.GetString(memStream.ToArray());

		return returnStr;
	}

    public static DateTime GetNetworkTime()
    {
        //default Windows time server
        const string ntpServer = "time1.google.com";

        // NTP message size - 16 bytes of the digest (RFC 2030)
        var ntpData = new byte[48];

        //Setting the Leap Indicator, Version Number and Mode values
        ntpData[0] = 0x1B; //LI = 0 (no warning), VN = 3 (IPv4 only), Mode = 3 (Client Mode)

        var addresses = Dns.GetHostEntry(ntpServer).AddressList;

        //The UDP port number assigned to NTP is 123
        var ipEndPoint = new IPEndPoint(addresses[0], 123);
        //NTP uses UDP

        using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
        {
            socket.Connect(ipEndPoint);

            //Stops code hang if NTP is blocked
            socket.ReceiveTimeout = 3000;

            socket.Send(ntpData);
            socket.Receive(ntpData);
            socket.Close();
        }

        //Offset to get to the "Transmit Timestamp" field (time at which the reply 
        //departed the server for the client, in 64-bit timestamp format."
        const byte serverReplyTime = 40;

        //Get the seconds part
        ulong intPart = BitConverter.ToUInt32(ntpData, serverReplyTime);

        //Get the seconds fraction
        ulong fractPart = BitConverter.ToUInt32(ntpData, serverReplyTime + 4);

        //Convert From big-endian to little-endian
        intPart = SwapEndianness(intPart);
        fractPart = SwapEndianness(fractPart);

        var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

        //**UTC** time
        var networkDateTime = (new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddMilliseconds((long)milliseconds);

        return networkDateTime.ToLocalTime();
    }

    // stackoverflow.com/a/3294698/162671
    static uint SwapEndianness(ulong x)
    {
        return (uint)(((x & 0x000000ff) << 24) +
                       ((x & 0x0000ff00) << 8) +
                       ((x & 0x00ff0000) >> 8) +
                       ((x & 0xff000000) >> 24));
    }
}

