﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BlingUIWidget : MonoBehaviour
{
	private UIWidget uiWidget;
	private Tweener tweener;

	[SerializeField]
	private float duration;
	[SerializeField]
	private Color color1;
	[SerializeField]
	private Color color2;
	[SerializeField]
	private Ease ease;

	private void OnEnable()
	{
		if (tweener != null)
		{
			tweener.Kill();
		}
		Bling();
	}

	private void Start()
	{
		if (uiWidget == null) uiWidget = gameObject.GetComponent<UIWidget>();
		if (tweener != null)
		{
			tweener.Kill();
		}
		Bling();
	}

	private void Bling()
	{
		tweener = DOTween.To( () => uiWidget.color, x => uiWidget.color = x, color1, duration ).SetEase(ease).OnComplete(delegate ()
			{
				tweener = DOTween.To( () => uiWidget.color, x => uiWidget.color = x, color2, duration).SetEase(ease).OnComplete(delegate () { Bling(); });
			});
	}

	private void OnDisable()
	{
		if (tweener != null)
		{
			tweener.Kill();
		}
		tweener = DOTween.To (() => uiWidget.color, x => uiWidget.color = x, color1, duration).SetEase (ease);
	}
}

