﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ShakeObject : MonoBehaviour
{
	[SerializeField]
	private float duration;
	[SerializeField]
	private float strength = 1f;

	public void Shake()
	{
		Shake (strength);
	}

	private Tweener tweener;
	public void Shake(float strength)
	{
		if (tweener != null)
			tweener.Kill (true);
		Vector3 pos = transform.position;
		tweener = transform.DOShakePosition(duration, new Vector3(strength,0f,0f)).OnComplete(delegate() {
			transform.position = pos;
		});
	}
}

