﻿using UnityEngine;
using System.Collections;

public class TextSetter : MonoBehaviour
{
    [SerializeField]
    private string key;
    private UILabel label;
    private void OnEnable()
    {
        if (label == null) label = gameObject.GetComponent<UILabel>();
        label.text = TextManager.Get(key);
    }
}
