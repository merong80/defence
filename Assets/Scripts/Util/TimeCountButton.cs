﻿using UnityEngine;
using System.Collections;

public class TimeCountButton : MonoBehaviour
{
    [SerializeField]
    private int showCount = 5;
    [SerializeField]
    private float checkTime = 1f;
    private int currentClick = 0;
    [SerializeField]
    private GameObject showObj;

    public void ClickButton()
    {
        if (currentClick == 0)
        {
            StartCoroutine(CheckCount());
        }
        currentClick += 1;
    }

    private IEnumerator CheckCount()
    {
        float time = 0f;
        while (time < checkTime)
        {
            yield return new WaitForEndOfFrame();
            if (showCount <= currentClick)
            {
                showObj.SetActive(true);
                break;
            }
        }
        currentClick = 0;
    }
}
