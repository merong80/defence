﻿using UnityEngine;
using System.Collections;

public class ButtonsSound : MonoBehaviour
{
	[SerializeField]
	private string clip;
	void OnPress (bool isPressed)
	{
		if (isPressed) {
			if (!string.IsNullOrEmpty(clip))
				SoundManager.Instance.PlayEffect (this.gameObject, clip);
			else
				SoundManager.Instance.PlayClick (this.gameObject);
		}
	}
}

