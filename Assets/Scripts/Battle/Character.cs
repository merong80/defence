﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Character : MonoBehaviour
{
	private CharacterState preState;
	private CharacterState _state;
    [SerializeField]
    private string attackSoundPath;
	[SerializeField]
	private string hitSoundPath;
	[SerializeField]
	private string dieSoundPath;
	[SerializeField]
	private string attackEffectPath;
	[SerializeField]
	private Transform attackEffectTransform;
	[SerializeField]
	private float atkSpeed;
	[SerializeField]
	private int atkFrame;
	[SerializeField]
	private AttackType atkType;
	[SerializeField]
	private AttackTarget atkTarget;
	[SerializeField]
	private string projectilePath;

	private Health health;

	public CharacterState state
	{
		set
		{
			preState = _state;
			_state = value;
			if (anim == null) {
				anim = GetComponent<UISpriteAnimation> ();
				if( anim == null )
					anim = GetComponentInChildren<UISpriteAnimation> ();
			}
			if (anim != null) {
				if( !isStop )
					StartCoroutine (StartAnimation ());
			}
		}
		get
		{
			return _state;
		}
	}
	public CharacterCondition condition;
	public CharacterBattleData data;

	private List<Health> target = new List<Health>();
	private float cool;
	private int currentDamage;

	public void AddTarget(Health go)
	{
		if (!target.Contains (go))
			target.Add (go);
	}

	public void RemoveTarget(Health go)
	{
		if (target.Contains (go))
			target.Remove (go);
	}

	private UISpriteAnimation anim;

	[SerializeField]
	private string IDLE_ANI;
	[SerializeField]
	private string WALK_ANI;
	[SerializeField]
	private string ATTACK_ANI;
	[SerializeField]
	private string HIT_ANI;
	[SerializeField]
	private string KNOCKBACK_ANI;
	[SerializeField]
	private string DIE_ANI;
	[SerializeField]
	private UISprite sprite;
	void Start()
	{
		//anim = GetComponent<UISpriteAnimation> ();
		//state = CharacterState.idle
		//Init();
		CheckState ();
	}

	private IEnumerator CompleteAnimation()
	{
		while (anim.isPlaying) {
			yield return new WaitForEndOfFrame ();
		}
		yield return null;
	}

	/*private IEnumerator TransformAnimation()
	{

	}*/

	private IEnumerator StartAnimation()
	{
		if( health == null ) health = gameObject.GetComponent<Health> ();
		//health.invincible = state == CharacterState.KNOCKBACK || state == CharacterState.DIE;
		switch (state) {
		case CharacterState.IDLE: 
			anim.loop = true;
			anim.namePrefix = IDLE_ANI;
			break;
		case CharacterState.WALK: 
			anim.loop = true;
			anim.namePrefix = WALK_ANI;
			break;
		case CharacterState.ATTACK: 
			anim.loop = false;
			anim.namePrefix = ATTACK_ANI;
			break;
		case CharacterState.HIT: 
			anim.loop = true;
			anim.namePrefix = HIT_ANI;
			break;
		case CharacterState.KNOCKBACK: 
			anim.loop = true;
                anim.namePrefix = IDLE_ANI;
                //anim.namePrefix = KNOCKBACK_ANI;
                break;
		case CharacterState.DIE: 
			anim.loop = true;
                anim.namePrefix = IDLE_ANI;
                //anim.namePrefix = DIE_ANI;
                break;
		}
		anim.ResetToBeginning ();
		anim.Play ();
		if (!anim.loop) {
			yield return CompleteAnimation ();
			state = preState;
			CheckState ();
		}
	}

	public FaceDirection faceDirection;
	[SerializeField]
	private UISprite hpFrameSprite;
	[SerializeField]
	private UISprite hpBarSprite;

	private Tower tower;
	public virtual void Init(Tower tower, CharacterBattleData characterData, Vector2 position, float scale, int depth, FaceDirection dir)
	{
		if (sprite == null)
			sprite = gameObject.GetComponent<UISprite> ();
		if( sprite == null ) sprite = gameObject.GetComponentInChildren<UISprite> ();

		this.tower = tower;

        if (fadeInDuration > 0f)
            FadeIn(fadeInDuration);

		gameObject.SetActive (true);
		transform.localEulerAngles = Vector3.zero;
		isDie = false;
		sprite.color = Color.white;
		data = characterData;
		faceDirection = dir;
		sprite.depth = depth;
		if( hpFrameSprite != null )
			hpFrameSprite.depth = depth + 1;
		if(hpBarSprite != null )
			hpBarSprite.depth = depth + 2;
		transform.localPosition = position;
		transform.localScale = new Vector3 (scale, scale, scale);
		currentDamage = 0;

		atkTarget = data.atkTarget;

		CheckState ();

		if( health == null ) health = gameObject.GetComponent<Health> ();
		if (knockback == null)
			knockback = gameObject.GetComponent<KnockBack> ();
		if (health != null) {
			health.Init (data.maxHp);
		}
	}

	void Update()
	{
		if (cool > 0) {
			cool -= Time.deltaTime;
			if (cool <= 0)
				cool = 0;
		}

		if (isStop)
			return;
		if( state == CharacterState.WALK || state == CharacterState.IDLE ) CheckState ();

		if (state == CharacterState.WALK) {
			Vector3 pos = transform.localPosition;
			float move = faceDirection == FaceDirection.RIGHT ? pos.x + (data.moveSpeed * Time.deltaTime) : pos.x - (data.moveSpeed * Time.deltaTime);
			transform.localPosition = new Vector3 (move, pos.y, pos.z);
		}
	}


	IEnumerator Attack()
	{
		//start attack animation
		ResetCool ();
		state = CharacterState.ATTACK;
		//float time = 0f;
		if( !string.IsNullOrEmpty(attackSoundPath) )
		{
			//SoundManager.Instance.PlayEffect(gameObject, attackSoundPath);
		}
		while(true)
		{
			if (state != CharacterState.ATTACK) {
				yield break;
			}
			if (anim.frameIndex >= atkFrame) {
				break;
			}
			yield return new WaitForEndOfFrame ();

			/*yield return new WaitForEndOfFrame ();
			time += Time.deltaTime;*/
		}
		if (state == CharacterState.ATTACK) {
			if (atkType == AttackType.MELEE) {
				if (target.Count > 0) {
					if (atkTarget == AttackTarget.ONE) {
						for (int i = 0; i < target.Count; i++) {
							target [i].Damage (data.atk, data.atkForce, data.finishForce);
							break;
						}
					} else {
						for (int i = 0; i < target.Count; i++) {
							target [i].Damage (data.atk, data.atkForce, data.finishForce);
						}
					}
					if (!string.IsNullOrEmpty (attackEffectPath)) {
						BattleEffect effect = BattleManager.Instance.effectPooler.Get (attackEffectPath, transform.parent, attackEffectTransform.localScale, attackEffectTransform.position);
						effect.StartEffect (faceDirection == FaceDirection.LEFT);
					}

					if( !string.IsNullOrEmpty(attackSoundPath) )
					{
						SoundManager.Instance.PlayEffect(gameObject, attackSoundPath);
					}
				}
			} else {
				Projectile proj = BattleManager.Instance.projectilePooler.Get (projectilePath, transform.parent);
				proj.Init (data.atk,attackEffectTransform.position,faceDirection,data.atkForce,data.finishForce);
			}
		}
	}

	private void ResetCool()
	{
		cool = data.atkCool;
	}

	private void CheckState()
	{
		if (target.Count > 0) {
			if (state == CharacterState.WALK) {
				state = CharacterState.IDLE;
			}
			if (state == CharacterState.IDLE) {
				if (cool == 0) {
					StartCoroutine (Attack ());
				}
			}
		} else {
			if( state != CharacterState.WALK )
				state = CharacterState.WALK;
		}
	}

	private KnockBack knockback;
	public void Hit(int damage, Vector2 force)
	{
		//Debug.Log ("Hit   " + damage+"  //  "+force+"  //  "+currentDamage+"  //  "+data.knockBackDamage);
		if( !string.IsNullOrEmpty(hitSoundPath) )
		{
			SoundManager.Instance.PlayEffect(gameObject, hitSoundPath);
		}
		if (knockback != null) {
			if (force != Vector2.zero) {
				currentDamage = 0;
				state = CharacterState.KNOCKBACK;
				knockback.KnockBackAnimation (force);
			} else {
				if (data.knockBackDamage > 0) {
					currentDamage += damage;

					if (currentDamage >= data.knockBackDamage) {
						currentDamage = 0;
						state = CharacterState.KNOCKBACK;
						knockback.KnockBackAnimation ();
					}
				}
			}
		}
	}

	public void KnockbackComplete()
	{
		if (isDie) {
			//state = CharacterState.DIE;
			//gameObject.SetActive (false);
			//BattleManager.Instance.characterPooler.Put (this);
			DieActually ();
		} else {
			//state = preState;
			state = CharacterState.IDLE;
			CheckState ();
		}
	}
	private bool isDie = false;
	public void Die(Vector2 finishForce)
	{
		isDie = true;

		if (knockback != null) {

			if (!string.IsNullOrEmpty (dieSoundPath)) {
				SoundManager.Instance.PlayEffect (gameObject,dieSoundPath);
			}
			state = CharacterState.KNOCKBACK;
			//knockback.KnockBackAnimation (finishForce);
			knockback.DieKnockBack (finishForce);
		} else {
			//DieAnimation ();
			//state = CharacterState.DIE;
			DieActually ();
		}
		//state = CharacterState.DIE;
	}

    [SerializeField]
    private float fadeInDuration;
    public void FadeIn(float duration)
    {
        sprite.alpha = 0f;
        DOTween.To(() => sprite.alpha, x => sprite.alpha = x, 1f, duration).SetEase(Ease.InCirc);
    }

    public void FadeOut(float duration)
    {
        DOTween.To(() => sprite.alpha, x => sprite.alpha = x, 0f, duration).SetEase(Ease.InCirc);
    }

	private void DieActually()
	{
		if (faceDirection == FaceDirection.LEFT) {
			//Debug.Log (GameManager.Instance.GetPlayerTowerLevelData (TowerLevelType.MONEY_KILL).value + "//" + data.killMoney);
			//Debug.Log (GameManager.Instance.GetPlayerTowerLevelData (TowerLevelType.COIN_KILL).value + "//" + data.killCoin);
            int money = Mathf.FloorToInt(GameManager.Instance.GetPlayerTowerLevelData(TowerLevelType.MONEY_KILL).value * data.killMoney);
			BattleManager.Instance.playerTower.ChargeMoney (money);
            int coin = Mathf.FloorToInt(GameManager.Instance.GetPlayerTowerLevelData(TowerLevelType.COIN_KILL).value * data.killCoin);
            GameManager.Instance.ChargeCoin (coin);
		}
		gameObject.SetActive (false);
		tower.RemoveCharacter (this);
		BattleManager.Instance.characterPooler.Put (this);
	}

	public void KnockBack(Vector2 force)
	{
		if (knockback != null) {
			state = CharacterState.KNOCKBACK;
			knockback.KnockBackAnimation (force);
		}
	}

	private bool isStop = false;
	public void Stop()
	{
		isStop = true;
		state = CharacterState.IDLE;
		StopAllCoroutines ();
	}
	public void Play()
	{
		isStop = false;
	}

	public void TogglePause()
	{
		if (knockback != null)
			knockback.Pause ();
	}

	/*[SerializeField]
	private Color dieColor;
	[SerializeField]
	private float dieDuration;
	[SerializeField]
	private Ease dieColorEase;

	[SerializeField]
	private float dieRiseHeight;
	[SerializeField]
	private Ease dieRiseHeightEase;
	[SerializeField]
	private float dieBounceWidth;
	[SerializeField]
	private float dieOverShoot;
	[SerializeField]
	private float diePeriod;
	[SerializeField]
	private Ease dieBounceEase;
	private void DieAnimation()
	{
		
		transform.localEulerAngles = Vector3.zero;
		sprite.color = dieColor;
		Tweener tweenerHeight = transform.DOLocalMoveY (dieRiseHeight,dieDuration).SetRelative (true).SetEase(dieRiseHeightEase);
		Tweener tweenerWidth = transform.DOLocalMoveX (dieBounceWidth,dieDuration*0.2f).SetRelative (true).SetEase (Ease.Linear).OnComplete (delegate() {
			transform.DOLocalMoveX (-dieBounceWidth,dieDuration*0.8f).SetRelative (true).SetEase(dieBounceEase).OnComplete (DieActually);
		});//sprite.color = dieColor;
	}*/
}
