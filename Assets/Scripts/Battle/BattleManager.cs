﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Com.LuisPedroFonseca.ProCamera2D;


public class BattleManager : Singleton<BattleManager>
{
	public Tower playerTower;
	public TowerMoneyCreator playerTowerMoneyCreator;
	public Tower enemyTower;

	public bool IsInit = false;
	public bool isPlay = false;

    public int[] characterNum;
    public float[] characterCreateTime;
    
    public float GetCreateTime(int id)
    {
        int num = playerTower.GetCharacterNum(id);
        for( int i = characterNum.Length-1; i >= 0; i-- )
        {
            if (num >= characterNum[i])
                return characterCreateTime[i];
        }
        return 1f;
    }

	void Start()
	{
        SoundManager.Instance.PlayBattleBGM();
		Init ();
	}
	public List<CharacterBattleData> characterDataPlayer = new List<CharacterBattleData>();
	public Dictionary<int, CharacterBattleData> characterDataEnemy = new Dictionary<int, CharacterBattleData>();

    [SerializeField]
    private BattleProduce production;
    [SerializeField]
    private BackgroundChanger backgroundChanger;
	public void Init()
	{
		stageLabel.text = "STAGE : " + GameManager.Instance.CurrentStage;
		characterDataPlayer = GameManager.Instance.GetBattlePlayerCharacterData ();
		backgroundChanger.Init();

		StageData stageData = GameManager.Instance.GetStageData (GameManager.Instance.CurrentStage);
		enemyTower.SetStageData (stageData);

        for (int i = 0; i < stageData.enemyIds.Length; i++)
        {
            characterDataEnemy.Add(stageData.enemyIds[i], GameManager.Instance.GetEnemyBattleData(stageData.enemyIds[i], GameManager.Instance.CurrentStage));
        }
		enemyTower.SetStagePattern (GameManager.Instance.GetEnemyPatterns (GameManager.Instance.CurrentStage));
        /*List<EnemyCreateData> enemyCreateData = GameManager.Instance.GetEnemyCreateList (GameManager.Instance.CurrentStage);
		for (int i = 0; i < enemyCreateData.Count; i++) {
			characterDataEnemy.Add (enemyCreateData[i].enemyId, GameManager.Instance.GetEnemyBattleData (enemyCreateData[i].enemyId, GameManager.Instance.CurrentStage));
		}
		enemyTower.SetEnemyCreateData (enemyCreateData);
		enemyTower.SetTowerEnemyCreateData (GameManager.Instance.GetTowerEnemyCreateList (GameManager.Instance.CurrentStage));*/


        towerMoneyLevel = GameManager.Instance.GetPlayerTowerMoneyLevel ();

		playerTowerLevel = 0;
		playerTowerLevelUpButton.UpdateButton (towerMoneyLevel[playerTowerLevel].level,towerMoneyLevel[playerTowerLevel+1].cost);
		playerTowerMoneyCreator.LevelUp (towerMoneyLevel[playerTowerLevel].moneySpeed,towerMoneyLevel[playerTowerLevel].moneyMax);

		for (int i = 0; i < characterButtons.Length; i++) {
			if (characterDataPlayer.Count > i) {
				characterButtons [i].SetData (characterDataPlayer [i]);
			} else {
				characterButtons [i].SetData (null);
			}
		}
		playerTower.maxHp = GameManager.Instance.GetPlayerTowerHp ();

		//EnemyTowerData enemyTowerData = GameManager.Instance.GetEnemyTowerData (GameManager.Instance.CurrentStage);
		if (GameManager.Instance.CurrentStage > GameManager.Instance.ClearStage) {
			clearRewardCoin = stageData.firstCoin;
		} else {
			clearRewardCoin = 0;
		}
		enemyTower.maxHp = stageData.maxHp;
		float distanceGap = (defaultEnemyTowerDistance - playerTower.transform.localPosition.x) / defaultEnemyTowerDistanceValue;
		enemyTower.transform.localPosition = new Vector3 (playerTower.transform.localPosition.x + distanceGap * stageData.distance, playerTower.transform.localPosition.y, playerTower.transform.localPosition.z);
        cameraBoundaries.RightBoundary = defaultBountdayRight+defaultBountdayRight / enemyTowerBoundarySize * enemyTower.transform.localPosition.x;

		playerTower.SetSkill (GameManager.Instance.GetPlayerSkillData (), distanceGap * stageData.distance-36f);

		IsInit = true;
		isPlay = true;
        production.Fight(null);
	}
	private int clearRewardCoin = 0;
	[SerializeField]
	private float defaultEnemyTowerDistance;
	private const float defaultEnemyTowerDistanceValue = 100f;
    [SerializeField]
    private ProCamera2DTriggerBoundaries cameraBoundaries;
	[SerializeField]
	private ProCamera2DShake cameraShake;
	[SerializeField]
	private ProCamera2DCinematics cameraCinematic;

	public IEnumerator ShakeCam(float duration,Vector2 strength,float delay)
	{
		yield return new WaitForSeconds (delay);
		cameraShake.Shake (duration,strength);
	}

	public void CinematicCam(Transform target)
	{
		cameraCinematic.Play ();
	}

    private float enemyTowerBoundarySize = 160f;
    private float defaultBountdayRight = 1f;

	[SerializeField]
	private CharacterButton[] characterButtons;

	private const string CHARACTER_PATH = "Prefabs/Character/Battle/";
	public ObjectPooler<Character> characterPooler = new ObjectPooler<Character> (CHARACTER_PATH);

	public float characterPositionMin;
	public float characterPositionMax;
	public int characterDepthMin;
	public int characterDepthMax;
	public float GetCharacterPosition(int depth)
	{
		float gap = (characterPositionMax - characterPositionMin) / (characterDepthMax - characterDepthMin);
		return (depth - characterDepthMin) * gap + characterPositionMin;
	}

	public float characterScaleMin;
	public float characterScaleMax;

	public float GetCharacterScale(int depth)
	{
		float gap = (characterScaleMax - characterScaleMin) / (characterDepthMax - characterDepthMin);
		return (depth - characterDepthMin) * gap + characterScaleMin;
	}

	public List<TowerMoneyLevel> towerMoneyLevel;

	//public List<CharacterBattleData> characterDatasPlayer;
	//public List<CharacterBattleData> characterDatasEnemy;

	public int playerTowerLevel = 0;
	public int playerTowerMaxLevel;

	[SerializeField]
	private TowerLevelUpButton playerTowerLevelUpButton;

	public void UpgradeTowerMoneyLevel()
	{
		playerTowerLevel += 1;
		playerTowerMoneyCreator.currentMoney -= towerMoneyLevel [playerTowerLevel].cost;
		playerTowerMoneyCreator.LevelUp (towerMoneyLevel [playerTowerLevel].moneySpeed, towerMoneyLevel[playerTowerLevel].moneyMax);
		if (playerTowerLevel < towerMoneyLevel.Count - 1) {
			playerTowerLevelUpButton.UpdateButton (towerMoneyLevel [playerTowerLevel].level, towerMoneyLevel [playerTowerLevel+1].cost);
		} else {
			playerTowerLevelUpButton.MaxUpgraded (towerMoneyLevel [playerTowerLevel].level);
		}

	}


	public bool EnableTowerUpgrade()
	{
		if (towerMoneyLevel [playerTowerLevel+1].cost <= playerTowerMoneyCreator.currentMoney) {
			return true;
		}
		return false;
	}

	public bool MaxTowerUpgrade()
	{
		if (playerTowerLevel >= towerMoneyLevel.Count - 1)
			return true;
		return false;
	}

	private const string EFFECT_PATH = "Prefabs/Effect/";
	public ObjectPooler<BattleEffect> effectPooler = new ObjectPooler<BattleEffect> (EFFECT_PATH);
	private const string PROJECTILE_PATH = "Prefabs/Projectile/";
	public ObjectPooler<Projectile> projectilePooler = new ObjectPooler<Projectile> (PROJECTILE_PATH);
	private const string EXPLOSION_PATH = "Prefabs/Explosions/";
	public ObjectPooler<Explosion> explosionPooler = new ObjectPooler<Explosion> (EXPLOSION_PATH);

	public UICamera uiCamera;

	public void DeadCharacter(Character character)
	{
		if (character.data.killMoney > 0) {
			playerTower.ChargeMoney (character.data.killMoney);
		}
	}

	public List<int> bossIds = new List<int>();

    public void DefeatTower(Tower tower)
	{
		//resultLabel.gameObject.SetActive (true);
		//resultLabel.text = tower == playerTower ? "DEFEAT" : "VICTORY";
		if (tower != playerTower) {

			GameManager.Instance.ClearStage = GameManager.Instance.CurrentStage;
			GameManager.Instance.CurrentStage += 1;
			GameManager.Instance.ChargeCoin (clearRewardCoin);
		}
        //Invoke ("GoMain", 2f);
        //StartCoroutine(GoMain());
        if( tower == playerTower )
        {
            production.Fail(delegate () { StartCoroutine(GameManager.Instance.LoadLevel(1)); });
			//SoundManager.Instance.PlayLoseBGM ();
        }
        else
        {
            for( int i = 0; i < bossIds.Count; i++ )
            {
                GameManager.Instance.AddGetBoss(bossIds[i]);
            }
			production.Clear(delegate () { GoMain(); });
			//SoundManager.Instance.PlayWinBGM ();
        }
        
	}

	public void GoMain()
	{
		AdvertiseManager.Instance.CheckVideo ();
        //yield return new WaitForSeconds(2f);
		StartCoroutine(GameManager.Instance.LoadLevel (1));
	}
	[SerializeField]
	private UILabel stageLabel;
	[SerializeField]
	private float[] gameSpeeds;
	private int currentGameSpeedIdx;
	public void GameSpeedChange()
	{
		currentGameSpeedIdx += 1;
		if (currentGameSpeedIdx >= gameSpeeds.Length)
			currentGameSpeedIdx = 0;
		Time.timeScale = gameSpeeds[currentGameSpeedIdx];
	}

	private bool isPause = false;

	public void OnPause()
	{
		isPause = !isPause;

		if (isPause) {
			Time.timeScale = 0f;
			PopupManager.Instance.Open ("option");
		} else {
			Time.timeScale = gameSpeeds[currentGameSpeedIdx];
		}

		playerTower.CharacterTogglePause ();
		enemyTower.CharacterTogglePause ();
	}
}

