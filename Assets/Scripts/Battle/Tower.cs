﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using DG.Tweening;

[System.Serializable]
public class TowerMoneyLevel
{
	public int level;
	public int cost;
	public int moneyMax;
	public float moneySpeed;
}

public class Tower : MonoBehaviour
{
	[SerializeField]
	private TowerMoneyCreator moneyCreator;

	public int maxHp
	{
		set {
			Health health = gameObject.GetComponent<Health> ();
			health.Init (value);
		}
	}

	// Use this for initialization
	void Start ()
	{
		//Health health = gameObject.GetComponent<Health> ();
		//health.Init (maxHp);
		Tower[] tower = GameObject.FindObjectsOfType<Tower> ();
		for (int i = 0; i < tower.Length; i++) {
			if (tower [i] != this)
				enemyTower = tower [i];
		}
	}

	private List<TowerEnemyCreateData> towerEnemyCreateData = new List<TowerEnemyCreateData> ();
	private EnemyCreateData enemyCreateData;
	private StageData stageData;
	public void SetStageData(StageData data)
	{
		stageData = data;

		for (int i = 0; i < data.hpCreate.Length; i++) {
			TowerEnemyCreateData tc = new TowerEnemyCreateData ();
			tc.hp = data.hpCreate[i];
			if( data.hpCreateEnemy.Length > i )
				tc.enemyNum = data.hpCreateEnemy [i];
			if( data.hpCreateBoss.Length > i )
				tc.bossId = data.hpCreateBoss [i];
			towerEnemyCreateData.Add (tc);
		}

		/*enemyCreateData = new EnemyCreateData (data);
        EnemyCreator creator = gameObject.GetComponent<EnemyCreator>();
        if( creator == null ) creator = gameObject.AddComponent<EnemyCreator>();
        creator.SetData(this, enemyCreateData);*/
    }

	public void SetStagePattern(EnemyPattern[] pattern)
	{
		EnemyCreator creator = gameObject.GetComponent<EnemyCreator>();
		if( creator == null ) creator = gameObject.AddComponent<EnemyCreator>();
		creator.SetData(this, pattern);
	}

	public void CharacterTogglePause()
	{
		foreach (KeyValuePair<int,List<Character>> pair in currentCharacter) {
			for (int i = 0; i < pair.Value.Count; i++) {
				pair.Value [i].TogglePause ();
			}
		}
	}
	/*private List<int> characterIds = new List<int>();
	public void SetTowerEnemyCreateData(List<TowerEnemyCreateData> towerEnemyCreateData)
	{
		characterIds.Clear ();
		this.towerEnemyCreateData.AddRange (towerEnemyCreateData);

		foreach (KeyValuePair<int,CharacterBattleData> pair in BattleManager.Instance.characterDataEnemy) {
			
			characterIds.Add (pair.Key);
		}
	}

	public void SetEnemyCreateData(List<EnemyCreateData> enemyCreateData)
	{
		EnemyCreator[] enemyCreator = gameObject.GetComponentsInChildren<EnemyCreator> ();

		for (int i = 0; i < enemyCreateData.Count; i++) {
			if (enemyCreator.Length > i) {
				enemyCreator [i].SetData (this, enemyCreateData [i]);
			} else {
				EnemyCreator creator = gameObject.AddComponent<EnemyCreator> ();
				creator.SetData (this, enemyCreateData [i]);
			}
		}
	}*/
    
	public void Hit(int percent)
	{
		shakeObj.Shake ();

		int removeIdx = -1;
		for (int i = 0; i < towerEnemyCreateData.Count; i++) {
			if (towerEnemyCreateData [i].hp >= percent) {
				for( int j = 0; j < towerEnemyCreateData[i].enemyNum; j++ )
				{
					CreateUnit (BattleManager.Instance.characterDataEnemy [stageData.enemyIds [Random.Range (0, stageData.enemyIds.Length)]]);
				}
				removeIdx = i;
                if( towerEnemyCreateData[i].bossId > 0 )
                {
                    if (!BattleManager.Instance.bossIds.Contains(towerEnemyCreateData[i].bossId)) BattleManager.Instance.bossIds.Add(towerEnemyCreateData[i].bossId);
                    CreateUnit(GameManager.Instance.GetEnemyBattleData(towerEnemyCreateData[i].bossId, GameManager.Instance.CurrentStage));
                }
				break;
			}
		}
		if( removeIdx >= 0 )
			towerEnemyCreateData.RemoveAt (removeIdx);
		//hp -= damage;
		//hpLabel.text = hp + "/" + maxHp;
	}

	[SerializeField]
	private FaceDirection dir;

    //private List<Character> currentCharacter = new List<Character> ();
    private Dictionary<int, List<Character>> currentCharacter = new Dictionary<int, List<Character>>();
	public void CreateUnit(CharacterBattleData data)
	{
		if( moneyCreator != null ) moneyCreator.currentMoney -= data.cost;

		Character character = BattleManager.Instance.characterPooler.Get (data.name, transform.parent);
        if (currentCharacter.ContainsKey(data.id))
            currentCharacter[data.id].Add(character);
        else
            currentCharacter.Add(data.id, new List<Character>() { character });
		int depth = Random.Range (BattleManager.Instance.characterDepthMin, BattleManager.Instance.characterDepthMax);
		float position = BattleManager.Instance.GetCharacterPosition (depth);
		float scale = BattleManager.Instance.GetCharacterScale (depth);
		character.Init (this,data,new Vector2(transform.localPosition.x,position),scale,depth,dir);

		if( data.initForce != Vector2.zero ) enemyTower.KnockBackAllCharacter (data.initForce);
	}

    public int GetCharacterNum(int id)
    {
        if (currentCharacter.ContainsKey(id)) return currentCharacter[id].Count;
        return 0;
    }

	private Tower enemyTower;

	public void KnockBackAllCharacter(Vector2 force)
	{
		foreach (KeyValuePair<int,List<Character>> pair in currentCharacter) {
			for( int i = 0; i < pair.Value.Count; i++ )
            {
                pair.Value[i].KnockBack(force);
            }
		}
	}

	private void KillAllCharacter()
	{
        foreach (KeyValuePair<int, List<Character>> pair in currentCharacter)
        {
            for (int i = 0; i < pair.Value.Count; i++)
            {
                pair.Value[i].Die(Vector2.zero);
            }
        }
    }

	public void StopAllCharacter()
	{
		EnemyCreator[] enemyCreator = gameObject.GetComponentsInChildren<EnemyCreator> ();
		for (int i = 0; i < enemyCreator.Length; i++) {
			enemyCreator [i].StopAllCoroutines ();
		}
        /*for (int i = 0; i < currentCharacter.Count; i++) {
			currentCharacter [i].Stop ();
		}*/
        foreach (KeyValuePair<int, List<Character>> pair in currentCharacter)
        {
            for (int i = 0; i < pair.Value.Count; i++)
            {
                pair.Value[i].Stop();
            }
        }
    }

	public void RemoveCharacter(Character character)
	{
        //if( currentCharacter.Contains (character) ) currentCharacter.Remove(character);
        if( currentCharacter.ContainsKey(character.data.id) )
        {
            if (currentCharacter[character.data.id].Contains(character)) currentCharacter[character.data.id].Remove(character);
        }
    }

	[SerializeField]
	private UILabel hpLabel;
	[SerializeField]
	private TowerDestroyEffect destroyEffect;

	public void DestroyTower()
	{
		enemyTower.StopAllCharacter ();
		StopAllCharacter ();
		KillAllCharacter ();
		//BattleManager.Instance.DefeatTower (this);
		if (BattleManager.Instance.playerTower == this) {
			SoundManager.Instance.PlayLoseBGM ();
		} else
			SoundManager.Instance.PlayWinBGM ();
		destroyEffect.StartEffect (this);
		BattleManager.Instance.isPlay = false;
		//BattleManager.Instance.CinematicCam (this.transform);
	}

	public void ClickChargeMoney()
	{
		ChargeMoney (100);
	}

	public void ChargeMoney(int money)
	{
		if (!BattleManager.Instance.isPlay)
			return;
		if (moneyCreator != null)
			moneyCreator.ChargeMoney (money);
	}
    
	public SkillMeteor meteorSkill;
	public SkillCollider skillCollider;
	public Vector2 meteorForce;

	private SkillData skillData;
	[SerializeField]
	private SkillButton skillButon;

	public void SetSkill(SkillData skillData, float max)
	{
		this.skillData = skillData;
		if (skillButon != null)
			skillButon.SetData (skillData);

		float distanceGap = (defaultSkillRange - transform.localPosition.x) / defaultSkillRangeValue;
		Vector3 rangePosition = skillRangeTransform.localPosition;
		float extension = distanceGap * skillData.range;
		if (extension > max)
			extension = max;
		skillRangeTransform.localPosition = new Vector3 (transform.localPosition.x+extension, rangePosition.y, rangePosition.z);
		Vector3 meteorPosition = meteorSkill.transform.localPosition;
		meteorSkill.transform.localPosition = new Vector3 (meteorPosition.x + extension, meteorPosition.y, meteorPosition.z);
	}
	[SerializeField]
	private Transform skillRangeTransform;

	[SerializeField]
	private float defaultSkillRange;
	private const float defaultSkillRangeValue = 100f;
	[SerializeField]
	private float skillShakeCamDuration;

	[SerializeField]
	private Vector2 skillShakeCamStrength;
	[SerializeField]
	private float skillShakeCamDelay;
	public void Skill()
	{
		StartCoroutine(BattleManager.Instance.ShakeCam (skillShakeCamDuration,skillShakeCamStrength,skillShakeCamDelay));
		meteorSkill.StartEffect ();
		skillCollider.Shot (skillData.damage,new Vector2(skillData.forceX,skillData.forceY),0.5f,0.2f, transform.localPosition, skillRangeTransform.localPosition);
	}

	[SerializeField]
	private ShakeObject shakeObj;
}

