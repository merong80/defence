﻿using UnityEngine;
using System.Collections;

public class SkillMeteor : MonoBehaviour
{
	[SerializeField]
	private SkillEffectMeteor[] meteors;
	[SerializeField]
	private float delay;

	public void StartEffect()
	{
		for (int i = 0; i < meteors.Length; i++) {
			StartCoroutine (FlyMeteor (i*delay,meteors[i]));
		}
	}

	private IEnumerator FlyMeteor(float delay, SkillEffectMeteor meteor)
	{
		yield return new WaitForSeconds (delay);
		meteor.StartEffect ();
	}
}

