﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SkillCollider : MonoBehaviour
{

	private int atk;
	private Vector2 force;

	public void Shot(int atk, Vector2 force, float duration,float delay, Vector3 startPosition, Vector3 finishPosition)
	{
		this.atk = atk;
		this.force = force;
		gameObject.SetActive (true);
		transform.localPosition = startPosition;
		transform.DOLocalMove (finishPosition,duration).SetDelay (delay).SetRelative (false).SetEase (Ease.Linear).OnComplete (delegate() {
			gameObject.SetActive (false);
		});
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (gameObject.tag != col.gameObject.tag) {
			Health health = col.gameObject.GetComponent<Health> ();
			if (health != null) {
				health.Damage (atk, force,Vector2.zero);
			}
		}
	}

}

