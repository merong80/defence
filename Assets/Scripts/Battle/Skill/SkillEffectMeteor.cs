﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SkillEffectMeteor : MonoBehaviour
{
	public Vector3 startPosition;
	public Vector3 finishPosition;
	public float duration = 1f;

	public SpriteAnimationEffect[] bombAnimations;

	public void StartEffect()
	{
		gameObject.SetActive (true);
		transform.localPosition = startPosition;
		transform.DOLocalMove (finishPosition,duration).SetEase (Ease.Linear).OnComplete (delegate() {
			gameObject.SetActive (false);
			for( int i = 0; i < bombAnimations.Length; i++ )
			{
				bombAnimations[i].StartEffect ();
                SoundManager.Instance.PlayEffect(bombAnimations[i].gameObject, "explode");
            }
            
		});
	}
}

