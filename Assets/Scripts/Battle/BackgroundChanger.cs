﻿using UnityEngine;
using System.Collections;

public class BackgroundChanger : MonoBehaviour
{
	[SerializeField]
	private Texture2D[] backgrounds;
	[SerializeField]
	private int changeStageNum;
	[SerializeField]
	private UITexture texture;
	[SerializeField]
	private int[] bgWidths;

	public void Init()
	{
		int stage = GameManager.Instance.CurrentStage;
		int idx = (stage - 1) / (changeStageNum*bgWidths.Length);
		if (idx >= backgrounds.Length) {
			idx = idx % backgrounds.Length;
		}
		int widthIdx = (stage - 1) % (changeStageNum * bgWidths.Length);
		widthIdx = widthIdx / changeStageNum;
		texture.mainTexture = backgrounds [idx];
		texture.width = bgWidths [widthIdx];
	}
}

