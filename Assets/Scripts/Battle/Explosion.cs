﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour
{
	private int damage;
	[SerializeField]
	private float explosionDuration;
	[SerializeField]
	private float showDuration;

	private FlashEffect effect;
	[SerializeField]
	private Vector2 force;
	private Vector2 finishForce;
	[SerializeField]
	private Collider2D thisCollider;
	public void Boom(Vector3 position, int damage, Vector2 finishForce)
	{
		if (effect == null)
			effect = gameObject.GetComponent<FlashEffect> ();

		effect.StartEffect ();
		this.damage = damage;
		transform.position = position;
		gameObject.SetActive (true);
		thisCollider.enabled = true;
		StartCoroutine (HideCollider ());
		StartCoroutine (HideGameObject ());
	}
	private IEnumerator HideCollider()
	{
		yield return new WaitForSeconds (explosionDuration);
		thisCollider.enabled = false;
	}
	private IEnumerator HideGameObject()
	{
		yield return new WaitForSeconds (showDuration);
		gameObject.SetActive (false);
		BattleManager.Instance.explosionPooler.Put (this);
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (gameObject.tag != collider.gameObject.tag) {
			Health health = collider.gameObject.GetComponent<Health> ();
			if (health != null) {
				if (force != Vector2.zero) {
					health.Damage (damage, force, finishForce);
				} else {
					health.Damage (damage);
				}
			}
		}
	}
}

