﻿using UnityEngine;
using System.Collections;

public class SpinProjectile : Projectile
{
    [SerializeField]
    private float spinSpeed;

    protected override void Update()
    {
        base.Update();
        if (gameObject.activeSelf)
        {
            transform.Rotate(Vector3.forward, spinSpeed * Time.deltaTime);
        }
        
    }
}
