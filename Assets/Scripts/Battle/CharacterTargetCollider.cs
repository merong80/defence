﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTargetCollider : MonoBehaviour {
	[SerializeField]
	private Character character;

	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.tag != gameObject.tag) {
			Health health = col.gameObject.GetComponent<Health> ();
			if( health != null ) character.AddTarget (health);
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if (col.gameObject.tag != gameObject.tag) {
			Health health = col.gameObject.GetComponent<Health> ();
			if( health != null ) character.RemoveTarget (health);

		}
	}
}
