﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BulletProjectile : Projectile
{
	[SerializeField]
	private float delayTime;
	[SerializeField]
	private float downHeight;
	[SerializeField]
	private float downDuration;

	public override void Init(int damage, Vector3 position, FaceDirection direction, Vector2 force, Vector2 finishForce)
	{
		if (rotateTweener != null)
			rotateTweener.Kill (false);
		if (moveTweener != null)
			moveTweener.Kill (false);
		
		base.Init (damage, position, direction, force, finishForce);
		transform.localEulerAngles = Vector3.zero;
		CancelInvoke ("Down");
		Invoke ("Down", delayTime);
		//Down ();
	}

	protected override void OnTriggerEnter2D(Collider2D col)
	{
		base.OnTriggerEnter2D (col);
		if( !gameObject.activeSelf ) CancelInvoke ("Down");
	}

	private Tweener rotateTweener;
	private Tweener moveTweener;

	private void Down()
	{
		rotateTweener = transform.DORotate (new Vector3 (0f, 0f, -45f), downDuration);
		moveTweener = transform.DOLocalMoveY (-downHeight, downDuration).OnComplete (delegate() {
			Disable();
		});

	}
}

