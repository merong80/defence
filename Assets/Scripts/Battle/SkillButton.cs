﻿using UnityEngine;
using System.Collections;

public class SkillButton : MonoBehaviour
{
	public SkillData skillData;
	public UISprite disableSprite;
	[SerializeField]
	private GameObject blingObj;
    [SerializeField]
    private UILabel countlabel;
    [SerializeField]
    private float countTime;

	public void SetData(SkillData data)
	{
		skillData = data;
		coolTime = skillData.cool;
	}

	private float coolTime;
	void Update()
	{
		if (coolTime > 0f) {
			coolTime -= Time.deltaTime;
		}

		if (coolTime > 0f) {
            skillLineObj.SetActive(false);
            disableSprite.gameObject.SetActive (true);
			disableSprite.fillAmount = coolTime / skillData.cool;
			blingObj.SetActive (false);
            if( coolTime < countTime )
            {
                countlabel.gameObject.SetActive(true);
                countlabel.text = Mathf.CeilToInt(coolTime).ToString();
            }
            else
            {
                countlabel.gameObject.SetActive(false);
            }

		} else {
            skillLineObj.SetActive(true);
			disableSprite.gameObject.SetActive (false);
			blingObj.SetActive (true);
            countlabel.gameObject.SetActive(false);
        }
	}

    [SerializeField]
    private GameObject skillLineObj;

	public void ClickSkill()
	{
		if (disableSprite.gameObject.activeSelf)
			return;
		coolTime = skillData.cool;
		BattleManager.Instance.playerTower.Skill ();
	}
}

