﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BattleProduce : MonoBehaviour
{
    [SerializeField]
    private UISprite bg;
    [SerializeField]
    private UISprite textSprite;

    public delegate void FinishCallback();
    public enum TweenType
    {
        NONE = 0,
        SIZE = 1,
        MOVING = 2,
        ALPHA = 3,
    }
    public enum FadeType
    {
        NONE = 0,
        FADEIN = 1,
        FADEOUT = 2,
    }

    [System.Serializable]
    public class TweenData
    {
        public Ease alphaEase;
        public float alphaDuration;
        public float initAlpha;
        public float finishAlpha;
        public Ease sizeEase;
        public float sizeDuration;
        public Vector3 initSize;
        public Vector3 finishSize;
        public Ease moveEase;
        public float moveDuration;
        public Vector3 initMove;
        public Vector3 finishMove;
    }
    [System.Serializable]
    public class ProduceData
    {
        public FadeType bgFadeType;
        public Color bgColor;
        public float bgFadeDuration;
        public TweenData initTweenData;
        public float delay;
        public TweenData finishData;
    }

    [SerializeField]
    private ProduceData fightData;
    [SerializeField]
    private ProduceData clearData;
    [SerializeField]
    private ProduceData failData;

    private FinishCallback finishCallback;

    public void Fight(FinishCallback callback)
    {
        Debug.Log("Fight!!!!!");
        gameObject.SetActive(true);
        finishCallback = callback;

        textSprite.spriteName = "b_fight";
        textSprite.MakePixelPerfect();
        StartCoroutine(ShowProduce(fightData));
    }

    public void Clear(FinishCallback callback)
    {
        gameObject.SetActive(true);
        finishCallback = callback;

        textSprite.spriteName = "b_clear";
        textSprite.MakePixelPerfect();
        StartCoroutine(ShowProduce(clearData));
    }

    public void Fail(FinishCallback callback)
    {
        gameObject.SetActive(true);
        finishCallback = callback;

        textSprite.spriteName = "b_failed";
        textSprite.MakePixelPerfect();
        StartCoroutine(ShowProduce(failData));
    }

    private IEnumerator ShowProduce(ProduceData data)
    {
        switch(data.bgFadeType)
        {
            case FadeType.FADEIN:
                bg.color = data.bgColor;
                bg.alpha = 0f;
                DOTween.To(() => bg.alpha, x => bg.alpha = x, data.bgColor.a, data.bgFadeDuration);
                break;
            case FadeType.FADEOUT:
                bg.color = data.bgColor;
                DOTween.To(() => bg.alpha, x => bg.alpha = x, 0f, data.bgFadeDuration);
                break;
        }

        textSprite.alpha = data.initTweenData.initAlpha;
        DOTween.To(() => textSprite.alpha, x => textSprite.alpha = x, data.initTweenData.finishAlpha, data.initTweenData.alphaDuration).SetEase(data.initTweenData.alphaEase);
        textSprite.transform.localPosition = data.initTweenData.initMove;
        textSprite.transform.DOLocalMove(data.initTweenData.finishMove, data.initTweenData.moveDuration).SetEase(data.initTweenData.moveEase);
        textSprite.transform.localScale = data.initTweenData.initSize;
        textSprite.transform.DOScale(data.initTweenData.finishSize, data.initTweenData.sizeDuration).SetEase(data.initTweenData.sizeEase);
        float delay = data.initTweenData.alphaDuration >= data.initTweenData.sizeDuration ? data.initTweenData.alphaDuration : data.initTweenData.sizeDuration;
        delay = delay >= data.initTweenData.moveDuration ? delay : data.initTweenData.moveDuration;

        yield return new WaitForSeconds(delay + data.delay);

        textSprite.alpha = data.finishData.initAlpha;
        DOTween.To(() => textSprite.alpha, x => textSprite.alpha = x, data.finishData.finishAlpha, data.finishData.alphaDuration).SetEase(data.finishData.alphaEase);
        textSprite.transform.localPosition = data.finishData.initMove;
        textSprite.transform.DOLocalMove(data.finishData.finishMove, data.finishData.moveDuration).SetEase(data.finishData.moveEase);
        textSprite.transform.localScale = data.finishData.initSize;
        textSprite.transform.DOScale(data.finishData.finishSize, data.finishData.sizeDuration).SetEase(data.finishData.sizeEase);

        delay = data.finishData.alphaDuration >= data.finishData.sizeDuration ? data.finishData.alphaDuration : data.finishData.sizeDuration;
        delay = delay >= data.finishData.moveDuration ? delay : data.finishData.moveDuration;

        yield return new WaitForSeconds(delay);
        if (finishCallback != null) finishCallback();
        finishCallback = null ;
        gameObject.SetActive(false);
    }
    
}
