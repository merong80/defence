﻿using UnityEngine;
using System.Collections;

public class CharacterButton : MonoBehaviour
{
	private CharacterBattleData characterData;
	[SerializeField]
	private UISprite disableSprite;
	[SerializeField]
	private UISprite characterSprite;
	[SerializeField]
	private UISprite gaugeFrameSprite;
	[SerializeField]
	private UISprite createCoolSprite;
	[SerializeField]
	private UILabel costLabel;
	[SerializeField]
	private GameObject blingObj;


	public void SetData(CharacterBattleData data)
	{
		characterData = data;
		if (data == null) {
			characterSprite.gameObject.SetActive (false);
			gaugeFrameSprite.gameObject.SetActive (false);
			createCoolSprite.gameObject.SetActive (false);
			costLabel.gameObject.SetActive (false);
			blingObj.SetActive (false);
			ButtonsSound bs = gameObject.GetComponent<ButtonsSound> ();
			if (bs != null)
				bs.enabled = false;
			return;
		}

		costLabel.text = data.cost.ToString ();
		characterSprite.spriteName = data.name;
	}

	private float createTime;
	void Update()
	{
		if (characterData == null)
			return;

		if (createTime > 0f) {
			createTime -= Time.deltaTime;
		}

		if (createTime > 0f) {
			gaugeFrameSprite.gameObject.SetActive (true);
			createCoolSprite.gameObject.SetActive (true);
			createCoolSprite.fillAmount = createTime / currentCool;
			disableSprite.gameObject.SetActive (true);
			//blingObj.SetActive (false);
		} else {
			gaugeFrameSprite.gameObject.SetActive (false);
			createCoolSprite.gameObject.SetActive (false);
			disableSprite.gameObject.SetActive (characterData.cost > BattleManager.Instance.playerTowerMoneyCreator.currentMoney);
			//blingObj.SetActive (characterData.cost <= BattleManager.Instance.playerTower.currentMoney);
		}
	}

    private float currentCool;
	public void ClickCreateCharacter()
	{
		if (characterData == null)
			return; 
		if (disableSprite.gameObject.activeSelf)
			return;

		SoundManager.Instance.PlayEffect (this.gameObject,"landing");
        currentCool = characterData.createCool * BattleManager.Instance.GetCreateTime(characterData.id);
        createTime = currentCool;
		BattleManager.Instance.playerTower.CreateUnit (characterData);
	}
}

