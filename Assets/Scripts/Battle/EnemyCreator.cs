﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyCreator : MonoBehaviour
{
	private Tower tower;

	void Start()
	{

	}
	private EnemyCreateData data;
    private int cnt = 0;

	private EnemyPattern[] patternData;
	public void SetData(Tower tower, EnemyPattern[] data)
	{
		this.tower = tower;
		this.patternData = data;
		StartCoroutine (CreateEnemy ());
	}

	private IEnumerator CreateEnemy()
	{
		float time = 0f;
		int idx = 0;
		while (idx < patternData.Length) {
			if (time > patternData [idx].time) {
				tower.CreateUnit (BattleManager.Instance.characterDataEnemy[patternData[idx].enemyId]);
				idx++;
			}
			yield return new WaitForEndOfFrame ();
			time += Time.deltaTime;
		}
		StartCoroutine(CreateEnemy ());
	}

	public void SetData(Tower tower, EnemyCreateData data)
	{
		this.data = data;
		Debug.Log (data.termMin + "//" + data.termMax);
		this.tower = tower;
        StartCoroutine(Create(data.initTime));
    }

	IEnumerator Create(float term)
	{
        if (cnt == data.createNum)
        {
            term += data.delay;
            cnt = 0;
        }

        yield return new WaitForSeconds (term);

		tower.CreateUnit (BattleManager.Instance.characterDataEnemy[data.GetId]);
		StartCoroutine(Create (Random.Range (data.termMin, data.termMax)));
        cnt++;
    }
}

