﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TowerMoneyCreator : MonoBehaviour
{
	public int currentMaxMoney;
	public int currentMoney;

	public int level;
	public int maxLevel;

	[SerializeField]
	private UILabel moneyCurrentLabel;
	[SerializeField]
	private BlingUIWidget moneyCurrentBling;
	[SerializeField]
	private UILabel moneyMaxLabel;
	[SerializeField]
	private BlingUIWidget moneyMaxBling;

	// Use this for initialization
	void Start ()
	{
		
	}

	public void LevelUp(float moneySpeed,int maxMoney)
	{
		currentMoneySpeed = moneySpeed;
		currentMaxMoney = maxMoney;
		moneyMaxBling.enabled = true;
		CancelInvoke ("FinishMaxBling");
		Invoke ("FinishMaxBling", 1f);
		UpdateLabel ();
	}

	private float moneyTime;
	private float currentMoneySpeed;

	void Update ()
	{
		if (!BattleManager.Instance.IsInit)
			return;

		if (!BattleManager.Instance.isPlay)
			return;

		if (currentMoney < currentMaxMoney) {
			moneyTime += Time.deltaTime;
			if (moneyTime > currentMoneySpeed) {
				int money = (int)(moneyTime / currentMoneySpeed);
				moneyTime = moneyTime%currentMoneySpeed;
				ChargeMoney (money);
			}
		}
	}

	private void UpdateLabel()
	{
		moneyCurrentLabel.text = currentMoney.ToString ();
		moneyMaxLabel.text = currentMaxMoney.ToString ();
	}

	public void ChargeMoney(int money)
	{
		if (!BattleManager.Instance.isPlay)
			return;
		currentMoney += money;
		moneyCurrentBling.enabled = true;
		CancelInvoke ("FinishCurrentBling");
		Invoke ("FinishCurrentBling", 1f);
		if (currentMoney >= currentMaxMoney) {
			currentMoney = currentMaxMoney;
			moneyTime = 0;
		}
		UpdateLabel ();
	}

	private void FinishCurrentBling()
	{
		moneyCurrentBling.enabled = false;
	}

	private void FinishMaxBling()
	{
		moneyMaxBling.enabled = false;
	}

	[SerializeField]
	private bool moneyTween = false;
	private Tweener labelTweener; 
}

