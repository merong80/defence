﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Health : MonoBehaviour
{
	private Character character;
	private Tower tower;
    [SerializeField]
    private UISprite frameSprite;
    [SerializeField]
	private UISprite gaugeSprite;
	[SerializeField]
	private UILabel hpLabel;

	private int currentHp;
	private int maxHp;
    [SerializeField]
	private float hideTime;

	void Awake()
	{
		character = gameObject.GetComponent<Character> ();
		tower = gameObject.GetComponent<Tower> ();
	}

	public void Init(int hp)
	{
		currentHp = hp;
		maxHp = hp;
		UpdateState ();
		if( hideTime > 0 ) HideObj ();
	}

	private void ShowObj()
	{
        if (frameSprite != null)
        {
            frameSprite.gameObject.SetActive(true);
        }
        if (gaugeSprite != null) {
			gaugeSprite.gameObject.SetActive (true);
		}
		if (hpLabel != null) {
			hpLabel.gameObject.SetActive (true);
		}
	}

	private void HideObj()
	{
        if (frameSprite != null)
        {
            frameSprite.gameObject.SetActive(false);
        }
        if (gaugeSprite != null) {
			gaugeSprite.gameObject.SetActive (false);
		}
		if (hpLabel != null) {
			hpLabel.gameObject.SetActive (false);
		}
	}

	private void UpdateState()
	{
		if (gaugeSprite != null) {
			gaugeSprite.fillAmount = (float)currentHp / maxHp;
		}
		if (hpLabel != null) {
			hpLabel.text = currentHp + "/" + maxHp;
		}
	}

	public void Damage(int damage)
	{
		Damage(damage,Vector2.zero,Vector2.zero);
	}

	public bool invincible = false;
	public void Damage(int damage, Vector2 force, Vector2 finishForce)
	{
		if (invincible || currentHp <= 0)
			return;
		
		currentHp -= damage;

		if (currentHp <= 0) {
            CancelInvoke("HideObj");
            HideObj();
            currentHp = 0;
			EventDelegate.Execute (dieListener);
			if (character != null) {
				character.Die (finishForce);
			}
			if (tower != null) {

			}
		} else {
            if (hideTime > 0)
            {
                ShowObj();
                CancelInvoke("HideObj");
                Invoke("HideObj", hideTime);
            }
            if (character != null){
				character.Hit (damage, force);
			}
			if (tower != null) {
				tower.Hit(Mathf.FloorToInt ((float)currentHp/maxHp*100f));
			}
		}
		UpdateState ();
	}
	public List<EventDelegate> dieListener = new List<EventDelegate>();
}

