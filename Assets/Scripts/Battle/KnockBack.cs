﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class KnockBack : MonoBehaviour
{
	private Character character;
	private Health health;

	void Start()
	{
		if (character == null)
			character = gameObject.GetComponent<Character> ();
		if (health != null)
			health = gameObject.GetComponent<Health> ();
	}
	[SerializeField]
	private float knockbackDuration;
	[SerializeField]
	private Ease knockbackEaseX;
	[SerializeField]
	private Ease knockbackEaseY;
	[SerializeField]
	private Vector2 defaultKnockbackForce;

	public void KnockBackAnimation()
	{
		KnockBackAnimation (defaultKnockbackForce);
	}

	private bool rotating = false;
	private float rotatingTime;
	private float rotatingSpeed = 100f;
	private const float rotatingDefaultSpeed = 10f;

	public void DieKnockBack(Vector2 force)
	{
		if (force != Vector2.zero) {
			rotating = true;
			rotatingTime = 0f;//transform.rotate
			rotatingSpeed = force.magnitude*rotatingDefaultSpeed;
            //transform.DORotate (new Vector3 (0f, 0f, character.faceDirection == FaceDirection.LEFT ? -90f : 90f), knockbackDuration * 0.2f).SetEase (Ease.OutSine);
            character.FadeOut(knockbackDuration);
			KnockBackAnimation (force);
		} else {
            character.FadeOut(knockbackDuration);
            transform.DORotate (new Vector3(0f,0f,character.faceDirection == FaceDirection.LEFT ? -90f : 90f),knockbackDuration*0.2f).SetEase(Ease.OutSine);
			KnockBackAnimation (defaultKnockbackForce);
		}
	}

	void Update()
	{
		if (rotating) {
			if (!gameObject.activeSelf) {
				rotating = false;
				//transform.Rotate (Vector3.forward,Vector3.zero);
				transform.localEulerAngles = Vector3.zero;
			} else {
				transform.Rotate (Vector3.forward, character.faceDirection == FaceDirection.LEFT ? -rotatingSpeed * Time.deltaTime : rotatingSpeed * Time.deltaTime);
				rotatingTime += Time.deltaTime;
				if (rotatingTime >= knockbackDuration) {
					rotating = false;
					//transform.Rotate (Vector3.forward,Vector3.zero);
					transform.localEulerAngles = Vector3.zero;
				}
			}
		}
		
	}

    private Tweener tweenerX;
    private Tweener tweenerY;
    private bool isKnockback = false;
    public void KnockBackAnimation(Vector2 force)
	{
        if (isKnockback) return;
		if (health != null)
			health.invincible = true;

        isKnockback = true;

		tweenerX = transform.DOLocalMoveX (character.faceDirection == FaceDirection.LEFT ? force.x : -force.x, knockbackDuration).SetRelative (true).OnUpdate(delegate() {
            if( tweenerX != null ) tweenerX.timeScale = Time.timeScale;
        }).SetEase(knockbackEaseX);
		tweenerY = transform.DOLocalMoveY (force.y,knockbackDuration*0.4f).SetRelative (true).SetEase(Ease.OutQuad).OnUpdate(delegate () {
            if (tweenerY != null) tweenerY.timeScale = Time.timeScale;
        }).OnComplete(delegate()
			{
				tweenerY = transform.DOLocalMoveY (-force.y,knockbackDuration*0.7f).SetRelative (true).SetEase(Ease.OutBounce).OnUpdate(delegate () {
                    if (tweenerY != null) tweenerY.timeScale = Time.timeScale;
                }).OnComplete (delegate() {
					if (health != null)
						health.invincible = false;
					character.KnockbackComplete ();

                    isKnockback = false;
				});
			});
	}

	public void Pause()
	{
		if (tweenerX != null)
			tweenerX.TogglePause ();
		if (tweenerY != null) {
			tweenerY.TogglePause ();
		}
	}
}

