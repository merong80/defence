﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class FlashEffect : BattleEffect
{
	[SerializeField]
	private float initTime;
	[SerializeField]
	private Ease initEase;
	[SerializeField]
	private float showDuration;
	[SerializeField]
	private float hideTime;
	[SerializeField]
	private Ease hideEase;

	[SerializeField]
	private UISprite sprite;
	public override void StartEffect ()
	{
		base.StartEffect ();

		DOTween.To ( () => sprite.alpha, x => sprite.alpha = x, 1f, initTime).SetEase (initEase).OnComplete (delegate() {
			DOTween.To ( () => sprite.alpha, x => sprite.alpha = x, 0f, hideTime).SetEase (hideEase).SetDelay (showDuration).OnComplete (delegate() {
				CompleteEffect (); 
			});
		});
	}
}

