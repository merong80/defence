﻿using UnityEngine;
using System.Collections;

public class BattleEffect : MonoBehaviour
{
	public bool managed = true;
	[SerializeField]
	protected bool randomRotate = false;

	//public void StartEffect(bool flip

	public virtual void StartEffect()
	{
		StartEffect (false);
	}

	public virtual void StartEffect(bool flip)
	{
		gameObject.SetActive (true);
		if (randomRotate)
			transform.Rotate (Vector3.forward, Random.Range (0f, 360f));
	}

	public virtual void CompleteEffect()
	{
		gameObject.SetActive (false);
		if( managed )
			BattleManager.Instance.effectPooler.Put (this);
	}

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

