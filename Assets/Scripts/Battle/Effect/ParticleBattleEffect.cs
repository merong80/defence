﻿using UnityEngine;
using System.Collections;

public class ParticleBattleEffect : BattleEffect
{
	private ParticleSystem particle;
	public override void StartEffect (bool flip)
	{
		if (particle == null)
			particle = gameObject.GetComponent<ParticleSystem> ();

		base.StartEffect (flip);
		particle.Play ();

		StartCoroutine (CheckComplete ());
	}

	private IEnumerator CheckComplete()
	{
		while (particle.isPlaying) {
			yield return new WaitForEndOfFrame ();
		}
		CompleteEffect ();
	}
}

