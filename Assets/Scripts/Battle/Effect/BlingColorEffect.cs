﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BlingColorEffect : MonoBehaviour
{
    private Tweener tweener;
    [SerializeField]
    private float duration;
    [SerializeField]
    private Color color1;
    [SerializeField]
    private Color color2;
    [SerializeField]
    private Ease ease;
    
    private void OnEnable()
    {
        if (tweener != null)
        {
            tweener.Kill();
        }
        Bling();
    }
    private UISprite sprite;
    private void Start()
    {
        if (sprite == null) sprite = gameObject.GetComponent<UISprite>();
        if (tweener != null)
        {
            tweener.Kill();
        }
        Bling();
    }

    private void Bling()
    {
        tweener = DOTween.To( () => sprite.color, x => sprite.color = x, color1, duration ).SetEase(ease).OnComplete(delegate ()
        {
            tweener = DOTween.To( () => sprite.color, x => sprite.color = x, color2, duration).SetEase(ease).OnComplete(delegate () { Bling(); });
        });
    }

    private void OnDisable()
    {
        if (tweener != null)
        {
            tweener.Kill();
        }
    }
}
