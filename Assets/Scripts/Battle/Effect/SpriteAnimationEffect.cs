﻿using UnityEngine;
using System.Collections;

public class SpriteAnimationEffect : BattleEffect
{
	private UISprite sprite;
	private UISpriteAnimation ani;
	public override void StartEffect (bool flip)
	{
		base.StartEffect (flip);

		if (ani == null)
			ani = transform.GetComponentInChildren<UISpriteAnimation> ();
		if (sprite == null)
			sprite = transform.GetComponentInChildren<UISprite> ();
		
		sprite.flip = flip ? UIBasicSprite.Flip.Horizontally : UIBasicSprite.Flip.Nothing;
		ani.ResetToBeginning ();
		ani.Play ();

		StartCoroutine (CheckComplete ());
	}

	private IEnumerator CheckComplete()
	{
		while (ani.isPlaying) {
			yield return new WaitForEndOfFrame ();
		}
		CompleteEffect ();
	}
}

