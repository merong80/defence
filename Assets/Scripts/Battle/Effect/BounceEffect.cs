﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BounceEffect : MonoBehaviour
{
    private Tweener tweener;
    [SerializeField]
    private float riseDuration;
    [SerializeField]
    private float riseHeight;
    [SerializeField]
    private Ease riseEase;
    [SerializeField]
    private float downDuration;
    [SerializeField]
    private Ease downEase;

    private void OnEnable()
    {
        if (tweener != null)
        {
            tweener.Kill();
            transform.localPosition = defaultPosition;
        }
        Bounce();
    }
    private Vector3 defaultPosition;
    private void Start()
    {
        defaultPosition = transform.localPosition;

        if (tweener != null)
        {
            tweener.Kill();
            transform.localPosition = defaultPosition;
        }
        Bounce();
    }

    private void Bounce()
    {
        tweener = transform.DOLocalMoveY(riseHeight, riseDuration).SetRelative(true).SetEase(riseEase).OnComplete(delegate ()
        {
            tweener = transform.DOLocalMoveY(-riseHeight, downDuration).SetRelative(true).SetEase(downEase).OnComplete(delegate() { Bounce(); });
        });
    }

    private void OnDisable()
    {
        if (tweener != null)
        {
            tweener.Kill();
            transform.localPosition = defaultPosition;
        }
    }
}
