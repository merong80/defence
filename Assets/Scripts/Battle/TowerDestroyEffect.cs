﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TowerDestroyEffect : MonoBehaviour
{
	[SerializeField]
	private UISprite towerSprite;
	[SerializeField]
	private ParticleSystem lightningParticle;
	[SerializeField]
	private ParticleSystem smokeParticle;
	[SerializeField]
	private Com.LuisPedroFonseca.ProCamera2D.ProCamera2DCinematics cinematicCam;
	[SerializeField]
	private Transform cinematicTargetTransform;
	[SerializeField]
	private float cinematicHoldDuraion;
	[SerializeField]
	private float cinematicZoom;
	public void StartEffect(Tower tower)
	{
		//lightningParticle.gameObject.SetActive (false);
		lightningParticle.Stop ();
		smokeParticle.gameObject.SetActive (true);
		smokeParticle.Play ();
		cinematicCam.AddCinematicTarget (cinematicTargetTransform,holdDuration:cinematicHoldDuraion,zoom:cinematicZoom);
		cinematicCam.Play ();

		cinematicCam.OnCinematicFinished.AddListener(() =>
			{
				BattleManager.Instance.DefeatTower (tower);
			});

		DOTween.To (() => towerSprite.alpha, x => towerSprite.alpha = x, 0f, 3f);
	}


}

