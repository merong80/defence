﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
	[SerializeField]
	private string explosion;
	private int damage;
	private FaceDirection direction;
	[SerializeField]
	private float speed;
	private bool init = false;
	private Vector2 force;
	private Vector2 finishForce;
	//public 
	public virtual void Init(int damage, Vector3 position, FaceDirection direction, Vector2 force, Vector2 finishForce)
	{
		this.damage = damage;
		this.direction = direction;
		this.force = force;
		this.finishForce = finishForce;
		transform.position = position;
		init = true;
		gameObject.SetActive (true);
	}

	protected virtual void Update()
	{
		if (!init)
			return;

		Vector3 pos = transform.localPosition;
		transform.localPosition = new Vector3 (direction == FaceDirection.RIGHT ? pos.x + speed * Time.deltaTime : pos.x - speed * Time.deltaTime, pos.y, pos.z);
	}
    [SerializeField]
    private string attackEffectPath;
    [SerializeField]
    private Transform attackEffectTransform;
    [SerializeField]
    private string attackSoundPath;

	protected virtual void OnTriggerEnter2D(Collider2D col)
	{
		if (gameObject.tag != col.gameObject.tag) {
			Health health = col.gameObject.GetComponent<Health> ();
			if (health != null) {
				if (string.IsNullOrEmpty (explosion)) {
					if (force != Vector2.zero) {
						health.Damage (damage, force,finishForce);
					} else {
						health.Damage (damage);
					}
                    if (!string.IsNullOrEmpty(attackEffectPath))
                    {
                        BattleEffect effect = BattleManager.Instance.effectPooler.Get(attackEffectPath, transform.parent, attackEffectTransform.localScale, attackEffectTransform.position);
                        effect.StartEffect(true);
                        if (!string.IsNullOrEmpty(attackSoundPath))
                        {
                            SoundManager.Instance.PlayEffect(effect.gameObject, attackSoundPath);
                        }
                    }
                    
                } else {
					Explosion bomb = BattleManager.Instance.explosionPooler.Get (explosion,transform.parent);
					bomb.Boom (transform.position,damage,finishForce);
				}

				Disable ();
			}
		}
	}

	protected void Disable()
	{
		gameObject.SetActive (false);
		init = false;
		BattleManager.Instance.projectilePooler.Put (this);
	}
}

