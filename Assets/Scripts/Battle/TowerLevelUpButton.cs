﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TowerLevelUpButton : MonoBehaviour
{
    public UISprite iconSprite;

    public UILabel currentLevelLabel;
    public UILabel costLabel;
    private bool max = false;

    private bool upgradeEnable;
	[SerializeField]
	private GameObject blingObj;
    void Update()
    {
        if (max)
        {
            return;
        }

        max = BattleManager.Instance.MaxTowerUpgrade();

        bool checkUpgrade = !max && BattleManager.Instance.EnableTowerUpgrade();
        if (!upgradeEnable && checkUpgrade)
        {
            Bling();
        }
        upgradeEnable = checkUpgrade;

		iconSprite.color = upgradeEnable ? Color.white : Color.gray;
		blingObj.SetActive (upgradeEnable);
        if (!upgradeEnable) tweener.Kill();
    }
    [SerializeField]
    private UISprite blingSprite;
    [SerializeField]
    private float blingTerm;

    private Tweener tweener;
    private void Bling()
    {
        tweener = DOTween.To(() => blingSprite.alpha, x => blingSprite.alpha = x, 0f, blingTerm).OnComplete(delegate ()
        {
            tweener = DOTween.To(() => blingSprite.alpha, x => blingSprite.alpha = x, 1f, blingTerm).OnComplete(delegate ()
            {
                Bling();
            });
        });

    }

	public void UpdateButton(int level, int cost)
	{
        iconSprite.color = Color.white;
		currentLevelLabel.text = "Lv."+level.ToString ();
		costLabel.text = cost.ToString ();
        Bling();
	}

	public void MaxUpgraded(int level)
	{
		currentLevelLabel.text = "Lv."+level.ToString ();
		costLabel.text = "MAX";
	}

	public void ClickLevelUp()
	{
		if (max || !BattleManager.Instance.EnableTowerUpgrade ())
			return;

		BattleManager.Instance.UpgradeTowerMoneyLevel ();
		SoundManager.Instance.PlayEffect ("success_buy");
	}
}

