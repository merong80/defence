﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupMessage : Popup {

	public delegate void MessageCallback(bool confirm);

	private MessageCallback messageCallback;

	[SerializeField]
	private UILabel messageLabel;
	[SerializeField]
	private UISprite messageBorder;
	[SerializeField]
	private int messageLabelDefaultHeight;
	[SerializeField]
	private int borderDefaultHeight;
	[SerializeField]
	private GameObject buttonObject;
	[SerializeField]
	private Vector3 buttonDefaultPosition;

	public void SetData(string msg, bool showButton, MessageCallback callback)
	{
		messageCallback = callback;
		messageLabel.text = msg;
		buttonObject.SetActive (showButton);
		if (messageLabel.printedSize.y > messageLabelDefaultHeight) {
			int gap = Mathf.FloorToInt (messageLabel.printedSize.y - messageLabelDefaultHeight);
			messageBorder.height = borderDefaultHeight + gap;
			buttonObject.transform.localPosition = new Vector3 (buttonDefaultPosition.x, buttonDefaultPosition.y + gap, buttonDefaultPosition.z);
		} else {
			messageBorder.height = borderDefaultHeight;
			buttonObject.transform.localPosition = buttonDefaultPosition;
		}
	}
	private bool isYes;
	public void ClickYes()
	{
		isYes = true;
		Close ();
	}

	public void ClickNo()
	{
		isYes = false;
		Close ();
	}

	public override void Close ()
	{
		base.Close ();
		if (messageCallback != null)
			messageCallback (isYes);
		messageCallback = null;
	} 
}
