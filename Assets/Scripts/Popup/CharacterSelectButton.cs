﻿using UnityEngine;
using System.Collections;

public class CharacterSelectButton : MonoBehaviour
{
	[SerializeField]
	private UISprite characterSprite;

	private int characterId;
	private PopupStageSelect selectPopup;
	public void SetData(PopupStageSelect popup, CharacterBattleData data)
	{
		selectPopup = popup;
		if (data == null) {
			characterId = 0;
			characterSprite.gameObject.SetActive (false);
			return;
		}
		characterSprite.gameObject.SetActive (true);
		characterId = data.id;
		characterSprite.spriteName = data.name;
		characterSprite.MakePixelPerfect ();
	}
	public void ClickCharacter()
	{
		if (characterId == 0)
			return;
		selectPopup.ClickCharacter (characterId);
	}
}

