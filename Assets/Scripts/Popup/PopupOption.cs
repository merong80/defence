﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupOption : Popup {

	[SerializeField]
	private GameObject bgmMuteObj;

	void Start()
	{
		bgmMuteObj.SetActive (SoundManager.Instance.GetMute());
	}

    private void OnEnable()
    {
        bgmMuteObj.SetActive(SoundManager.Instance.GetMute());
    }

    public void ClickExit()
	{
        Close ();
		if (BattleManager.HasInstance)
			BattleManager.Instance.GoMain ();
			//GameManager.Instance.LoadLevel (1);
        //StartCoroutine(Exit());
	}

    private IEnumerator Exit()
    {
        yield return StartCoroutine(GameManager.Instance.LoadLevel(1));
        Close();
    }

    public void ClickBGM()
	{
        SoundManager.Instance.Mute();
		bgmMuteObj.SetActive (SoundManager.Instance.GetMute());
	}

	public void ClickSave()
	{
		GPManager.Instance.CreateNewSnapshot ();
	}

	public override void Close ()
	{
		base.Close ();
		if (BattleManager.HasInstance)
			BattleManager.Instance.OnPause ();
	}

	public void ClickTutorial()
	{
		PopupManager.Instance.Open ("tutorial");
	}
}
