﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loading : MonoBehaviour {
    [SerializeField]
    private UILabel loadingLabel;
    [SerializeField]
    private float speed = 0.5f;
    [SerializeField]
    private string[] texts;
    private int idx;
    private float time;

    void Update()
    {
        if(gameObject.activeSelf)
        {
            time += Time.deltaTime;
            if( speed < time)
            {
                time = 0f;
                if (idx >= texts.Length)
                {
                    idx = 0;
                }

                loadingLabel.text = texts[idx];
                idx++;
            }
        }
    }
}
