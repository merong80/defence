﻿using UnityEngine;
using System.Collections;

public class PopupExplore : Popup
{
    [SerializeField]
    private GameObject exploringObjs;
    [SerializeField]
    private UISprite exploringBgSprite;

    [SerializeField]
    private UILabel exploringMessageLabel;
    [SerializeField]
    private UILabel exploringRemainTimeLabel;
    [SerializeField]
    private MainCharacterMoving exploringCharacter;

    private int remainTime;
    private Coroutine checkRemainCoroutine;
    public override void Open()
    {
        base.Open();
        UpdateExplore();
        exploringCharacter.CheckMoving();
    }

    private void UpdateExplore()
    {
        if (!CheckReward())
        {
            remainTime = GameManager.Instance.GetExploreRemainTime();
            if (remainTime > 0)
            {
                exploringObjs.SetActive(true);
                exploringMessageLabel.text = TextManager.Get(TextKey.exploring);
                exploringRemainTimeLabel.text = TextManager.Get(TextKey.remain_time) + " " + Util.GetTimeString(remainTime, "{0:D2}:{1:D2}:{2:D2}");
                if (checkRemainCoroutine != null) StopCoroutine(checkRemainCoroutine);
                checkRemainCoroutine = StartCoroutine(CheckRemainTime());
            }
            else
            {
                exploringObjs.SetActive(false);
            }
        }
    }

    private bool CheckReward()
    {
        RewardData rewardData = GameManager.Instance.GetExploreRewardData();
        if (rewardData != null)
        {
            exploringObjs.SetActive(false);

            PopupRewardMessage rewardPopup = (PopupRewardMessage)PopupManager.Instance.Open("reward_message");
            rewardPopup.SetData(TextManager.Get(TextKey.explore_complete), rewardData);
        }
        return rewardData != null;
    }

    private IEnumerator CheckRemainTime()
    {
        while( remainTime > 0 )
        {
            yield return new WaitForSeconds(1f);
            remainTime -= 1; //GameManager.Instance.GetExploreRemainTime();
            exploringRemainTimeLabel.text = TextManager.Get(TextKey.remain_time) + " " + Util.GetTimeString(remainTime, "{0:D2}:{1:D2}:{2:D2}");
        }

        CheckReward();

    }

    public void ClickExplore(GameObject obj)
    {
        string[] strs = obj.name.Split('_');
        int id = int.Parse(strs[1]);
        ExploreData data = GameManager.Instance.exploreDatas[id];
        if( data.unlockStage > GameManager.Instance.ClearStage )
        {
            return;
        }
        PopupManager.Instance.Message(string.Format(TextManager.Get(TextKey.ask_explore), TextManager.Get(TextKey.explore_area_name + "_" + data.area), string.Format(TextManager.Get(TextKey.hour), data.hour)), true,
            delegate (bool isConfirm)
            {
                if(isConfirm)
                {
                    GameManager.Instance.StartExplore(id);
                    UpdateExplore();
                }
            });
    }

    public override void Close()
    {
        if (checkRemainCoroutine != null) StopCoroutine(checkRemainCoroutine);
        base.Close();
    }
}
