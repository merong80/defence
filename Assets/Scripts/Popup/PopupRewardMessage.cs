﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopupRewardMessage : Popup
{
    [SerializeField]
    private UILabel messageLabel;

    [SerializeField]
    private PopupRewardList list;

    private RewardData rewardData;
    public void SetData(string message, RewardData data)
    {
        rewardData = data;
        messageLabel.text = message;
        List<RewardItemData> itemDatas = new List<RewardItemData>();
        if( data.coin > 0 )
        {
            RewardItemData coinData = new RewardItemData();
            coinData.type = RewardType.COIN;
            coinData.amount = data.coin;

            itemDatas.Add(coinData);
        }


        foreach(KeyValuePair<int,int> pair in data.cards)
        {
            RewardItemData cardData = new RewardItemData();
            cardData.type = RewardType.CARDS;
            cardData.id = pair.Key;
            cardData.amount = pair.Value;
            itemDatas.Add(cardData);
        }

        list.UpdateList(itemDatas);
    }

    public void ClickGetReward()
    {
        //GameManager.Instance.ChargeExploreRewardData();
        GameManager.Instance.ChargeRewardData(rewardData);

        Close();
    }

    public override void Close()
    {
        base.Close();
        if(PopupManager.Instance.GetCurrentPopup().GetType() == typeof(PopupRoulette))
        {
            PopupManager.Instance.ClosePopup();
            GameManager.Instance.RewardRoulette();
        }
        else
        {
            GameManager.Instance.ResetPlayerExploreRewardData();

        }
    }
}
