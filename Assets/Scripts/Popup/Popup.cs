﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum PopupShowType
{
    NONE = 0,
    MOVING = 1,
    SIZE = 2,
    ALPHA = 3,
}

public class Popup : MonoBehaviour
{
    private UIPanel panel;
    private UIPanel[] scrollPanels;

    [SerializeField]
    private Transform tweenTransform;
    [SerializeField]
    private PopupShowType showType;
    [SerializeField]
    private float showDuration;
    [SerializeField]
    private Ease showEase;
    [SerializeField]
    private Vector2 showInitValue;

    [SerializeField]
    private PopupShowType hideType;
    [SerializeField]
    private float hideDuration;
    [SerializeField]
    private Ease hideEase;
    [SerializeField]
    private Vector2 hideCompleteValue;

    private bool init = false;

    public void SetDepth(int depth)
    {
        if (!init)
            Init();
        if (panel != null) panel.depth = depth;
        for (int i = 0; i < scrollPanels.Length; i++)
        {
            scrollPanels[i].depth = (depth + i + 1);
        }

    }

    private void Init()
    {
        init = true;
        if (tweenTransform == null)
            tweenTransform = transform;

        panel = gameObject.GetComponent<UIPanel>();
        scrollPanels = gameObject.GetComponentsInChildren<UIPanel>();
    }

    public virtual void Open()
    {
        if (!init)
            Init();


        switch (showType)
        {
            case PopupShowType.MOVING:

                tweenTransform.localPosition = showInitValue;
                tweenTransform.DOLocalMove(Vector3.zero, showDuration).SetEase(showEase).OnComplete(delegate () {
                    PopupManager.Instance.OpenComplete(this);
                }).timeScale = 1f;
                break;
            case PopupShowType.SIZE:
                tweenTransform.localScale = showInitValue;
                tweenTransform.DOScale(Vector3.one, showDuration).SetEase(showEase).OnComplete(delegate () {
                    PopupManager.Instance.OpenComplete(this);
                }).timeScale = 1f;
                break;
            default:
                PopupManager.Instance.OpenComplete(this);
                break;
        }
    }

    public virtual void UpdatePopup()
    {

    }

    public virtual void Close()
    {
        switch (hideType)
        {
            case PopupShowType.MOVING:
                tweenTransform.DOLocalMove(hideCompleteValue, hideDuration).SetEase(hideEase).OnComplete(delegate () {
                    CloseComplete();
                }).timeScale = 1f;
                break;
            case PopupShowType.SIZE:
                tweenTransform.DOScale(hideCompleteValue, hideDuration).SetEase(hideEase).OnComplete(delegate () {
                    CloseComplete();
                    //PopupManager.Instance.CloseComplete (this);
                }).timeScale = 1f;
                break;
            default:
                CloseComplete();
                //PopupManager.Instance.CloseComplete (this);
                break;
        }
    }

    protected virtual void CloseComplete()
    {
        PopupManager.Instance.CloseComplete(this);
    }
}
