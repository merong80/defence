﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopupStageSelect : Popup
{
	//[SerializeField]
	//private GameObject scrollObj;
	[SerializeField]
	private UIScrollView scroll;
	[SerializeField]
	private UIWrapContent wrapContent;
	[SerializeField]
	private UICenterOnChild centerOnChild;


	[SerializeField]
	private int stageListWidth = 280;
	[SerializeField]
	private UIScrollBar scrollBar;
	private const int maxListItemNum = 5;

	[SerializeField]
	private GameObject stageSelectItemObj;

	[SerializeField]
	private GameObject startLabelObject;

	void Start()
	{
		//InitializeScroll ();
		wrapContent.onInitializeItem = delegate(GameObject go, int wrapIndex, int realIndex) {
			go.GetComponent<StageSelectListItem> ().SetData (realIndex-wrapContent.minIndex+1);
		};

		centerOnChild.onCenter = delegate(GameObject centeredObject) {
			if( currentStageListItem != null ) currentStageListItem.Active (false);
			currentStageListItem = centeredObject.GetComponent<StageSelectListItem> ();
			currentStageListItem.Active (true);
		};
	}

	/*private void InitializeScroll()
	{
		GameObject scrollGo = Instantiate (scrollObj, transform);
		scrollGo.transform.localPosition = Vector3.zero;
		scrollGo.transform.localScale = Vector3.one;
		scroll = scrollGo.GetComponent<UIScrollView> ();
		wrapContent = scrollGo.GetComponentInChildren<UIWrapContent> ();
		centerOnChild = scrollGo.GetComponentInChildren<UICenterOnChild> ();

		centerOnChild.enabled = false;
		wrapContent.onInitializeItem = delegate(GameObject go, int wrapIndex, int realIndex) {
			go.GetComponent<StageSelectListItem> ().SetData (realIndex-wrapContent.minIndex+1);
		};

		centerOnChild.onCenter = delegate(GameObject centeredObject) {
			if( currentStageListItem != null ) currentStageListItem.Active (false);
			currentStageListItem = centeredObject.GetComponent<StageSelectListItem> ();
			currentStageListItem.Active (true);
		};
	}*/

	void Update()
	{
		startLabelObject.SetActive (!scroll.isDragging);
	}

    public override void Open()
    {
        base.Open();
        UpdateStageList();
        InitCharacter();
    }

	private int clearStage = -1;
	private int currentStage = -1;

	private void UpdateStageList()
	{
		if (GameManager.Instance.ClearStage == clearStage)
			return;
		//scroll.enabled = false;
		clearStage = GameManager.Instance.ClearStage;
		currentStage = GameManager.Instance.CurrentStage;
		int totalStage = clearStage + 1;

		int needChild = maxListItemNum > totalStage ? totalStage : maxListItemNum;

		//List<Transform> removeLists = new List<Transform> ();//int currentChild = wrapContent.transform.childCount;
		int childNum = wrapContent.transform.childCount;
		//for (int i = 0; i < childNum; i++) {
		//	Destroy(contentTransform.GetChild (0).gameObject);
		//}
		//childNum = 0;
		//InitializeScroll ();
		int totalCurrentGap = totalStage - currentStage;


		wrapContent.minIndex = (totalStage-currentStage)-totalStage+1;
		wrapContent.maxIndex = totalStage+wrapContent.minIndex-1;
		for (int i = childNum; i < needChild; i++) {
			GameObject go = Instantiate (stageSelectItemObj, wrapContent.transform);
			go.transform.localScale = Vector3.one;
		}

		scroll.MoveAbsolute (Vector3.zero);
		wrapContent.SortBasedOnScrollMovement ();
		//wrapContent.WrapContent ();
		//float moveX = wrapContent.transform.childCount%2 > 0 ? -wrapContent.itemSize*((totalStage-currentStage)-wrapContent.transform.childCount%2+1) : -wrapContent.itemSize*((totalStage-currentStage));
		StartCoroutine (SortingScroll (totalCurrentGap,currentStage));

	}

	private StageSelectListItem currentStageListItem = null;

	private IEnumerator SortingScroll(int moreCount, int idx)
	{
		Debug.Log ("SortingScroll   " + moreCount);
		yield return new WaitForEndOfFrame ();

		List<Transform> moveTransform = new List<Transform> ();
		for (int i = 0; i < wrapContent.transform.childCount; i++) {
			Debug.Log (wrapContent.transform.GetChild (i).localPosition.x);
			if (wrapContent.transform.GetChild (i).localPosition.x > moreCount * wrapContent.itemSize) {
				moveTransform.Add(wrapContent.transform.GetChild(i));
			}
		}
		Debug.Log (moveTransform.Count);
		//int overNum = Mathf.FloorToInt (wrapContent.transform.childCount * 0.5f);
		int overNum = (wrapContent.transform.childCount-moveTransform.Count)-moreCount;
		for (int i = 0; i < moveTransform.Count; i++) {
			
			moveTransform [i].localPosition = new Vector3 (-(i+overNum) * wrapContent.itemSize, 0, 0);
			Debug.Log (i + "//" + (-(i + overNum) * wrapContent.itemSize));
			moveTransform [i].GetComponent<StageSelectListItem> ().SetData (idx-(i +overNum));
		}
		scroll.enabled = clearStage != 0;
		centerOnChild.enabled = true;
		centerOnChild.Recenter ();
	}

	private Transform GetLeftTransform()
	{
		for (int i = 0; i < wrapContent.transform.childCount; i++) {
			if (centerOnChild.centeredObject.transform.localPosition.x - wrapContent.transform.GetChild (i).localPosition.x == wrapContent.itemSize)
				return wrapContent.transform.GetChild (i);
		}

		return null;
	}
	public void ClickArrowStageLeft()
	{
		Transform leftTransform = GetLeftTransform ();
		if( leftTransform != null )
			centerOnChild.CenterOn (leftTransform);

	}

	private Transform GetRightTransform()
	{
		for (int i = 0; i < wrapContent.transform.childCount; i++) {
			if (wrapContent.transform.GetChild (i).localPosition.x - centerOnChild.centeredObject.transform.localPosition.x == wrapContent.itemSize)
				return wrapContent.transform.GetChild (i);
		}

		return null;
	}
	public void ClickArrowStageRight()
	{
		Transform rightTransform = GetRightTransform ();
		if( rightTransform != null )
			centerOnChild.CenterOn (rightTransform);

	}

	[SerializeField]
	private UIGrid characterSelectGrid;
	[SerializeField]
	private CharacterSelectButton[] characterButtons;

	[SerializeField]
	private GameObject characterListObj;

	private List<StageCharacterListItem> characterListItems = new List<StageCharacterListItem>();

	public void InitCharacter()
	{
		Dictionary<int, CharacterBattleData> characterDatas = GameManager.Instance.GetPlayerCharacterData ();
		int listCount = characterListItems.Count;
		if (characterDatas.Count > listCount) {
			for (int i = listCount; i < characterDatas.Count; i++) {
				GameObject go = Instantiate (characterListObj, characterSelectGrid.transform);
				go.transform.localScale = Vector3.one;
				characterListItems.Add(go.GetComponent<StageCharacterListItem> ());
			}
			characterSelectGrid.Reposition ();
		}
		int idx = 0;
		foreach (KeyValuePair<int, CharacterBattleData> pair in characterDatas) {
			characterListItems [idx].SetData (this, pair.Value);
			idx++;
		}

		List<int> currentBattleCharacter = GameManager.Instance.GetCurrentBattleCahracter ();
		for (int i = 0; i < characterButtons.Length; i++) {
			characterButtons[i].SetData (this, currentBattleCharacter.Count > i ? GameManager.Instance.GetCharacterBattleData (currentBattleCharacter[i]) : null);
		}
	}

	public void ClickCharacter(int id)
	{
		//GameManager.Instance.SetCurrentBattleCharacter (id);
		List<int> currentBattleCharacter = GameManager.Instance.GetCurrentBattleCahracter ();
		if (currentBattleCharacter.Contains (id))
        {
            if( currentBattleCharacter.Count == 1 )
            {
                PopupManager.Instance.Message(TextManager.Get(TextKey.no_battle_character));
            }
            else
            {
                GameManager.Instance.RemoveCurrentBattleCharacter(id);
            }
        }
		else
			GameManager.Instance.SetCurrentBattleCharacter (id);

		InitCharacter ();
	}

	public void StartStage(int stage)
	{
		if (GameManager.Instance.GetCurrentBattleCahracter ().Count == 0) {
			PopupManager.Instance.Message (TextManager.Get(TextKey.no_battle_character));
			return;
		}
		if (GameManager.Instance.GetStageData (stage) == null || GameManager.Instance.GetEnemyPatterns(stage) == null) {
			PopupManager.Instance.Message (TextManager.Get(TextKey.no_stage));
			return;
		}

		GameManager.Instance.CurrentStage = stage;
        StartCoroutine(GoBattle());
	}

    private IEnumerator GoBattle()
    {
        yield return StartCoroutine(GameManager.Instance.LoadLevel(2));
        Close();
    }

    public override void Close()
    {
        if (MainManager.HasInstance)
            MainManager.Instance.UpdateMovingCharacter();

        base.Close();
        
    }

	protected override void CloseComplete ()
	{
		base.CloseComplete ();
		//Destroy (scroll.gameObject);
	}
}

