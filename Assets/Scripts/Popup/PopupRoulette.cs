﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class PopupRoulette : Popup
{
    [SerializeField]
    private PopupRewardListItem[] rewardListItems;
    [SerializeField]
    private int[] coins;
    [SerializeField]
    private Transform rouletteTransform;
    [SerializeField]
    private int spinNum = 100;
    [SerializeField]
    private float spinDuration = 5f;
    [SerializeField]
    private Ease spinEase = Ease.Linear;

    public override void Open()
    {
        base.Open();
        List<RewardItemData> rewardDatas = new List<RewardItemData>();
        for (int i = 0; i < coins.Length; i++)
        {
            RewardItemData coinData = new RewardItemData();
            coinData.type = RewardType.COIN;
            coinData.amount = coins[i];
            rewardDatas.Add(coinData);
        }

        for (int i = coins.Length; i < rewardListItems.Length; i++)
        {
            RewardItemData cardData = new RewardItemData();
            cardData.type = RewardType.CARDS;
            cardData.id = Random.Range(101, 112);
            cardData.amount = Random.Range(1, 4);
            rewardDatas.Insert(Random.Range(0,rewardDatas.Count),cardData);
        }

        for (int i = 0; i < rewardListItems.Length; i++)
        {
            rewardListItems[i].SetData(rewardDatas[i]);
        }
        rewardIdx = Random.Range(0, rewardDatas.Count);
        //rewardData = rewardDatas[rewardIdx];
        //rewardData = rewardListItems[rewardIdx].GetData();
        isSpin = false;
    }

    //private RewardItemData rewardData;
    private int rewardIdx;

    private bool isSpin = false;
    public void Spin()
    {
        if (isSpin) return;
        isSpin = true;
        rouletteTransform.localEulerAngles = Vector3.zero;
        float rotation = 360f * spinNum + 45f * rewardIdx;
        RewardItemData rewardData = rewardListItems[rewardIdx].GetData();
        Debug.Log(rewardIdx + "//" + rotation);

        rouletteTransform.DORotate(new Vector3(0f, 0f, rotation), spinDuration,RotateMode.FastBeyond360).SetEase(spinEase).OnComplete(delegate()
        {
            PopupRewardMessage popup = (PopupRewardMessage)PopupManager.Instance.Open("reward_message");
            RewardData data = new RewardData();

            if( rewardData.type == RewardType.COIN )
            {
                data.coin = rewardData.amount;
            }
            else
            {
                data.cards.Add(rewardData.id, rewardData.amount);
            }
            popup.SetData(TextManager.Get(TextKey.roulette_reward_message), data);
        });
    }


}
