﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupTutorial : Popup {

	[SerializeField]
	private Texture2D[] contents;
	[SerializeField]
	private UITexture contentsTexture;
	[SerializeField]
	private UILabel contentsLabel;

	private int index;
	private const string TEXTKEY = "TUTORIAL_";
	public override void Open ()
	{
		base.Open ();
		SetTutorial (index);
	}

	public void NextTutorial()
	{
		index += 1;
		if (index >= contents.Length) {
			index = 0;
			Close ();
		}
		else
			SetTutorial (index);
	}

	private void SetTutorial(int idx)
	{
		contentsTexture.mainTexture = contents [idx];
		contentsLabel.text = TextManager.Get (TEXTKEY + idx);
	}
}
