﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExploreArea : MonoBehaviour
{
    [SerializeField]
    private int area;

    [SerializeField]
    private UILabel areaNameLabel;
    [SerializeField]
    private UILabel[] buttonLabels;
    [SerializeField]
    private UISprite[] rewardSprites;

    [SerializeField]
    private GameObject lockObj;
    [SerializeField]
    private UILabel lockCondition;

    void Start()
    {
        //areaNameLabel.text = TextManager.Get(TextKey.explore_area_name+"_"+area);
        List<ExploreData> datas = GameManager.Instance.GetExploreData(area);


        for( int i = 0; i < buttonLabels.Length; i++ )
        {
            if (datas.Count > i)
            {
                buttonLabels[i].transform.parent.gameObject.SetActive(true);
                buttonLabels[i].text = string.Format(TextManager.Get(TextKey.hour), datas[i].hour);
            }
            else buttonLabels[i].transform.parent.gameObject.SetActive(false);
        }
        if( datas.Count > 0 )
        {
            Debug.Log("ExploreArea   " + area + "//" + GameManager.Instance.ClearStage + "//"+datas[0].unlockStage);
           if( GameManager.Instance.ClearStage < datas[0].unlockStage )
            {
                lockObj.SetActive(true);
                lockCondition.text = string.Format(TextManager.Get(TextKey.explore_area_unlock_condition), datas[0].unlockStage);
            }
           else
            {
                lockObj.SetActive(false);
            }
            for (int i = 0; i < rewardSprites.Length; i++)
            {
                if (datas[0].drop.Length > i)
                {
                    rewardSprites[i].gameObject.SetActive(true);
                    rewardSprites[i].spriteName = string.Format("card_{0:D2}",datas[0].drop[i]);
                }
                else
                {
                    rewardSprites[i].gameObject.SetActive(false);
                }
            }
        }
        else
        {
            for (int i = 0; i < rewardSprites.Length; i++)
            {
                rewardSprites[i].gameObject.SetActive(false);
            }
        }

    }
}
