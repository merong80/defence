﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum UILayer
{
    POPUP = 0,
    MAIN = 1,
    BATTLE = 2,
}

public class PopupManager : Singleton<PopupManager>
{

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(this.gameObject);
        Init();
    }

    public bool HasPopup()
    {
        return currentPopups.Count > 0;
    }

    void Init()
    {
        //popupPooler
    }
    [SerializeField]
    private UICamera uiCamera;

    private const string POPUP_PATH = "Prefabs/Popup/";
    private ObjectPooler<Popup> popupPooler = new ObjectPooler<Popup>(POPUP_PATH);
    [SerializeField]
    private Transform popupTransform;

    private List<Popup> currentPopups = new List<Popup>();

    [SerializeField]
    private int gameUIDepth = 10;
    [SerializeField]
    private int popupChildPanelCount = 2;

    public Popup GetCurrentPopup()
    {
        if (currentPopups.Count > 0) return currentPopups[0];
        return null;
    }

    public int GetPopupCount()
    {
        return currentPopups.Count;
    }

    public void UpdateCurrentPopup()
    {
        if (currentPopups.Count > 0)
        {
            currentPopups[0].UpdatePopup();
        }
    }

    public void UpdateAllPopup()
    {
        for (int i = 0; i < currentPopups.Count; i++)
        {
            currentPopups[i].UpdatePopup();
        }
    }

    [SerializeField]
    private LayerMask disableLayerMask;
    [SerializeField]
    private LayerMask pageLayerMask;
    [SerializeField]
    private LayerMask battleLayerMask;
    [SerializeField]
    private LayerMask popupLayerMask;

    private void ActiveEvent()
    {
        if (currentPopups.Count > 0)
        {
            if (BattleManager.HasInstance)
            {
                BattleManager.Instance.uiCamera.eventReceiverMask = disableLayerMask;
            }
            /*
            if (MainManager.HasInstance)
                MainManager.Instance.uiCamera.eventReceiverMask = disableLayerMask;*/
            Instance.uiCamera.eventReceiverMask = popupLayerMask;
        }
        else
        {
            if (BattleManager.HasInstance)
            {
                BattleManager.Instance.uiCamera.eventReceiverMask = battleLayerMask;
            }
            /*if (MainManager.HasInstance)
                MainManager.Instance.uiCamera.eventReceiverMask = pageLayerMask;*/
            Instance.uiCamera.eventReceiverMask = disableLayerMask;
        }
    }

    private void AllDisableEvent()
    {
        BattleManager.Instance.uiCamera.eventReceiverMask = disableLayerMask;
        Instance.uiCamera.eventReceiverMask = disableLayerMask;
    }
    [SerializeField]
    private List<string> overPopupNames = new List<string>();
    public Popup Open(string name)
    {
        //if (currentPopups.Count > 0) {
        //  currentPopups [0].gameObject.SetActive (false);
        //}
        Popup popup = popupPooler.Get(name, popupTransform);
        popup.gameObject.SetActive(true);
        currentPopups.Insert(0, popup);
        popup.Open();
        UpdatePopupDepth(overPopupNames.Contains(name));
        ActiveEvent();
        return popup;
    }

    private void UpdatePopupDepth(bool over)
    {
        if (currentPopups.Count <= 0)
            return;
        if (over)
        {
            for (int i = currentPopups.Count - 1, j = 1; i >= 0; i--, j += popupChildPanelCount)
            {
                currentPopups[i].SetDepth(gameUIDepth + j);
            }
        }
        else
        {
            for (int i = 0, j = popupChildPanelCount; i < currentPopups.Count; i++, j += popupChildPanelCount)
            {
                currentPopups[i].SetDepth(gameUIDepth - j);
            }
        }

        //for( int i = 0; i<
    }

    public void OpenComplete(Popup popup)
    {

    }

    public bool ClosePopup()
    {
        if (currentPopups.Count > 0)
        {
            //if (currentPopups[0].GetType() == typeof(PopupMain))
            //    return false;
            currentPopups[0].Close();
            return true;
        }
        return false;
    }

    public void CloseComplete(Popup popup)
    {
        if (currentPopups.Contains(popup))
        {
            currentPopups.Remove(popup);
            popup.gameObject.SetActive(false);
            popupPooler.Put(popup);
        }
        if (currentPopups.Count > 0)
        {
            currentPopups[0].gameObject.SetActive(true);
        }
        ActiveEvent();
    }

    public void CloseAll()
    {
        for (int i = 0; i < currentPopups.Count; i++)
        {
            currentPopups[i].gameObject.SetActive(false);
            popupPooler.Put(currentPopups[i]);
        }
        currentPopups.Clear();
        ActiveEvent();
    }
    [SerializeField]
    private GameObject loadingObject;

    public void Loading()
    {
        loadingObject.SetActive(true);
        Instance.uiCamera.eventReceiverMask = disableLayerMask;
    }

    public void CloseLoading()
    {
        loadingObject.SetActive(false);
        ActiveEvent();
    }

    public void Message(string msg)
    {
        Message(msg, false, null);
    }

    public void Message(string msg, PopupMessage.MessageCallback callback)
    {
        Message(msg, false, callback);
    }

    public void Message(string msg, bool showButton, PopupMessage.MessageCallback callback)
    {
        PopupMessage message = (PopupMessage)Open("message");
        message.SetData(msg, showButton, callback);
    }

    public void RemoveStageSelect()
    {
        Popup popup = popupPooler.GetRemove("stage_select");
        if (popup != null)
            Destroy(popup);
    }


    [SerializeField]
    private UILabel simpleMessageLabel;

    private float simpleMessageDuration = 2f;

    public void SimpleMessage(string msg)
    {
        SimpleMessage(msg, Vector3.zero, Color.white, simpleMessageDuration);
    }

    public void SimpleMessage(string msg, Vector3 pos)
    {
        SimpleMessage(msg, pos, Color.white, simpleMessageDuration);
    }

    public void SimpleMessage(string msg, Vector3 pos, Color color, float duration)
    {
        simpleMessageLabel.gameObject.SetActive(true);
        simpleMessageLabel.text = msg;
        simpleMessageLabel.transform.position = pos;
        simpleMessageLabel.color = color;
        StartCoroutine(ShowSimpleMessage(duration));
    }

    private IEnumerator ShowSimpleMessage(float duration)
    {
        simpleMessageLabel.gameObject.SetActive(true);
        yield return new WaitForSeconds(duration);
        simpleMessageLabel.gameObject.SetActive(false);
    }
}
