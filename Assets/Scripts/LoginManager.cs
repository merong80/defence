﻿using UnityEngine;
using System.Collections;

public enum LoginState
{
	OFFLINE = 0,
	LOADING_GAMEDATA = 1,
	LOADING_PLAYERDATA = 2,
	LOADING_ACHIEVEDATA = 3,
	LOADING_LEADERBOARDDATA = 4,
	ONLINE = 5,
}

public class LoginManager : Singleton<LoginManager>
{
	public bool needLogin = false;
	private LoginState loginState;
	private string PLAYERID;

	public void SetState(LoginState state)
	{
		switch (state) {
		case LoginState.OFFLINE: 
			break;
		}
	}
}

