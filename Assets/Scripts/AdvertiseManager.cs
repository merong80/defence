﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Collections.Generic;

public class AdvertiseManager : Singleton<AdvertiseManager>
{
	private string rewardPlacementId = "rewardedVideo";
	private string videoPlacementId = "video";

	private const int rewardTerm = 600;
	private float rewardedUnityTime = 0;

	private long rewardCoin = 100;

	private const int videoShowCount = 5;
	private int currentCheckVideoCount = 0;

	public void CheckVideo()
	{
		currentCheckVideoCount += 1;
		if (videoShowCount <= currentCheckVideoCount) {
			currentCheckVideoCount = 0;
			ShowAd ();

		}
	}

	protected override void Awake()
	{
		
		base.Awake ();
		DontDestroyOnLoad (this.gameObject);
	}

	void Start ()
	{
		//Advertisement.Initialize (gameI
	}

	void Update ()
	{
		//if (m_Button) m_Button.interactable = Advertisement.IsReady(placementId);
	}

	private Dictionary<int,long> rewardTable = new Dictionary<int,long> ();
	public void SetRewardData(AdvertiseRewardData[] rewards)
	{
		if (rewards == null)
			return;
		rewardTable.Clear ();
		for (int i = 0; i < rewards.Length; i++) {
			rewardTable.Add (rewards [i].level, rewards [i].reward);
		}
	}

	public void ShowRewardVideo()
	{
		if (Advertisement.IsReady (rewardPlacementId)) {
			ShowOptions options = new ShowOptions ();
			options.resultCallback = HandleShowResult;

			Advertisement.Show (rewardPlacementId, options);
		}
	}

	public void ShowAd ()
	{
		if (Advertisement.IsReady (videoPlacementId)) {
			ShowOptions options = new ShowOptions ();
			//options.resultCallback = HandleShowResult;

			Advertisement.Show (videoPlacementId, options);
		}
	}

    public int GetRemainRewardTime()
	{
		int remain = 0;
        long currentTime = Util.CurrentTime();
        if (currentTime == 0) return remain;
		if (currentTime - GameManager.Instance.GetAdRewardTime () < rewardTerm) {
			remain = rewardTerm-(int)(currentTime - GameManager.Instance.GetAdRewardTime ());
		} else {
			if (rewardedUnityTime > 0) {
				if (Time.realtimeSinceStartup - rewardedUnityTime < rewardTerm) {
					remain = rewardTerm-Mathf.FloorToInt(Time.realtimeSinceStartup - rewardedUnityTime);
				}
			}
		}
		return remain;
	}

	private long GetRewardCoin()
	{
		long reward = 0;
		if (rewardTable.ContainsKey (GameManager.Instance.ClearStage)) {
			reward = rewardTable [GameManager.Instance.ClearStage];
		} else if (rewardTable.ContainsKey (1)) {
			reward = rewardTable [1];
		}
		return reward;
		//GameManager.Instance.CurrentStage*
	}

	void HandleShowResult (ShowResult result)
	{
		if(result == ShowResult.Finished) {
			Debug.Log("Video completed - Offer a reward to the player");
			GameManager.Instance.SetAdRewardTime (Util.CurrentTime ());
			rewardedUnityTime = Time.realtimeSinceStartup;
			GameManager.Instance.ChargeCoin (GetRewardCoin());
			PopupManager.Instance.Message (string.Format (TextManager.Get (TextKey.success_rewarded_advertise), GetRewardCoin()));
            MainManager.Instance.Init();
		}else if(result == ShowResult.Skipped) {
			Debug.LogWarning("Video was skipped - Do NOT reward the player");

		}else if(result == ShowResult.Failed) {
			Debug.LogError("Video failed to show");
		}
	}
}