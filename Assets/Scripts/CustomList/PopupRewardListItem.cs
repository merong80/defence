﻿using UnityEngine;
using System.Collections;

public class PopupRewardListItem : ListItem<RewardItemData>
{
    [SerializeField]
    private UISprite sprite;
    [SerializeField]
    private UILabel amountLabel;

    public override void SetData(RewardItemData data)
    {
        base.SetData(data);

        switch(data.type)
        {
            case RewardType.COIN:
                sprite.spriteName = "card_00";
                amountLabel.text = data.amount.ToString();
                break;
            case RewardType.CARDS:
                sprite.spriteName = string.Format("card_{0:D2}",data.id);
                amountLabel.text = data.amount.ToString();
                break;
        }
    }
}
