﻿using UnityEngine;
using System.Collections;

public class ListItem<T> : MonoBehaviour where T : class
{
    private T data;

    public T GetData()
    {
        return data;
    }

    public virtual void SetData(T data)
    {
        if (scroll == null)
        {
            scroll = GetComponentInParent<CustomList<T>>();
        }
        this.data = data;

    }

    protected bool isActive;

    public virtual void Active(bool active)
    {
        isActive = active;
    }

    protected CustomList<T> scroll;
    public virtual void ClickItem()
    {

    }

    protected void SendScrollFocus()
    {
        scroll.Focus(this);
    }
    [SerializeField]
    protected GameObject disableObj;

    public virtual void Focus(ListItem<T> focusItem)
    {
        if (disableObj != null) disableObj.SetActive(focusItem != this);
    }
    public virtual void DisableFocusing()
    {
        if (disableObj != null)
            disableObj.SetActive(false);
    }
}

