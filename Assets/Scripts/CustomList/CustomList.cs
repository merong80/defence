﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomList<T> : MonoBehaviour where T : class
{
    protected List<ListItem<T>> listItems = new List<ListItem<T>>();
    [SerializeField]
    protected UIScrollView scroll;
    [SerializeField]
    protected UIGrid grid;
    [SerializeField]
    protected GameObject itemObj;


    public virtual void UpdateList(List<T> datas)
    {
        for (int i = 0; i < datas.Count; i++)
        {
            if (listItems.Count <= i)
            {
                GameObject go = Instantiate(itemObj);
                go.transform.parent = grid.transform;
                go.transform.position = Vector3.zero;
                go.transform.localScale = Vector3.one;
                ListItem<T> item = go.GetComponent<ListItem<T>>();
                listItems.Add(item);
            }
            listItems[i].gameObject.SetActive(true);
            listItems[i].SetData(datas[i]);
        }

        if (datas.Count < listItems.Count)
        {
            for (int i = datas.Count; i < listItems.Count; i++)
            {
                listItems[i].gameObject.SetActive(false);
            }
        }
        grid.Reposition();
        scroll.UpdatePosition();
    }

    public void Active(bool active)
    {
        for (int i = 0; i < listItems.Count; i++)
        {
            listItems[i].Active(active);
        }
    }

    public bool isFocusing = false;
    public void Focus(ListItem<T> target)
    {
        isFocusing = true;
        for (int i = 0; i < listItems.Count; i++)
        {
            listItems[i].Focus(target);
        }

    }
    public void DisableFocusing()
    {
        isFocusing = false;
        for (int i = 0; i < listItems.Count; i++)
        {
            listItems[i].DisableFocusing();
        }
    }
}

