﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> {

    //public const int CHARACTER_MAX_LEVEL = 10;
    public int[] CHARACTER_MAX_LEVEL;
    public const int CHARACTER_MAX_GRADE = 4;
    public int[] upgradeNeedCards;
    public float upgradeCostGradeRate = 0.2f;

	private const int BATTLE_CHARACTER_NUM = 7;
	public const bool NEED_LOGIN = true;
	public static string PlayerID;
	private PlayerData playerData;

    //private System.DateTime

	protected override void Awake()
	{
		base.Awake ();
		DontDestroyOnLoad (this.gameObject);

        //System.DateTime currentTime = Util.GetNetworkTime();
        //Debug.Log(currentTime);
        //StartCoroutine(GetServerTime());
        //Application.OpenURL("http://time1.google.com")
    }

    private IEnumerator GetServerTime()
    {
        Debug.Log("GetServerTime");
        WWW timeServer = new WWW("time1.google.com");
        yield return timeServer;
        if( timeServer.error != null )
        {
            Debug.Log("Error   "+timeServer.error);
        }
        else
        {
            Debug.Log("Success   "+timeServer.text);
        }
    }

    public void TestInitPlayerData()
	{
		PlayerPrefs.DeleteKey (PrefsKey.PLAYERDATA);
		playerData = new PlayerData (PlayerID);

		SaveManager.Instance.SaveData (playerData);
		PopupManager.Instance.RemoveStageSelect ();
		UpdatePlayerCharacterData ();
		SetInitStep (InitStep.LOGIN);
	}

	public void SetPlayerData(PlayerData data)
	{
		if (playerData != null)
			return;
		playerData = data;
		SaveManager.Instance.SaveData (playerData);
		PopupManager.Instance.RemoveStageSelect ();
		UpdatePlayerCharacterData ();
		SetInitStep (InitStep.LOGIN);
	}

	private enum InitStep
	{
		INIT = 0,
		LOAD_DATA = 1,
		LOGIN = 2,
		LOAD_SCENE = 3,
		COMPLETE = 4,
	}

	private void SetInitStep(InitStep step, float progress)
	{
		float stepPercent = 1f / (int)InitStep.COMPLETE;
		float percent = (float)step / (int)InitStep.COMPLETE+stepPercent*progress;
		if (TitleManager.HasInstance)
			TitleManager.Instance.SetGuage (percent);
	}

	private void SetInitStep(InitStep step)
	{
		SetInitStep (step,0f);
	}

	// Use this for initialization
	void Start () {
		Init ();
	}
	
	// Update is called once per frame
	void Update () {
		#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)

		if( Input.GetKeyDown(KeyCode.Escape) )
		{
			OnEscapeKey();
		}

		#elif UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			OnEscapeKey();
		}

		#endif
	}

	private void OnEscapeKey()
	{
		if (!PopupManager.Instance.ClosePopup ()) {
			PopupManager.Instance.Message ("Quit",true,delegate(bool confirm) {
				if( confirm )
				{
					Application.Quit ();
				}
			});
		}
	}

	private void Init()
	{
		SetInitStep (InitStep.INIT);
		DG.Tweening.DOTween.Init();
		DG.Tweening.DOTween.defaultTimeScaleIndependent = true;

        LoadData ();
		SetInitStep (InitStep.LOAD_DATA);
		Login ();
	}

	private void Login()
	{
		#if UNITY_EDITOR
		PlayerID = SystemInfo.deviceUniqueIdentifier;
		SetPlayerData(SaveManager.Instance.LoadData ());
		#else
		if (NEED_LOGIN) {
			//PopupManager.Instance.Loading ();
			GPManager.Instance.Connect ();
		} else {
			PlayerID = SystemInfo.deviceUniqueIdentifier;
			SetPlayerData(SaveManager.Instance.LoadData ());
		}
		#endif
	}

	public void LoadData()
	{
		characterModelData.Clear ();
		CharacterModelData characterModel = JsonConvert.DeserializeObject <CharacterModelData> (Resources.Load ("Data/data").ToString ());
		for (int i = 0; i < characterModel.character.Length; i++) {
			if (characterModelData.ContainsKey (characterModel.character [i].id))
				continue;
			characterModelData.Add (characterModel.character [i].id, characterModel.character [i]);

		}
		enemyModelData.Clear ();
		for (int i = 0; i < characterModel.enemy.Length; i++) {
			if (enemyModelData.ContainsKey (characterModel.enemy [i].id))
				continue;
			enemyModelData.Add (characterModel.enemy [i].id, characterModel.enemy [i]);
		}
        for (int i = 0; i < characterModel.boss.Length; i++)
        {
			if (enemyModelData.ContainsKey (characterModel.boss [i].id))
				continue;
            enemyModelData.Add(characterModel.boss[i].id, characterModel.boss[i]);
        }

		for (int i = 0; i < characterModel.stageData.Length; i++) {
            if (characterModel.stageData[i].stage == 0) break;
			stageData.Add (characterModel.stageData [i].stage, characterModel.stageData [i]);
		}

        for( int i = 0; i < characterModel.explore.Length; i++ )
        {
            if (exploreDatas.ContainsKey(characterModel.explore[i].id)) exploreDatas[characterModel.explore[i].id] = characterModel.explore[i];
            else exploreDatas.Add(characterModel.explore[i].id, characterModel.explore[i] );
        }
        /*enemyCreateModelData.Clear ();
		for (int i = 0; i < characterModel.enemyCreate.Length; i++) {
			enemyCreateModelData.Add (characterModel.enemyCreate [i].id, characterModel.enemyCreate [i]);
		}

		towerEnemyCreateModelData.Clear ();
		maxTowerStage = 0;
		for (int i = 0; i < characterModel.towerEnemyCreate.Length; i++) {
			if (characterModel.towerEnemyCreate [i].stage > maxTowerStage)
				maxTowerStage = characterModel.towerEnemyCreate [i].stage;
			
			if (towerEnemyCreateModelData.ContainsKey (characterModel.towerEnemyCreate [i].stage)) {
				towerEnemyCreateModelData [characterModel.towerEnemyCreate [i].stage].Add (characterModel.towerEnemyCreate [i]);
			} else {
				towerEnemyCreateModelData.Add (characterModel.towerEnemyCreate [i].stage, new List<TowerEnemyCreateModelData> (){ characterModel.towerEnemyCreate [i] });
			}
		}

		enemyTowerData.Clear ();
		for (int i = 0; i < towerData.enemyTower.Length; i++) {
			enemyTowerData.Add (towerData.enemyTower [i].stage, towerData.enemyTower [i]);
		}

		*/
        LoadEnemyPatterns ();
		towerLevelupData.Clear ();
        towerMoneyLevelData.Clear();
		TowerLevelModelData towerData = JsonConvert.DeserializeObject<TowerLevelModelData> (Resources.Load ("Data/towerData").ToString ());
		for (int i = 0; i < towerData.towerMoney.Length; i++) {
			towerMoneyLevelData.Add (towerData.towerMoney [i].level, towerData.towerMoney [i]);
		}
		SetTowerLevelupData (TowerLevelType.MONEY_SPEED, towerData.moneySpeed);
		SetTowerLevelupData (TowerLevelType.MONEY_MAX, towerData.moneyMax);
		SetTowerLevelupData (TowerLevelType.HP, towerData.hp);
		SetTowerLevelupData (TowerLevelType.MONEY_KILL, towerData.killMoney);
		SetTowerLevelupData (TowerLevelType.CREATE_COOL, towerData.createCool);
		SetTowerLevelupData (TowerLevelType.COIN_KILL, towerData.killCoin);
		skillData.Clear ();
		for (int i = 0; i < towerData.skill.Length; i++) {
			skillData.Add (towerData.skill [i].level, towerData.skill [i]);
		}
		AdvertiseManager.Instance.SetRewardData (towerData.ad_reward);
		if (playerData != null)
			UpdatePlayerCharacterData ();
	}

	private Dictionary<int,EnemyPattern[]> enemyPatternDatas = new Dictionary<int, EnemyPattern[]>();
	private void LoadEnemyPatterns()
	{
		enemyPatternDatas.Clear ();
		for (int i = 1; i <= 1000; i++) {
			Object src = Resources.Load ("Data/Stage/stage_" + i);
			if (src == null)
				break;
			EnemyPatternData data = JsonConvert.DeserializeObject<EnemyPatternData> (src.ToString());
			enemyPatternDatas.Add (i, data.patterns);
		}
	}

	public EnemyPattern[] GetEnemyPatterns(int stage)
	{
		if (enemyPatternDatas.ContainsKey (stage))
			return enemyPatternDatas [stage];

		return null;
	}

	private void SetTowerLevelupData(TowerLevelType type, LevelupData[] list)
	{
		Dictionary<int,LevelupData> data = new Dictionary<int, LevelupData> ();
		for (int i = 0; i < list.Length; i++) {
			data.Add (list [i].level, list[i]);
		}
		towerLevelupData.Add (type, data);
	}

	#region PlayerData
	public int CurrentStage
	{
		set {
			playerData.currentStage = value;
			SaveManager.Instance.SaveData (playerData);
		}
		get {
			if (playerData == null) return 1;
			return playerData.currentStage;
		}
	}

	public int ClearStage
	{
		set {
			if (playerData.clearStage < value) {
				playerData.clearStage = value;
				GPManager.Instance.UpdateAchievementStage(playerData.clearStage);
                GPManager.Instance.SubmitScore(playerData.clearStage);
				SaveManager.Instance.SaveData (playerData);
			}
		}
		get {
			if (playerData == null) return 1;
			return playerData.clearStage;
		}
	}

	public List<int> GetCurrentBattleCahracter()
	{
		return playerData.battleCharacter;
	}

	public void SetCurrentBattleCharacter(int id)
	{
		if (playerData.battleCharacter.Count < BATTLE_CHARACTER_NUM && !playerData.battleCharacter.Contains (id)) {
			if( playerData.hasCharacter.Contains (id) )
				playerData.battleCharacter.Add (id);
		}
		SaveManager.Instance.SaveData (playerData);
	}

	public void RemoveCurrentBattleCharacter(int id)
	{
		if (playerData.battleCharacter.Contains (id)) {
			playerData.battleCharacter.Remove (id);
		}
	}

	public long GetPlayerGameStartTime()
	{
		return playerData.gameStartTime;
	}

	public void SetAdRewardTime(long time)
	{
		playerData.adRewardTime = time;
	}

	public long GetAdRewardTime()
	{
		return playerData.adRewardTime;
	}

	public long GetPlayerCoin()
	{
		return playerData.coin;
	}

	#endregion


	#region CharacterData
	private Dictionary<int,CharacterData> characterModelData = new Dictionary<int, CharacterData>();
	private Dictionary<int, CharacterBattleData> playerCharacterData = new Dictionary<int, CharacterBattleData>();
	private List<CharacterBattleData> currentBattleCharacterData = new List<CharacterBattleData>();

	private Dictionary<int,CharacterData> enemyModelData = new Dictionary<int, CharacterData>();

	public Dictionary<int,CharacterBattleData> GetPlayerCharacterData()
	{
		return playerCharacterData;
	}

	public List<CharacterBattleData> GetBattlePlayerCharacterData()
	{
        if (playerData == null)
        {
			playerData = SaveManager.Instance.LoadData ();
            LoadData();
        }
		currentBattleCharacterData.Clear ();
		for (int i = 0; i < playerData.battleCharacter.Count; i++) {
			currentBattleCharacterData.Add (playerCharacterData[playerData.battleCharacter[i]]);
		}

		return currentBattleCharacterData;
	}

	public Dictionary<int,CharacterBattleData> GetEnemyCharacterData(int level)
	{
		Dictionary<int, CharacterBattleData> data = new Dictionary<int, CharacterBattleData> ();
		foreach (KeyValuePair<int, CharacterData> pair in characterModelData) {
			CharacterBattleData characterData = GetCharacterBattleData (pair.Key, level);
			//characterData.name = characterData.name;
			data.Add (pair.Key, characterData);
		}
		return data;
	}

	private void UpdatePlayerCharacterData()
	{
		playerCharacterData.Clear ();

		foreach (KeyValuePair<int, CharacterData> pair in characterModelData) {
			playerCharacterData.Add (pair.Key, GetCharacterBattleData (pair.Key, playerData.characterLevel.ContainsKey (pair.Key) ? playerData.characterLevel[pair.Key] : 0));
		}

		if (MainManager.HasInstance) {
			MainManager.Instance.Init ();
			MainManager.Instance.UpdateCoin (playerData.coin);
		} else {
			if( SceneManager.GetActiveScene ().buildIndex == 0 )
				StartCoroutine(LoadLevel (1));
		}
	}

	public CharacterBattleData GetCharacterBattleData(int id)
	{
		return GetCharacterBattleData (id, playerData.characterLevel.ContainsKey (id) ? playerData.characterLevel[id] : 0);
	}

	public CharacterBattleData GetCharacterBattleData(int id, int level)
	{
        //if (playerData.characterCards.ContainsKey(id)) Debug.Log("GetCharacterBattleData   " + id+"//"+playerData.characterCards[id]);
		CharacterBattleData data = new CharacterBattleData ();
		data.id = id;
		data.name = characterModelData[id].name;
		data.cost = characterModelData[id].costCreate;
		data.level = level;
        if (playerData.characterGrade == null) playerData.characterGrade = new Dictionary<int, int>();
        if (playerData.characterCards == null) playerData.characterCards = new Dictionary<int, int>();

        data.grade = playerData.characterGrade.ContainsKey(id) ? playerData.characterGrade[id] : 0;
        data.cards = playerData.characterCards.ContainsKey(id) ? playerData.characterCards[id] : 0;

        //data.atk = Mathf.FloorToInt (characterModelData [id].atk * (Mathf.Pow (characterModelData [id].upgradeAtk, level > 0 ? level-1 : 0)));
        //data.maxHp = Mathf.FloorToInt (characterModelData [id].maxHp * (Mathf.Pow (characterModelData [id].upgradeHp, level > 0 ? level-1 : 0)));
		if (level <= 1) {
			data.atk = Mathf.FloorToInt (characterModelData [id].atk);
			data.maxHp = Mathf.FloorToInt (characterModelData [id].maxHp);
		} else {
			//data.atk = Mathf.FloorToInt(characterModelData[id].atk + (characterModelData[id].upgradeAtk*(level-1)));
			//data.maxHp = Mathf.FloorToInt (characterModelData [id].maxHp + (characterModelData [id].upgradeHp*( level-1)));
			data.atk = Mathf.FloorToInt(characterModelData[id].atk + (characterModelData[id].upgradeAtk*((level-1)*level/2)));
			data.maxHp = Mathf.FloorToInt(characterModelData[id].maxHp + (characterModelData[id].upgradeHp*((level-1)*level/2)));
		}

        data.moveSpeed = characterModelData [id].moveSpeed;
		data.atkCool = characterModelData [id].atkCool;
		data.knockBackDamage = Mathf.FloorToInt ((float)data.maxHp * (characterModelData[id].knockBackDamage * 0.01f));
		data.initForce = new Vector2 (characterModelData [id].initForceX, characterModelData [id].initForceY);
		data.atkForce = new Vector2 (characterModelData [id].atkForceX, characterModelData [id].atkForceY);
		data.finishForce = new Vector2 (characterModelData [id].finishForceX, characterModelData [id].finishForceY);
		data.createCool = characterModelData [id].createCool;
		data.level = level;

		data.atkTarget = characterModelData [id].atkTarget;

		return data;
	}

	public CharacterBattleData GetEnemyBattleData(int id, int stage)
	{
		CharacterBattleData data = new CharacterBattleData ();
		data.id = id;
		data.name = enemyModelData[id].name;
		data.cost = enemyModelData[id].costCreate;

        //data.atk = Mathf.FloorToInt (enemyModelData [id].atk * (Mathf.Pow (enemyModelData [id].upgradeAtk, stage > 0 ? stage-1 : 0)));
        //data.maxHp = Mathf.FloorToInt (enemyModelData [id].maxHp * (Mathf.Pow (enemyModelData [id].upgradeHp, stage > 0 ? stage-1 : 0)));

        //data.atk = Mathf.FloorToInt(enemyModelData[id].atk + (enemyModelData[id].upgradeAtk * (stage - 1)));
        //data.maxHp = Mathf.FloorToInt(enemyModelData[id].maxHp + (enemyModelData[id].upgradeHp * (stage - 1)));

		data.atk = Mathf.FloorToInt(enemyModelData[id].atk + (enemyModelData[id].upgradeAtk*((stage-1)*stage/2)));
		data.maxHp = Mathf.FloorToInt(enemyModelData[id].maxHp + (enemyModelData[id].upgradeHp*((stage-1)*stage/2)));

        data.moveSpeed = enemyModelData [id].moveSpeed;
		data.atkCool = enemyModelData [id].atkCool;
		data.knockBackDamage = Mathf.FloorToInt ((float)data.maxHp * (enemyModelData[id].knockBackDamage * 0.01f));
		data.initForce = new Vector2 (enemyModelData [id].initForceX, enemyModelData [id].initForceY);
		data.atkForce = new Vector2 (enemyModelData [id].atkForceX, enemyModelData [id].atkForceY);
		data.finishForce = new Vector2 (enemyModelData [id].finishForceX, enemyModelData [id].finishForceY);
		data.createCool = enemyModelData [id].createCool;
		data.level = stage;
		data.killMoney = enemyModelData [id].killMoney;
		data.killCoin = enemyModelData [id].killCoin+Mathf.FloorToInt(enemyModelData[id].upgradeKillCoin*(stage-1));
		data.atkTarget = enemyModelData [id].atkTarget;
		return data;
	}

	private void UpdatePlayerCharacterData(int id)
	{
		if (playerCharacterData.ContainsKey (id)) {
			playerCharacterData [id] = GetCharacterBattleData (id,playerData.characterLevel.ContainsKey (id) ? playerData.characterLevel[id] : 0 );
		} else {
			playerCharacterData.Add (id, GetCharacterBattleData (id,playerData.characterLevel.ContainsKey (id) ? playerData.characterLevel[id] : 0));
		}
	}

	public void BuyCharacter(int id)
	{
		if (playerData.hasCharacter.Contains (id)) {
			//already have character
			return;
		}
		if (!UseCoin (characterModelData [id].costCoin)) {
            //not enough coin
            PopupManager.Instance.Message(TextManager.Get(TextKey.not_enough_coin));
            return;
		}

        if (playerData.characterGrade == null) playerData.characterGrade = new Dictionary<int, int>();
        if (playerData.characterCards == null) playerData.characterCards = new Dictionary<int, int>();

        playerData.hasCharacter.Add (id);
		if (playerData.characterLevel.ContainsKey (id)) {
			playerData.characterLevel [id] = 1;
		} else {
			playerData.characterLevel.Add (id, 1);
		}
        if( !playerData.characterCards.ContainsKey(id) )
        {
            playerData.characterCards.Add(id, 0);

        }


        if ( !playerData.characterGrade.ContainsKey(id) )
        {
            playerData.characterGrade.Add(id, 0);
        }

        if (playerData.battleCharacter.Count < BATTLE_CHARACTER_NUM) {
			playerData.battleCharacter.Add (id);
		}
        if (MainManager.HasInstance) MainManager.Instance.UpdateMovingCharacter();
		UpdatePlayerCharacterData (id);

        GPManager.Instance.UpdateAchievementStep(characterCollectAchievementId, playerData.hasCharacter.Count);
        GPCharacterLevelupAchievement(id);
        SoundManager.Instance.PlayEffect ("success_buy");
		SaveManager.Instance.SaveData (playerData);
	}

    public bool UseCharacterCards(int id, int amount)
    {
        if (playerData.characterGrade == null) playerData.characterGrade = new Dictionary<int, int>();
        if (playerData.characterCards == null) playerData.characterCards = new Dictionary<int, int>();

        if (!playerData.characterCards.ContainsKey(id)) return false;

        if (playerData.characterCards[id] < amount) return false;

        playerData.characterCards[id] -= amount;
        return true;
    }

    public void ChargeCharacterCards(int id, int amount)
    {
        Debug.Log("ChargeCharacterCards   " + id + "//" + amount);
        if (playerData.characterGrade == null) playerData.characterGrade = new Dictionary<int, int>();
        if (playerData.characterCards == null) playerData.characterCards = new Dictionary<int, int>();
        if ( playerData.characterCards.ContainsKey(id) )
        {
            playerData.characterCards[id] += amount;
        }
        else
        {
            playerData.characterCards.Add(id, amount);
        }
    }

    public void UpgradeCharacter(int id)
    {
        if (playerData.characterGrade == null) playerData.characterGrade = new Dictionary<int, int>();
        if (playerData.characterCards == null) playerData.characterCards = new Dictionary<int, int>();

        if (!playerData.characterGrade.ContainsKey(id)) playerData.characterGrade.Add(id, 0);
        if( playerData.characterGrade[id] >= CHARACTER_MAX_GRADE )
        {
            PopupManager.Instance.Message(TextManager.Get(TextKey.character_max_level));
            return;
        }
        if (!playerData.hasCharacter.Contains(id))
        {
            //no have character
            return;
        }
        if (!UseCharacterCards(id, upgradeNeedCards[playerData.characterGrade[id]]))
        {
            //not enough coin
            PopupManager.Instance.Message(TextManager.Get(TextKey.not_enough_cards));
            return;
        }
        playerData.characterLevel[id] = 1;
        playerData.characterGrade[id] += 1;
        UpdatePlayerCharacterData(id);
        GPCharacterLevelupAchievement(id);
        SoundManager.Instance.PlayEffect("success_buy");

        SaveManager.Instance.SaveData(playerData);
    }

    private void GPCharacterLevelupAchievement(int characterId)
    {
        for (int i = 0; i < characterLevelupAchievementId.Count; i++)
        {
            if (characterLevelupAchievementId[i].characterId == characterId)
            {
                int level = 0;
                for( int j = 0; j < playerData.characterGrade[characterId]; j++ )
                {
                    level = CHARACTER_MAX_LEVEL[j];
                }
                level += playerData.characterLevel[characterId];

                GPManager.Instance.UpdateAchievementStep(characterLevelupAchievementId[i].achievementId, level);
                break;
            }

        }
    }

    public void LevelupCharacter(int id)
	{
        if (playerData.characterGrade == null) playerData.characterGrade = new Dictionary<int, int>();
        if (playerData.characterCards == null) playerData.characterCards = new Dictionary<int, int>();

        if (!playerData.characterGrade.ContainsKey(id)) playerData.characterGrade.Add(id, 0);
        if (playerData.characterLevel [id] >= CHARACTER_MAX_LEVEL[playerData.characterGrade[id]]) {
			PopupManager.Instance.Message (TextManager.Get (TextKey.character_max_level));
			return;
		}
		if (!playerData.hasCharacter.Contains (id)) {
			//no have character
			return;
		}
		if (!UseCoin (GetCharacterUpgradeCost (id))) {
			//not enough coin
			PopupManager.Instance.Message(TextManager.Get(TextKey.not_enough_coin));
			return;
		}
		playerData.characterLevel [id] += 1;
		UpdatePlayerCharacterData (id);

        GPCharacterLevelupAchievement(id);

		SoundManager.Instance.PlayEffect ("success_buy");

		SaveManager.Instance.SaveData (playerData);
	}

	public int GetCharacterUpgradeCost(int id)
	{
        if (playerData.characterGrade == null) playerData.characterGrade = new Dictionary<int, int>();
        if (playerData.characterCards == null) playerData.characterCards = new Dictionary<int, int>();
        if (!playerData.characterGrade.ContainsKey(id)) playerData.characterGrade.Add(id, 0);
        //int cost = Mathf.FloorToInt(characterModelData[id].costdefUpgrade + (characterModelData[id].costUpgrade* (playerData.characterLevel[id]-1)));
        int cost = Mathf.FloorToInt(characterModelData[id].costdefUpgrade + ((characterModelData[id].costUpgrade+(characterModelData[id].costUpgrade*(upgradeCostGradeRate * playerData.characterGrade[id]))) * (playerData.characterLevel[id] - 1)));

        return cost;
	}

	public int GetCharacterBuyCost(int id)
	{
		int cost = characterModelData [id].costCoin;
		return cost;
	}

    public void AddGetBoss(int id)
    {
        if (!playerData.getBossCharacter.Contains(id)) playerData.getBossCharacter.Add(id);
    }

    public List<CharacterBattleData> GetPlayerGetBossCharacterData()
    {
        List<CharacterBattleData> list = new List<CharacterBattleData>();
        for( int i = 0; i < playerData.getBossCharacter.Count; i++ )
        {
            list.Add(GetEnemyBattleData(playerData.getBossCharacter[i], 1));
        }
        return list;
    }

	#endregion
	#region TowerData
	private Dictionary<int,TowerMoneyLevel> towerMoneyLevelData = new Dictionary<int, TowerMoneyLevel>();
	private Dictionary<TowerLevelType, Dictionary<int,LevelupData>> towerLevelupData = new Dictionary<TowerLevelType, Dictionary<int,LevelupData>> ();
	private Dictionary<int,SkillData> skillData = new Dictionary<int, SkillData> ();
	public void LevelUpTower(TowerLevelType type)
	{
		int level = playerData.towerLevel [type];
		if (type == TowerLevelType.SKILL) {
			if (!skillData.ContainsKey (level + 1)) {
				PopupManager.Instance.Message (TextManager.Get(TextKey.max_level));
				return;
			}
			if (!UseCoin (skillData[level + 1].cost)) {
				PopupManager.Instance.Message (TextManager.Get(TextKey.not_enough_coin));
				return;
			}
		} else {
			if (!towerLevelupData [type].ContainsKey (level + 1)) {
				PopupManager.Instance.Message (TextManager.Get(TextKey.max_level));
				return;
			}
			if (!UseCoin (towerLevelupData [type] [level + 1].cost)) {
				PopupManager.Instance.Message (TextManager.Get(TextKey.not_enough_coin));
				return;
			}
		}

		SoundManager.Instance.PlayEffect ("success_buy");
		//if( UseCoin
		playerData.towerLevel[type] += 1;
	}

	public List<TowerMoneyLevel> GetPlayerTowerMoneyLevel()
	{
		List<TowerMoneyLevel> list = new List<TowerMoneyLevel> ();
		foreach (KeyValuePair<int,TowerMoneyLevel> pair in towerMoneyLevelData) {
			TowerMoneyLevel data = new TowerMoneyLevel ();
			data.level = pair.Key;
			data.cost = Mathf.FloorToInt (pair.Value.cost * towerLevelupData [TowerLevelType.MONEY_MAX] [playerData.towerLevel[TowerLevelType.MONEY_MAX]].value);
			data.moneyMax = Mathf.FloorToInt (pair.Value.moneyMax * towerLevelupData [TowerLevelType.MONEY_MAX] [playerData.towerLevel[TowerLevelType.MONEY_MAX]].value);
			data.moneySpeed = pair.Value.moneySpeed * towerLevelupData [TowerLevelType.MONEY_SPEED] [playerData.towerLevel[TowerLevelType.MONEY_MAX]].value;

			list.Add (data);
		}
		return list;
	}

	public LevelupData GetTowerLevelData(TowerLevelType type, int level)
	{
		//LevelupData data = new LevelupData ();
		if( !towerLevelupData[type].ContainsKey (level) ) return null;
		return towerLevelupData [type] [level];
	}

	public int GetPlayerTowerHp()
	{
		LevelupData data = GetPlayerTowerLevelData (TowerLevelType.HP);
		return (int)data.value;
	}

	public LevelupData GetPlayerTowerLevelData(TowerLevelType type)
	{
		return GetTowerLevelData (type,playerData.towerLevel[type]);
	}

	public LevelupData GetPlayerNextTowerLevelData(TowerLevelType type)
	{
		return GetTowerLevelData (type,playerData.towerLevel[type]+1);
	}

	public SkillData GetPlayerSkillData()
	{
		return skillData [playerData.towerLevel[TowerLevelType.SKILL]];
	}

	public SkillData GetPlayerNextSkillData()
	{
		if( skillData.ContainsKey (playerData.towerLevel[TowerLevelType.SKILL]+1) ) 
			return skillData [playerData.towerLevel[TowerLevelType.SKILL]+1];

		return null;
	}
	#endregion
	#region StageEnemyData
	private Dictionary<int, StageData> stageData = new Dictionary<int, StageData> ();
	public StageData GetStageData(int stage)
	{
		if (stageData.ContainsKey (stage))
			return stageData [stage];

		return null;
	}
	/*private Dictionary<int, EnemyCreateModelData> enemyCreateModelData = new Dictionary<int, EnemyCreateModelData> ();
	public List<EnemyCreateData> GetEnemyCreateList(int stage)
	{
		List<EnemyCreateData> list = new List<EnemyCreateData> ();
		foreach (KeyValuePair<int,EnemyCreateModelData> pair in enemyCreateModelData) {
			if (pair.Value.stageMin <= stage && pair.Value.stageMax >= stage) {
				list.Add(new EnemyCreateData (stage,pair.Value));
			}
		}
		return list;
	}

	private int maxTowerStage = 0;
	public List<TowerEnemyCreateModelData> GetTowerEnemyCreateList(int stage)
	{
		if (towerEnemyCreateModelData.ContainsKey (stage))
			return towerEnemyCreateModelData [stage];

		return towerEnemyCreateModelData [maxTowerStage];
	}*/
	#endregion

	public bool UseCoin(int coin)
	{
		return UseCoin ((long)coin);
	}

	public bool UseCoin(long coin)
	{
		if (coin > playerData.coin) {
			return false;
		}

		playerData.coin -= coin;
		if( MainManager.HasInstance ) MainManager.Instance.UpdateCoin (playerData.coin);
		return true;
	}

	public void ChargeCoin(int coin)
	{
		ChargeCoin ((long)coin); 
	}

	public void ChargeCoin(long coin)
	{
		playerData.coin += coin;

		if( MainManager.HasInstance ) MainManager.Instance.UpdateCoin (playerData.coin);

		SaveManager.Instance.SaveData (playerData);
	}

    public Dictionary<int, ExploreData> exploreDatas = new Dictionary<int, ExploreData>();

    public List<ExploreData> GetExploreData(int area)
    {
        List<ExploreData> datas = new List<ExploreData>();
        foreach(KeyValuePair<int, ExploreData> pair in exploreDatas)
        {
            if (pair.Value.area == area) datas.Add(pair.Value);
        }
        return datas;
    }

    public long GetExploreStartTime()
    {
        return playerData.exploreStartTime;
    }
    public ExploreData GetCurrentExploreData()
    {
        Debug.Log("GetCurrentExploreData   " + playerData.exploreId+"//"+exploreDatas.ContainsKey(playerData.exploreId));
        if (exploreDatas.ContainsKey(playerData.exploreId)) return exploreDatas[playerData.exploreId];

        return null;
    }

    public void StartExplore(int id)
    {
        long currentTime = Util.CurrentTime();
        if (currentTime == 0) return;
        playerData.exploreId = id;
        playerData.exploreStartTime = currentTime;

        SaveManager.Instance.SaveData(playerData);
    }

    public RewardData GetExploreRewardData()
    {
        //Debug.Log("GetExploreRewardData   " );
        if (playerData.exploreRewardData != null)
        {
            if( playerData.exploreRewardData.coin <= 0 && playerData.exploreRewardData.cards.Count == 0 )
            {
                playerData.exploreRewardData = null;
                return null;
            }
            //Debug.Log(playerData.exploreRewardData.coin + "//" + playerData.exploreRewardData.cards.Count);
            return playerData.exploreRewardData;
        }

        ExploreData exploringData = GetCurrentExploreData();
        if (exploringData != null)
        {
            if( GetExploreRemainTime() <= 0 )
            {

                RewardData rewardData = new RewardData();
                rewardData.coin = 0;
                for (int i = 0; i < exploringData.hour; i++)
                {

                    int coin = Random.Range(exploringData.coin_min, exploringData.coin_max);
                    rewardData.coin += coin;
                    for (int j = 0; j < exploringData.drop.Length; j++)
                    {
                        if (Random.Range(0, 100) < exploringData.drop_rate[j])
                        {
                            if (rewardData.cards.ContainsKey(exploringData.drop[j])) rewardData.cards[exploringData.drop[j]] += 1;
                            else rewardData.cards.Add(exploringData.drop[j], 1);
                        }
                    }
                }
                playerData.exploreId = 0;
                playerData.exploreRewardData = rewardData;
                return rewardData;
            }
        }

        return null;
    }

    public int GetExploreRemainTime()
    {
        ExploreData exploringData = GetCurrentExploreData();
        if (exploringData != null)
        {
            long exploreStartTime = playerData.exploreStartTime;
            long exploreTime = Util.CurrentTime() - exploreStartTime;

            return (exploringData.hour * 3600) - (int)exploreTime;
        }
        return 0;
    }

    public void ResetPlayerExploreRewardData()
    { 
        playerData.exploreRewardData = null;
        SaveManager.Instance.SaveData(playerData);
    }

    public void ChargeRewardData(RewardData data)
    {
        if (data != null)
        {
            ChargeCoin(data.coin);
            //Debug.Log()
            foreach (KeyValuePair<int, int> pair in data.cards)
            {
                Debug.Log("ChargeRewardData   " + pair.Key + " // " + pair.Value);
                ChargeCharacterCards(pair.Key, pair.Value);
            }

            //playerData.exploreRewardData = null;
            UpdatePlayerCharacterData();
        }
        SaveManager.Instance.SaveData(playerData);
    }


    public void TestExploringComplete()
    {
        Debug.Log("TestExploringComplete"+playerData.exploreStartTime);
        ExploreData exploreData = GetCurrentExploreData();
        if( exploreData != null )
        {
            playerData.exploreStartTime = Util.CurrentTime() - (exploreData.hour * 3600 - 10);
            Debug.Log("TestExploringComplete" + playerData.exploreStartTime);
        }

    }

    public bool CheckRoulette()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable) return false;
        string today = Util.GetToday();
        if (string.IsNullOrEmpty(today)) return false;
        return (playerData.rouletteRewardDay != today);
    }

    public void RewardRoulette()
    {
        playerData.rouletteRewardDay = Util.GetToday();
        SaveManager.Instance.SaveData(playerData);
    }
    #region SceneManager
    private AsyncOperation loadAsync;
    public IEnumerator LoadLevel(int idx)
    {
		if (!TitleManager.HasInstance) PopupManager.Instance.Loading();
        loadAsync = SceneManager.LoadSceneAsync(idx);
        loadAsync.completed += LoadComplete;
        while ( !loadAsync.isDone )
        {
			if (TitleManager.HasInstance) {
				SetInitStep (InitStep.LOAD_SCENE,loadAsync.progress);
			}
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    private void LoadComplete(AsyncOperation ao)
    {
        loadAsync.completed -= LoadComplete;
        if (Time.timeScale != 1f) Time.timeScale = 1f;
        if (MainManager.HasInstance)
        {
            MainManager.Instance.Init();
            MainManager.Instance.UpdateCoin(playerData.coin);
        }
        PopupManager.Instance.CloseLoading();
    }
    #endregion
    [SerializeField]
    private string characterCollectAchievementId;
    [SerializeField]
    private List<GPAchievementCharacterLevelupData> characterLevelupAchievementId = new List<GPAchievementCharacterLevelupData>();
}
[System.Serializable]
public class GPAchievementCharacterLevelupData
{
    public string achievementId;
    public int characterId;
}