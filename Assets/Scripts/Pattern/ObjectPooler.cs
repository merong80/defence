﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public sealed class ObjectPooler<T> where T : MonoBehaviour, new()
{
	private readonly object _threadLocker;
	//private readonly Queue<T> _poolManager;

	private Dictionary<string,Queue<T>> _poolManager;
	private Dictionary<string,GameObject> resources;
	private readonly string path;
	private Transform parent;

	public ObjectPooler(string path)
	{
		_poolManager = new Dictionary<string, Queue<T>> ();
		resources = new Dictionary<string, GameObject> ();
		_threadLocker = new object();
		this.path = path;
	}

	public bool Has(string name )
	{
		return _poolManager.ContainsKey (name);
	}
	public T Get(string name, Transform parent)
	{
		return Get (name, parent, Vector3.one, Vector3.zero);
	}

    public T Get(string name, Transform parent, Vector3 scale, Vector3 position)
    {
        return Get(name, parent, scale, position, false);
    }

    public T Get(string name, Transform parent, Vector3 scale, Vector3 position, bool relativeLocal)
	{
		lock (_threadLocker)
		{
			T returnValue = null;
			if (_poolManager.ContainsKey (name)) {
				if (_poolManager [name].Count > 0) {
					returnValue = _poolManager [name].Dequeue ();
				}
			}
			if (returnValue == null) {
				if (!resources.ContainsKey (name)) {
					GameObject go = Resources.Load (path+name) as GameObject;
					resources.Add (name, go);
				}
				GameObject stageGo = UnityEngine.Object.Instantiate (resources [name],parent) as GameObject;
				returnValue = stageGo.GetComponent <T>();
			}
			returnValue.gameObject.name = name;
			returnValue.transform.parent = parent;
			returnValue.transform.localScale = scale;
            if( relativeLocal )
                returnValue.transform.localPosition = position;
            else
                returnValue.transform.position = position;
			return returnValue;
		}
	}
	public int Count()
	{
		return _poolManager.Count;
	}
	public void Put(T obj)
	{ 
		lock(_threadLocker)
		{
			if (_poolManager.ContainsKey (obj.gameObject.name)) {
				_poolManager [obj.gameObject.name].Enqueue (obj);
			} else {
				Queue<T> que = new Queue<T> ();
				que.Enqueue (obj);
				_poolManager.Add (obj.gameObject.name, que);
			}
		}
	}

	public T GetRemove(string key)
	{
		T remove = null;
		if (_poolManager.ContainsKey (key)) {
			if (_poolManager [key].Count > 0) {
				remove = _poolManager [key].Dequeue ();
			}
			if (_poolManager [key].Count == 0)
				_poolManager.Remove (key);
		}

		return remove;
	}
}	


