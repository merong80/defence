﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {

	private static T _instance;
	public static T Instance
	{
		get {
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType(typeof(T)) as T;
				if (_instance == null) {
					_instance = new GameObject ().AddComponent<T> ();
					_instance.gameObject.name = _instance.GetType ().FullName;
				}
			}
			return _instance;
		}
	}

	protected virtual void Awake()
	{
		if( _instance == null ) _instance = this as T;
	}

	public static bool HasInstance {
		get {
			return !IsDestroyed;
		}
	}

	public static bool IsDestroyed {
		get {
			if(_instance == null) {
				return true;
			} else {
				return false;
			}
		}
	}

	protected virtual void OnDestroy () {
		_instance = null;
	}

	protected virtual void OnApplicationQuit () {
		_instance = null;
	}
}
