﻿using UnityEngine;
using System.Collections;

public class LabListItem : ListItem
{
	private const string borderActiveName = "button__06";
	private const string borderUnActiveName = "button__07";

	[SerializeField]
	private UISprite iconSprite;
	[SerializeField]
	private UISprite borderSprite;
	[SerializeField]
	private UILabel levelLabel;
	[SerializeField]
	private UILabel nameLabel;

	private MainTabPageLab tabPage;
	private TowerLevelType type;

	public void SetData(MainTabPageLab tabPage,TowerLevelType type)
	{
		this.tabPage = tabPage;
		this.type = type;
		iconSprite.spriteName = type.ToString ();
		iconSprite.MakePixelPerfect ();
		nameLabel.text = TextManager.Get (type.ToString () + "_name");

		if (type == TowerLevelType.SKILL) {
			levelLabel.text = "Lv."+GameManager.Instance.GetPlayerSkillData ().level.ToString ();
		} else {
			LevelupData data = GameManager.Instance.GetPlayerTowerLevelData (type);
			levelLabel.text = "Lv." + data.level.ToString ();
		}
	}

	public void Active(TowerLevelType type)
	{
		borderSprite.spriteName = this.type == type ? borderActiveName : borderUnActiveName;
	}

	public void ClickItem()
	{
		tabPage.ClickLabListItem (type);
	}
}

