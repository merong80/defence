﻿using UnityEngine;
using System.Collections;

public class StageSelectListItem : ListItem
{
	[SerializeField]
	private UILabel stageLabel;
	[SerializeField]
	private UISprite borderSprite;

	private bool isActive;
	public void Active(bool active)
	{
		isActive = active;
		borderSprite.spriteName = active ? "button__11" : "button__12";
	}
	private PopupStageSelect stageSelectPopup;
	private int stage;
	public void SetData(int stage)
	{
		this.stage = stage;
		stageLabel.text = "STAGE " + stage.ToString ();
	}

	public void ClickStage()
	{
		if (stageSelectPopup == null) {
			stageSelectPopup = transform.GetComponentInParent<PopupStageSelect> ();
		}
		if( isActive ) stageSelectPopup.StartStage (stage);
	}
}

