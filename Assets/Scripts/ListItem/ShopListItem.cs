﻿using UnityEngine;
using System.Collections;

public class ShopListItem : ListItem
{

	private const string borderActiveName = "button__06";
	private const string borderUnActiveName = "button__07";

	[SerializeField]
	private UISprite iconSprite;
	[SerializeField]
	private UISprite borderSprite;
	[SerializeField]
	private UILabel costLabel;
	[SerializeField]
	private UILabel coinLabel;
	private GoogleProductTemplate data;
	private MainTabPageShop page;
	public void SetData(MainTabPageShop page,GoogleProductTemplate data)
	{
		Debug.Log ("SetData   " + data.SKU);
		this.page = page;
		this.data = data;
		if( data.LocalizedPrice != null ) costLabel.text = data.LocalizedPrice;
		int coinAmount = PurchaseManager.Instance.GetCoinAmount (data.SKU);
		coinLabel.text = "X " + string.Format ("{0:n0}", coinAmount);
		iconSprite.spriteName = data.SKU;
		iconSprite.MakePixelPerfect ();
	}

	public void Active(GoogleProductTemplate data)
	{
		Debug.Log (this.data.SKU + "//" + data.SKU);
		borderSprite.spriteName = this.data.SKU == data.SKU ? borderActiveName : borderUnActiveName;
	}

	public void ClickItem()
	{
		//tabPage.ClickLabListItem (type);
		page.ClickShopListItem(data);
	}
}

