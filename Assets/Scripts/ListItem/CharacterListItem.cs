﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterListItem : ListItem {

	[SerializeField]
	private UISprite borderSprite;
	[SerializeField]
	private UILabel levelLabel;
	[SerializeField]
	private UILabel nameLabel;
	[SerializeField]
	private UISprite characterSprite;
	private const string borderActiveName = "button__06";
	private const string borderUnActiveName = "button__07";
	private MainTabPageCharacter tabPage;
	public void SetData(MainTabPageCharacter tabPage, CharacterBattleData data)
	{
		this.tabPage = tabPage;
		this.data = data;
		//borderSprite.color = data.level > 0 ? Color.white : Color.black;

		levelLabel.text = "Lv." + data.level.ToString ();
		nameLabel.text = TextManager.Get(data.name.ToString ()+"_name");
		characterSprite.spriteName = data.name;
		characterSprite.color = data.level > 0 ? Color.white : Color.black;
		characterSprite.MakePixelPerfect ();
	}

	public void Active(int id)
	{
		borderSprite.spriteName = id == data.id ? borderActiveName : borderUnActiveName;
		if (this.data.level <= 0) {
			borderSprite.color = GameManager.Instance.GetPlayerCoin () >= GameManager.Instance.GetCharacterBuyCost (data.id) ? Color.white : Color.gray;
		}
	}

	private CharacterBattleData data;

	public void ClickItem()
	{
		tabPage.ClickCharacterItem (data);
	}
}
