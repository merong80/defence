﻿using UnityEngine;
using System.Collections;

public class StageCharacterListItem : ListItem
{
	[SerializeField]
	private UISprite characterSprite;

	private int characterId;
	private PopupStageSelect selectPopup;
	public void SetData(PopupStageSelect popup,CharacterBattleData data)
	{
		selectPopup = popup;
		characterSprite.color = data.level > 0 ? Color.white : Color.black;
		characterSprite.spriteName = data.name;
		characterSprite.MakePixelPerfect ();

		if (data.level > 0) {
			characterId = data.id;
			characterSprite.color = Color.white;
		} else {
			characterId = 0;
			characterSprite.color = Color.black;
		}
	}

	public void ClickItem()
	{
		if (characterId == 0)
			return;
		selectPopup.ClickCharacter (characterId);
	}
}

