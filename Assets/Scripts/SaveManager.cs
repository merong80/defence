﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;

public class SaveManager : Singleton<SaveManager>
{
	protected override void Awake ()
	{
		base.Awake ();
		DontDestroyOnLoad (this);
	}

	public void SaveData(PlayerData data)
	{
		string src = JsonConvert.SerializeObject (data);
		string encryptSrc = Util.Encrypt (src);
		PlayerPrefs.SetString (PrefsKey.PLAYERDATA,encryptSrc);
	}

	//--------------------
	// use by GPManager to MakeSave (return decrypted src)
	//----------------------
	public string GetSaveData()
	{
		PlayerData data = null;
		if (!PlayerPrefs.HasKey (PrefsKey.PLAYERDATA)) {
			data = new PlayerData (GameManager.PlayerID);
			return JsonConvert.SerializeObject (data);
		}
		return PlayerPrefs.GetString (PrefsKey.PLAYERDATA);
	}

	public PlayerData LoadData()
	{
		PlayerData data;
		if (!PlayerPrefs.HasKey (PrefsKey.PLAYERDATA)) {
			data = new PlayerData (GameManager.PlayerID);
			SaveData (data);
			return data;
		}
		string decryptSrc = PlayerPrefs.GetString (PrefsKey.PLAYERDATA);
		string src = Util.Decrypt (decryptSrc);
		data = JsonConvert.DeserializeObject<PlayerData> (src);
		return data;
	}


}

