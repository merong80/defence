﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MainTabType
{
	HEROES = 0,
	LAB = 1,
	SHOP = 2,
}

public class MainManager : Singleton<MainManager> {
	public UICamera uiCamera;
	public MainTabButton[] tabButtons;
	private MainTabType currentTab;

	public void ClickTab(MainTabType type)
	{
		if (type == MainTabType.SHOP) {
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				PopupManager.Instance.Message (TextManager.Get (TextKey.no_network));
				return;
			}
			if (!AndroidInAppPurchaseManager.Client.IsConnected) {
				PurchaseManager.Instance.ConnectShop ();
				return;
			}
		}
		currentTab = type;
		for (int i = 0; i < tabButtons.Length; i++) {
			tabButtons [i].Active (currentTab);
		}
	}

	void Start()
	{
        SoundManager.Instance.PlayMainBGM();
		Init ();
	}
    private bool isOpenRoulette;
    //private bool isInit = false;
	public void Init()
	{
        //if (isInit) return;
        //isInit = true;
		for (int i = 0; i < tabButtons.Length; i++) {
			tabButtons [i].Init ();
			tabButtons [i].Active (currentTab);
		}
        UpdateMovingCharacter();

        UpdateAdvertiseRemain();
        if(!isOpenRoulette && GameManager.Instance.CheckRoulette() )
        {
            PopupManager.Instance.Open("roulette");
            isOpenRoulette = true;
        }
    }

    private void UpdateAdvertiseRemain()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            advertiseRemainLabel.text = "Show Video";
            return;
        }
        advertiseRemain = AdvertiseManager.Instance.GetRemainRewardTime();
        if (advertiseRemain > 0)
        {
            advertiseRemainLabel.text = "-" + Util.GetTimeString(advertiseRemain);
            if (advertiseCheckCoroutine != null) StopCoroutine(advertiseCheckCoroutine);
            advertiseCheckCoroutine = StartCoroutine(CheckAdvertiseRemain());
        }
        else
        {
            advertiseRemainLabel.text = "Show Video";
        }
    }

    private Coroutine advertiseCheckCoroutine;
    private IEnumerator CheckAdvertiseRemain()
    {
        while(advertiseRemain > 0)
        {
            yield return new WaitForSeconds(1f);
            advertiseRemain -= 1;
            advertiseRemainLabel.text = "-" + Util.GetTimeString(advertiseRemain);

        }
        advertiseRemainLabel.text = "Show Video";
    }

    [SerializeField]
	private UILabel playerCoinLabel;
	public void UpdateCoin(long coin)
	{
		playerCoinLabel.text = string.Format ("{0:n0}", coin);
	}

	public void ClickReady()
	{
		PopupManager.Instance.Open ("stage_select");
	}

	public void ClickStart()
	{
		//GameManager.Instance.LoadLevel (2);
	}

    public void ClickExplore()
    {
        PopupManager.Instance.Open("explore");
    }

    public void ClickOption()
    {
        PopupManager.Instance.Open("option_main");
    }

    //치트키
	public void ClickChargeCoin()
	{
		GameManager.Instance.ChargeCoin (1000);
	}

	public void ClickStageClear()
	{
		GameManager.Instance.ClearStage += 1;
		GameManager.Instance.CurrentStage += 1;
		//Debug.Log(Util.CurrentTime ());
	}

	public void ClickInitData()
	{
		PopupManager.Instance.Message ("initializing data?",true,delegate(bool confirm) {
			if( confirm ) GameManager.Instance.TestInitPlayerData ();
		});
	}

    public void ClickExploringComplete()
    {
        GameManager.Instance.TestExploringComplete();
    }
    //

    [SerializeField]
    private MainMovingCharacterManager movingCharacterManager;
    public void UpdateMovingCharacter()
    {
        if (movingCharacterManager != null) movingCharacterManager.UpdateCharacter();
    }

	[SerializeField]
	private UILabel advertiseRemainLabel;

	private int advertiseRemain;
	public void ClickAdvertise()
	{
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            PopupManager.Instance.Message(TextManager.Get(TextKey.no_network));
            return;
        }
        if (advertiseRemain > 0)
			return;

		AdvertiseManager.Instance.ShowRewardVideo ();
	}

	void Update()
	{

	}

    public void ClickShowLeaderboard()
    {
		if (Application.internetReachability == NetworkReachability.NotReachable) {
			PopupManager.Instance.Message (TextManager.Get (TextKey.no_network));
			return;
		}
        GPManager.Instance.ShowLeaderBoardsUI();
    }

    public void ClickShowAchieve()
    {
		if (Application.internetReachability == NetworkReachability.NotReachable) {
			PopupManager.Instance.Message (TextManager.Get (TextKey.no_network));
			return;
		}
        GPManager.Instance.ShowAchievementsUI();
    }
}
