﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainTabPageShop : MainTabPage
{
	private List<GoogleProductTemplate> dataList;
	public override void Init()
	{
		base.Init ();

		dataList = PurchaseManager.Instance.GetStoreItems ();
		data = dataList.Count > 0 ? dataList[0] : null;
		UpdateList ();
		UpdateInfoWindow ();
	}

	private void UpdateList()
	{
		InitListItems (dataList.Count);

		for (int i = 0; i < listItems.Count; i++) {
			ShopListItem item = (ShopListItem)listItems [i];
			item.SetData (this,dataList[i]);
			item.Active (data);
		}
	}
	private GoogleProductTemplate data;
	public void ClickShopListItem(GoogleProductTemplate data)
	{
		this.data = data;
		UpdateList ();
		UpdateInfoWindow ();
	}

	public void ClickPurchase()
	{
		PurchaseManager.Instance.Purchase (this.data.SKU);
		//GameManager.Instance.LevelUpTower (type);
		//UpdateList ();
		//UpdateInfoWindow ();
	}

	[SerializeField]
	private UISprite iconSprite;
	[SerializeField]
	private UILabel descLabel;

	[SerializeField]
	private UILabel buttonLabel;
	[SerializeField]
	private UILabel costLabel;

	public void UpdateInfoWindow()
	{
		iconSprite.spriteName = data.SKU;
		iconSprite.MakePixelPerfect ();
		descLabel.text = data.Description;
		buttonLabel.text = TextManager.Get (TextKey.purchase);
		costLabel.text = data.LocalizedPrice;
	}
}

