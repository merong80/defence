﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCharacterMoving : MonoBehaviour {

	[SerializeField]
	private UISprite sprite;
	[SerializeField]
	private UISpriteAnimation spriteAnimation;

	[SerializeField]
	private float speed = 100;
	[SerializeField]
	private float leftBoundary;
	[SerializeField]
	private float rightBoundary;

	[SerializeField]
	private float idleMinTime;
	[SerializeField]
	private float idleMaxTime;

	private float targetPosition;

	//public void 

    public void SetData(float speed, float leftBoundary, float rightBoundary)
    {
        this.speed = speed;
        this.leftBoundary = leftBoundary;
        this.rightBoundary = rightBoundary;
        CheckMoving();
    }

    void OnDisable()
    {
        moving = false;
    }

    void Start()
	{
		//CheckMoving ();
	}

	void Update()
	{
		if (moving) {
			Vector3 pos = transform.localPosition;
			transform.localPosition = new Vector3 (facingRight ? pos.x+speed * Time.deltaTime : pos.x-speed * Time.deltaTime, pos.y, pos.z);
			//transform.Translate (facingRight ? speed*Time.deltaTime : -speed*Time.deltaTime,0f,0f);
			if( (!facingRight && transform.localPosition.x <= targetPosition) || (facingRight && transform.localPosition.x >= targetPosition) ){
				moving = false;
				CheckMoving ();
			}
		}
	}

	public void CheckMoving()
	{
		if (Random.Range (0, 2) == 0) {
			Moving ();
		} else {
			Idle ();
		}
	}

	private void Idle()
	{
		Invoke ("Moving", Random.Range (idleMinTime, idleMaxTime));
		spriteAnimation.namePrefix = "idle";
		spriteAnimation.ResetToBeginning ();
	}

	private void Moving()
	{
		Flip ();
		targetPosition = !facingRight ? Random.Range (leftBoundary, transform.localPosition.x) : Random.Range (transform.localPosition.x,rightBoundary);
		spriteAnimation.namePrefix = "walk";
		spriteAnimation.ResetToBeginning ();
		moving = true;
		//Debug.Log ("Moving   " + targetPosition + " // " + transform.localPosition.x + "//" + facingRight);
	}

	private bool moving = false;
	private bool facingRight = false;
	private void Flip()
	{
		facingRight = !facingRight;
		sprite.flip = facingRight ? UIBasicSprite.Flip.Nothing : UIBasicSprite.Flip.Horizontally;
	}
}
