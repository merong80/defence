﻿using UnityEngine;
using System.Collections;

public class MainTabButton : MonoBehaviour
{
	[SerializeField]
	private MainTabType type;
	[SerializeField]
	private UISprite sprite;
	[SerializeField]
	private string spriteNameActive;
	[SerializeField]
	private string spriteNameUnActive;
	[SerializeField]
	private UILabel label;
	[SerializeField]
	private MainTabPage tabPage;

	public void Init()
	{
		/*switch (type) {
		case MainTabType.HEROES: 
			label.text = TextManager.Get (TextKey.heroes);
			break;
		case MainTabType.LAB: 
			label.text = TextManager.Get (TextKey.lab);
			break;
		case MainTabType.SHOP: 
			label.text = TextManager.Get (TextKey.shop);
			break;
		}*/
	}

	public void Active(MainTabType type)
	{
		sprite.spriteName = this.type == type ? spriteNameActive : spriteNameUnActive;
		label.color = this.type == type ? Color.white : Color.black;
		label.effectStyle = this.type == type ? UILabel.Effect.Outline8 : UILabel.Effect.None;
		tabPage.gameObject.SetActive (this.type == type);
		if (this.type == type)
			tabPage.Init ();
	}

	public void ClickButton()
	{
		MainManager.Instance.ClickTab (type);
	}
}

