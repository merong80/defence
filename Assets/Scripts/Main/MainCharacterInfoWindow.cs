﻿using UnityEngine;
using System.Collections;

public class MainCharacterInfoWindow : MonoBehaviour
{
	[SerializeField]
	private UISprite characterSprite;
	[SerializeField]
	private UILabel levelLabel;
	[SerializeField]
	private UILabel hpCurrentLabel;
	[SerializeField]
	private UILabel hpNextLabel;
	[SerializeField]
	private UILabel atkCurrentLabel;
	[SerializeField]
	private UILabel atkNextLabel;
	[SerializeField]
	private GameObject hpArrow;
	[SerializeField]
	private GameObject atkArrow;

    [SerializeField]
    private GameObject upgradeObjGrade;
    [SerializeField]
    private UILabel upgradeCurrentCardLabel;
    [SerializeField]
    private UILabel upgradeCostGrade;
    [SerializeField]
    private UILabel upgradeGradeButtonLabel;


    [SerializeField]
    private GameObject upgradeObjLevel;
    [SerializeField]
	private UILabel upgradeLevelButtonLabel;
    [SerializeField]
	private UILabel upgradeCostLevel;

	[SerializeField]
	private UILabel descLabel;
    [SerializeField]
    private UILabel currentCardLabel;
    [SerializeField]
    private GameObject[] characterGradeObjs;

	//private CharacterBattleData data;
	public void SetData(CharacterBattleData data)
	{
		//this.data = data;
		characterSprite.spriteName = data.name;
		characterSprite.color = data.level > 0 ? Color.white : Color.black;
		characterSprite.MakePixelPerfect ();
		levelLabel.text = data.level.ToString ();
		descLabel.text = TextManager.Get (data.name + "_desc");
        currentCardLabel.text = data.cards.ToString();
        if (data.level >= GameManager.Instance.CHARACTER_MAX_LEVEL[data.grade]) {
            upgradeObjGrade.SetActive(true);
            upgradeObjLevel.SetActive(false);

            if (data.grade >= GameManager.CHARACTER_MAX_GRADE)
            {
                upgradeGradeButtonLabel.text = "MAX";
                upgradeCostGrade.text = "";
                upgradeCurrentCardLabel.text = data.cards.ToString();
                upgradeCurrentCardLabel.color = Color.white;
            }
            else
            {
                upgradeGradeButtonLabel.text = TextManager.Get(TextKey.character_upgrade);
                upgradeCostGrade.text = "/"+GameManager.Instance.upgradeNeedCards[data.grade];
                upgradeCurrentCardLabel.text = data.cards.ToString();
                upgradeCurrentCardLabel.color = data.cards >= GameManager.Instance.upgradeNeedCards[data.grade] ? Color.white : Color.red;
            }
            //buttonLabel.text = "MAX";
        } else {
            upgradeObjGrade.SetActive(false);
            upgradeObjLevel.SetActive(true);
            upgradeLevelButtonLabel.text = data.level > 0 ? TextManager.Get(TextKey.character_levelup) : TextManager.Get(TextKey.buy);

            upgradeCostLevel.text = data.level > 0 ? GameManager.Instance.GetCharacterUpgradeCost(data.id).ToString() : GameManager.Instance.GetCharacterBuyCost(data.id).ToString();
        }

		
		if (data.level > 0) {
			CharacterBattleData nextData = GameManager.Instance.GetCharacterBattleData (data.id, data.level + 1);
			hpNextLabel.gameObject.SetActive (true);
			hpArrow.SetActive (true);
			atkNextLabel.gameObject.SetActive (true);
			atkArrow.SetActive (true);
			hpCurrentLabel.text = data.maxHp.ToString ();
			hpNextLabel.text = nextData.maxHp.ToString ();
			atkCurrentLabel.text = data.atk.ToString ();
			atkNextLabel.text = nextData.atk.ToString ();
            for (int i = 0; i < characterGradeObjs.Length; i++)
            {
                characterGradeObjs[i].SetActive(i <= data.grade);
            }
        } else {
            for( int i =0; i < characterGradeObjs.Length; i++ )
            {
                characterGradeObjs[i].SetActive(false);
            }
            hpNextLabel.gameObject.SetActive (false);
			hpArrow.SetActive (false);
			atkNextLabel.gameObject.SetActive (false);
			atkArrow.SetActive (false);
			hpCurrentLabel.text = data.maxHp.ToString ();
			atkCurrentLabel.text = data.atk.ToString ();
		}

	}
}

