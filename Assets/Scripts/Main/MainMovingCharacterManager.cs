﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMovingCharacterManager : MonoBehaviour
{
    private const string path = "Prefabs/Character/Main/";
    private ObjectPooler<MainCharacterMoving> pooler = new ObjectPooler<MainCharacterMoving>(path);
    [SerializeField]
    private float ourHeight;
    [SerializeField]
    private float ourLeftBoundary;
    [SerializeField]
    private float ourRightBoundary;

    private Dictionary<string, MainCharacterMoving> currentOurs = new Dictionary<string,MainCharacterMoving>();

    public void UpdateCharacter()
    {
        UpdateOurs();
        UpdateEnemies();
    }
    public void UpdateOurs()
    {
        List<CharacterBattleData> list = GameManager.Instance.GetBattlePlayerCharacterData();
        List<string> removeKey = new List<string>();
        foreach(KeyValuePair<string,MainCharacterMoving> pair in currentOurs)
        {
            bool remove = true;
            for( int i = 0; i < list.Count; i++ )
            {
                if(list[i].name == pair.Key)
                {
                    remove = false;
                    break;
                }
            }
            if (remove) removeKey.Add(pair.Key);
        }
        for( int i = 0; i < removeKey.Count; i++ )
        {
            currentOurs[removeKey[i]].gameObject.SetActive(false);

            pooler.Put(currentOurs[removeKey[i]]);
            currentOurs.Remove(removeKey[i]);
        }
        
        for( int i = 0; i < list.Count; i++ )
        {
            if (currentOurs.ContainsKey(list[i].name) ) continue;
            MainCharacterMoving character = pooler.Get(list[i].name,this.transform,Vector3.one,new Vector3(Random.Range(ourLeftBoundary,ourRightBoundary),ourHeight,0),true);
            character.gameObject.SetActive(true);
            character.SetData(list[i].moveSpeed, ourLeftBoundary, ourRightBoundary);
            currentOurs.Add(list[i].name,character);
        }
    }

    public void UpdateEnemies()
    {
        List<CharacterBattleData> list = GameManager.Instance.GetPlayerGetBossCharacterData();
        List<string> removeKey = new List<string>();
        foreach (KeyValuePair<string, MainCharacterMoving> pair in currentEnemies)
        {
            bool remove = true;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].name == pair.Key)
                {
                    remove = false;
                    break;
                }
            }
            if (remove) removeKey.Add(pair.Key);
        }
        for (int i = 0; i < removeKey.Count; i++)
        {
            currentEnemies[removeKey[i]].gameObject.SetActive(false);

            pooler.Put(currentEnemies[removeKey[i]]);
            currentEnemies.Remove(removeKey[i]);
        }

        for (int i = 0; i < list.Count; i++)
        {
            if (currentEnemies.ContainsKey(list[i].name)) continue;
            MainCharacterMoving character = pooler.Get(list[i].name, this.transform, Vector3.one, new Vector3(Random.Range(enemyLeftBoundary, enemyRightBoundary), enemyHeight, 0), true);
            character.gameObject.SetActive(true);
            character.SetData(list[i].moveSpeed, enemyLeftBoundary, enemyRightBoundary);
            currentEnemies.Add(list[i].name, character);
        }
    }

    [SerializeField]
    private float enemyHeight;
    [SerializeField]
    private float enemyLeftBoundary;
    [SerializeField]
    private float enemyRightBoundary;

    private Dictionary<string, MainCharacterMoving> currentEnemies = new Dictionary<string, MainCharacterMoving>();
}
