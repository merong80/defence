﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainTabPage : MonoBehaviour
{
	[SerializeField]
	protected UIGrid grid;
	[SerializeField]
	protected GameObject listObj;

	protected List<ListItem> listItems = new List<ListItem>();

	public virtual void Init()
	{

	}

	protected virtual void InitListItems(int count)
	{
		int currentCount = listItems.Count;
		if (currentCount < count) {
			for (int i = 0; i < count; i++) {
				if (currentCount > i) {
					listItems [i].gameObject.SetActive (true);
				} else {
					GameObject go = Instantiate (listObj);
					go.transform.parent = grid.transform;
					go.transform.localScale = Vector3.one;
					listItems.Add (go.GetComponent<ListItem> ());
				}
			}
		} else if (listItems.Count > count) {
			for (int i = count; i < listItems.Count; i++) {
				listItems [i].gameObject.SetActive (false);
			}
		}
		grid.Reposition ();
	}

}

