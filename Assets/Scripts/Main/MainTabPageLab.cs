﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainTabPageLab : MainTabPage
{
	public override void Init()
	{
		base.Init ();
		UpdateList ();
		UpdateInfoWindow ();
	}

	private void UpdateList()
	{
		InitListItems ((int)TowerLevelType.MAX);

		for (int i = 0; i < listItems.Count; i++) {
			LabListItem item = (LabListItem)listItems [i];
			item.SetData (this,(TowerLevelType)i);
			item.Active (type);
		}
	}
	private TowerLevelType type;
	public void ClickLabListItem(TowerLevelType type)
	{
		this.type = type;
		UpdateList ();
		UpdateInfoWindow ();
	}

	public void ClickUpgrade()
	{
		GameManager.Instance.LevelUpTower (type);
		UpdateList ();
		UpdateInfoWindow ();
	}

	[SerializeField]
	private UISprite iconSprite;
	[SerializeField]
	private UILabel descLabel;
	[SerializeField]
	private UILabel levelLabel;

	[SerializeField]
	private UILabel buttonLabel;
	[SerializeField]
	private UILabel costLabel;

	public void UpdateInfoWindow()
	{
		iconSprite.spriteName = type.ToString ();
		iconSprite.MakePixelPerfect ();
		descLabel.text = TextManager.Get (type.ToString () + "_desc");


		if (type == TowerLevelType.SKILL) {
			SkillData currentData = GameManager.Instance.GetPlayerSkillData ();
			SkillData nextData = GameManager.Instance.GetPlayerNextSkillData ();
			levelLabel.text = currentData.level.ToString ();
			costLabel.text = nextData == null ? "MAX" : nextData.cost.ToString ();
			buttonLabel.text = nextData == null ? "MAX" : "UPGRADE";
		} else {
			LevelupData currentData = GameManager.Instance.GetPlayerTowerLevelData (type);
			LevelupData nextData = GameManager.Instance.GetPlayerNextTowerLevelData (type);

			levelLabel.text = currentData.level.ToString ();
			costLabel.text = nextData == null ? "MAX" : nextData.cost.ToString ();
			buttonLabel.text = nextData == null ? "MAX" : "UPGRADE";
		}
		/*currentLabel.text = currentData.value.ToString ();
		if (nextData == null) {
			nextLabel.gameObject.SetActive (false);
			arrow.SetActive (false);
		} else {
			nextLabel.text = nextData.value.ToString ();
			nextLabel.gameObject.SetActive (true);
			arrow.SetActive (true);
		}*/

	}
}

