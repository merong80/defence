﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainTabPageCharacter : MainTabPage
{
	public override void Init()
	{
		base.Init ();
		selectedCharacterData = null;
		Dictionary<int,CharacterBattleData> characterDatas = GameManager.Instance.GetPlayerCharacterData ();
		foreach (KeyValuePair<int, CharacterBattleData> pair in characterDatas) {
			if (selectedCharacterData == null) {
				selectedCharacterData = pair.Value;
				break;
			} else {
				break;
			}
		}
		UpdateCharacterInfo (selectedCharacterData);
		UpdateCharacterList ();
	}

	public void ClickCharacterItem(CharacterBattleData data)
	{
		UpdateCharacterInfo (data);
        for( int i = 0; i < listItems.Count; i++ )
        {
            CharacterListItem cItem = (CharacterListItem)listItems[i];
            cItem.Active(selectedCharacterData.id);
        }
	}

	[SerializeField]
	private MainCharacterInfoWindow characterInfoWindow;
	public void UpdateCharacterInfo(CharacterBattleData data)
	{
		selectedCharacterData = data;
		characterInfoWindow.SetData (selectedCharacterData);
	}

	private CharacterBattleData selectedCharacterData;

	public void ClickCharacterBuyUpgrade()
	{
        if (selectedCharacterData.level > 0)
        {
            if( selectedCharacterData.level >= GameManager.Instance.CHARACTER_MAX_LEVEL[selectedCharacterData.grade] )
            {
                GameManager.Instance.UpgradeCharacter(selectedCharacterData.id);
            }
            else
            {
                GameManager.Instance.LevelupCharacter(selectedCharacterData.id);
            }

        }
		else
			GameManager.Instance.BuyCharacter (selectedCharacterData.id);

		selectedCharacterData = GameManager.Instance.GetCharacterBattleData (selectedCharacterData.id);
		UpdateCharacterList ();
	}

	public void UpdateCharacterList()
	{
		Dictionary<int,CharacterBattleData> characterDatas = GameManager.Instance.GetPlayerCharacterData ();
		InitListItems (characterDatas.Count);

		int idx = 0;
		foreach (KeyValuePair<int,CharacterBattleData> pair in characterDatas) {
			//pair.Value.SetData (characterDatas[pair.Key]);
			CharacterListItem cItem = (CharacterListItem)listItems [idx];
			cItem.SetData (this,pair.Value);
            cItem.Active(selectedCharacterData.id);
			idx++;
		}

		UpdateCharacterInfo (selectedCharacterData);
	}
}

