﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TitleManager : Singleton<TitleManager>
{
	[SerializeField]
	private UISprite guageBar;

	private Tweener barTweener;
	public void SetGuage(float percent)
	{
		if (barTweener != null) {
			barTweener.Kill ();
		}
		barTweener = DOTween.To (() => guageBar.fillAmount, x => guageBar.fillAmount = x, percent, 0.2f);
	}
}

