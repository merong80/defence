﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerData
{
	public string id;
	public List<int> hasCharacter;
	public Dictionary<int,int> characterLevel;
    //업데이트
    public Dictionary<int, int> characterGrade;
    public Dictionary<int, int> characterCards;

    public long exploreStartTime;
    public int exploreId;
    public RewardData exploreRewardData = null;

    public string rouletteRewardDay;
    //
	public Dictionary<TowerLevelType,int> towerLevel;
	public int clearStage;
	public int currentStage;
	public long coin;
	public List<int> battleCharacter;
    public List<int> getBossCharacter;
	public long gameStartTime;
	public long adRewardTime;

	public PlayerData(string id)
	{
		this.id = id;
		hasCharacter = new List<int> (){ 101 };
		battleCharacter = new List<int> (){ 101 };
		characterLevel = new Dictionary<int, int> (){ { 101,1 } };

        characterGrade = new Dictionary<int, int>() { { 101, 0 } };
        characterCards = new Dictionary<int, int>() { { 101, 0 } };

        getBossCharacter = new List<int>();
		towerLevel = new Dictionary<TowerLevelType, int> ();
		for (int i = 0; i < (int)TowerLevelType.MAX; i++) {
			towerLevel.Add ((TowerLevelType)i, 1);
		}
		currentStage = 1;
		clearStage = 0;
		coin = 0;
		//gameStartTime = Util.CurrentTime ();
	}
}

public class RewardData
{
    public int coin;
    public Dictionary<int, int> cards = new Dictionary<int, int>();
}

public enum RewardType
{
    NONE = 0,
    COIN = 1,
    CARDS = 2,
}
public class RewardItemData
{
    public RewardType type;
    public int id;
    public int amount;
}

public class PrefsKey
{
	public const string BGM = "BGM";
	public const string PLAYERDATA = "PLAYERDATA";
}

public enum TowerLevelType
{
	MONEY_SPEED = 0,
	MONEY_MAX = 1,
	HP = 2,
	CREATE_COOL = 3,
	MONEY_KILL = 4,
	COIN_KILL = 5,
	SKILL = 6,
	MAX = 7,
}

public class TowerLevelData
{
	public int speed = 1;
	public int max = 1;
	public int hp = 1;
	public int createCool = 1;
	public int killMoney = 1;
}

public class LevelupData
{
	public int level;
	public int cost;
	public float value;
}

public class SkillData
{
	public int level;
	public int cost;
	public float cool;
	public int damage;
	public float forceX;
	public float forceY;
	public float range;
}

public class TowerLevelModelData
{
	//public EnemyTowerData[] enemyTower;
	public TowerMoneyLevel[] towerMoney;
	public LevelupData[] moneySpeed;
	public LevelupData[] moneyMax;
	public LevelupData[] hp;
	public LevelupData[] createCool;
	public LevelupData[] killMoney;
	public LevelupData[] killCoin;
	public SkillData[] skill;
	public AdvertiseRewardData[] ad_reward;
}

public class CharacterModelData
{
	public CharacterData[] character;
	public CharacterData[] enemy;
	public CharacterData[] boss;
	public StageData[] stageData;
    public ExploreData[] explore;
	//public EnemyCreateModelData[] enemyCreate;
	//public TowerEnemyCreateModelData[] towerEnemyCreate;
}

public class ExploreData
{
    public int id;
    public int area;
    public int hour;
    public int unlockStage;
    public int[] drop;
    public int[] drop_rate;
    public int coin_min;
    public int coin_max;
}

public class StageData
{
	public int stage;
	public int[] enemyIds;
	public int[] enemyRates;
	public float initTime;
    public int createNum;
    public float delay;
	public float termMin;
	public float termMax;
	public int[] hpCreate;
	public int[] hpCreateEnemy;
	public int[] hpCreateBoss;
	public int maxHp;
	public int distance;
	public int firstCoin;
}

public class EnemyPatternData
{
	public EnemyPattern[] patterns;
}

public class EnemyPattern
{
	public int id;
	public float time;
	public int enemyId;
}

public class EnemyCreateData
{
	public int id;
	public Dictionary<int, int> enemies = new Dictionary<int, int> ();
	public float initTime;
    public int createNum;
    public float delay;
	public float termMin;
	public float termMax;
	public int GetId
	{
		get{
			int ran = Random.Range (0, 100);
			int sum = 0;
			int cnt = 0;
			foreach (KeyValuePair<int,int> pair in enemies) {
				sum += pair.Value;
				if (ran < sum) {
					return pair.Key;
				}
				cnt += 1;
				if (cnt == enemies.Count) {
					return pair.Key;
				}
			}
			return 0;
		}
	}

	public EnemyCreateData(StageData data)
	{
        this.termMin = data.termMin;
        this.termMax = data.termMax;
        this.initTime = data.initTime;
        this.createNum = data.createNum;
        this.delay = data.delay;
		for (int i = 0; i < data.enemyIds.Length; i++) {
			enemies.Add (data.enemyIds [i], data.enemyRates [i]);
		}
	}

}

public class TowerEnemyCreateData
{
	public int hp;
	public int enemyNum;
	public int bossId;
}

public class AdvertiseRewardData
{
	public int level;
	public long reward;
}


/*public class EnemyTowerData
{
	public int stage;
	public int hp;
	public int distance;
	public int firstCoin;
}

public class EnemyCreateModelData
{
	public int id;
	public int enemyId;
	public int stageMin;
	public int stageMax;
	public float initTimeMin;
	public float initTimeMax;
	public float termMin;
	public float termMax;
	public float stageMaxTermMin;
	public float stageMaxTermMax;
}

public class TowerEnemyCreateModelData
{
	public int stage;
	public int hp;
	public int enemyNum;
	public int bossId;
}

public class EnemyCreateData
{
	public int id;
	public int enemyId;
	public float initTimeMin;
	public float initTimeMax;
	public float termMin;
	public float termMax;
	public EnemyCreateData(int stage,EnemyCreateModelData data)
	{
		this.id = data.id;
		this.enemyId = data.enemyId;
		initTimeMin = data.initTimeMin;
		initTimeMax = data.initTimeMax;
		if (data.stageMax - data.stageMin > 0) {
			termMin = data.termMin + ((data.stageMaxTermMin - data.termMin) / (data.stageMax - data.stageMin)) * stage;
			termMax = data.termMax + ((data.stageMaxTermMax - data.termMax) / (data.stageMax - data.stageMin)) * stage;
		} else {
			termMin = data.termMin;
			termMax = data.termMax;
		}
	}
}
*/

public enum AttackType
{
	MELEE = 0,
	RANGE = 1,
}

public enum AttackTarget
{
	ONE = 0,
	RANGE = 1,
}

public enum CharacterCondition
{
	NORMAL = 0,
	STUN = 1,
	FREEZE = 2,
}

public enum CharacterState
{
	IDLE = 0,
	WALK = 1,
	ATTACK = 2,
	HIT = 3,
	KNOCKBACK = 4,
	DIE = 5,
}

[System.Serializable]
public class CharacterData
{
	public int id;
	public string name;
	public AttackTarget atkTarget;
	public int atk;
	public int maxHp;
	public float moveSpeed;
	public float atkCool;
	public int knockBackDamage;
	public float initForceX;
	public float initForceY;
	public float atkForceX;
	public float atkForceY;
	public float finishForceX;
	public float finishForceY;
	public float createCool;
	public int costCreate;
	public int costCoin;
    public int costdefUpgrade;
	public float costUpgrade;
	public float upgradeAtk;
	public float upgradeHp;
	public int killMoney;
	public int killCoin;
    public float upgradeKillCoin;
}
[System.Serializable]
public class CharacterBattleData
{
	public int id;
	public string name;
	public int cost;
	public AttackTarget atkTarget;
	public int atk;
	public int maxHp;
	public float moveSpeed;
	public float atkCool;
	public int knockBackDamage;
	public Vector2 initForce;
	public Vector2 atkForce;
	public Vector2 finishForce;
	public float createCool;
	public int killMoney;
	public int killCoin;
	public int level;
    public int grade;
    public int cards;
}

public class CharacterMainData
{
	public string name;
	public bool has;
	public int cost;
	public int atk;
	public int maxHp;
}

public enum FaceDirection
{
	LEFT = 0,
	RIGHT = 1,
}

