﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextManager : Singleton<TextManager>
{
	protected override void Awake()
	{
		base.Awake ();
		DontDestroyOnLoad (this);
		LoadText ();
	}
	public Locale locale;
	private static Dictionary<string, string> textData = new Dictionary<string, string>();
	private void LoadText()
	{
		textData.Clear ();
		TextModelData textmodel = Newtonsoft.Json.JsonConvert.DeserializeObject<TextModelData> (Resources.Load("Data/Text").ToString ());
		for (int i = 0; i < textmodel.text.Length; i++) {
			switch (locale) {
			case Locale.KOR: 
				textData.Add (textmodel.text [i].key, textmodel.text [i].kor);
				break;
			default : 
				textData.Add (textmodel.text [i].key, textmodel.text [i].en);
				break;
			}
		}
	}

	public void ChangeLocale(Locale locale)
	{
		if (this.locale == locale)
			return;
		this.locale = locale;
		LoadText ();
	}

	private Font fontEnObj;
	public Font fontEn
	{
		get {
			if (fontEnObj) {
				fontEnObj = Resources.Load ("Font/CartoonShout") as Font;
			}
			return fontEnObj;
		}
	}
	private Font fontUniObj;
	public Font fontUni
	{
		get {
			if (fontUniObj) {
				fontUniObj = Resources.Load ("Font/CartoonShout") as Font;
			}
			return fontUniObj;
		}
	}

	public static string Get(string key)
	{
		if (textData.ContainsKey (key)) {
			
			if (!string.IsNullOrEmpty (textData [key])) {
				return textData [key];
			}
		}
		return key;
	}
}

public class TextKey
{
	public const string title = "title";
	public const string heroes = "heroes";
	public const string lab = "lab";
	public const string shop = "shop";
	public const string start = "start";
	public const string not_enough_coin = "not_enough_coin";
	public const string max_level = "max_level";
	public const string no_battle_character = "no_battle_character";
	public const string no_saved_data = "no_saved_data";
	public const string connection_failed = "connection_failed";
	public const string success_save_data = "success_save_data";
	public const string fail_save_data = "fail_save_data";
	public const string success_rewarded_advertise = "success_rewarded_advertise";
	public const string purchase = "purchase";
	public const string success_purchase = "success_purchase";
	public const string failed_purchase = "failed_purchase";
	public const string no_stage = "no_stage";
	public const string quit = "quit";
	public const string character_max_level = "character_max_level";
	public const string no_network = "no_network";

    public const string character_upgrade = "character_upgrade";
    public const string character_levelup = "character_levelup";
    public const string main_ready = "main_ready";
    public const string main_explore = "main_explore";
    public const string main_tab_character = "main_tab_character";
    public const string main_tab_lab = "main_tab_lab";
    public const string main_tab_shop = "main_tab_shop";
    public const string buy = "buy";

    public const string not_enough_cards = "not_enough_cards";
    public const string explore_area_name = "explore_area_name";
    public const string explore_time = "explore_time";
    public const string hour = "hour";
    public const string explore_reward = "explore_reward";
    public const string ask_explore = "ask_explore";
    public const string explore_complete = "explore_complete";
    public const string exploring = "exploring";
    public const string remain_time = "remain_time";
    public const string get_reward = "get_reward";

    public const string roulette_push_button_message = "roulette_push_button_message";
    public const string roulette_reward_message = "roulette_reward_message";

    public const string explore_area_unlock_condition = "explore_area_unlock_condition";
}

public enum Locale
{
	EN = 0,
	KOR = 1,
}

public class TextModelData
{
	public TextData[] text;
}

public class TextData
{
	public string key;
	public string en;
	public string kor;
}

