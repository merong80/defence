﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PurchaseManager : Singleton<PurchaseManager>
{
    [System.Serializable]
    public class IAPData
    {
        public string id;
        public int coin;
    }

	protected override void Awake ()
	{
		base.Awake ();
		DontDestroyOnLoad (this.gameObject);
		Init ();
	}

    [SerializeField]
    private List<IAPData> shopData = new List<IAPData>();
    private Dictionary<string, int> shopDataMap = new Dictionary<string, int>();
	public int GetCoinAmount(string sku)
	{
		if (shopDataMap.ContainsKey (sku))
			return shopDataMap [sku];

		return 0;
	}
    //--------------------------------------
    //  INITIALIZE
    //--------------------------------------
    public void Init()
    {
		AndroidInAppPurchaseManager.Client.AddProduct ("coins1");
		AndroidInAppPurchaseManager.Client.AddProduct ("coins2");
		AndroidInAppPurchaseManager.Client.AddProduct ("coins3");
		AndroidInAppPurchaseManager.Client.AddProduct ("coins4");
		AndroidInAppPurchaseManager.Client.AddProduct ("coins5");

        AndroidInAppPurchaseManager.ActionProductPurchased += OnProductPurchased;
        AndroidInAppPurchaseManager.ActionProductConsumed += OnProductConsumed;
        AndroidInAppPurchaseManager.ActionBillingSetupFinished += OnBillingConnected;
        
        for (int i = 0; i < shopData.Count; i++)
        {
            if (!shopDataMap.ContainsKey(shopData[i].id)) shopDataMap.Add(shopData[i].id, shopData[i].coin);
        }

		//you may use loadStore function without parametr if you have filled base64EncodedPublicKey in plugin settings
		AndroidInAppPurchaseManager.Client.Connect();
    }

	private bool connectShop = false;
	public void ConnectShop()
	{
		connectShop = true;
		PopupManager.Instance.Loading ();
		AndroidInAppPurchaseManager.Client.Connect ();
	}

    
    //--------------------------------------
    //  PUBLIC METHODS
    //--------------------------------------


    public void Purchase(string SKU)
    {
        AndroidInAppPurchaseManager.Client.Purchase(SKU);
    }

    public void Consume(string SKU)
    {
        AndroidInAppPurchaseManager.Client.Consume(SKU);
    }

    //--------------------------------------
    //  EVENTS
    //--------------------------------------

    private void OnProcessingPurchasedProduct(GooglePurchaseTemplate purchase)
    {
        //some stuff for processing product purchse. Add coins, unlock track, etc
        Consume(purchase.SKU);
    }

    private void OnProcessingConsumeProduct(GooglePurchaseTemplate purchase)
    {
		if (shopDataMap.ContainsKey (purchase.SKU)) {
			GameManager.Instance.ChargeCoin (shopDataMap [purchase.SKU]);
			PopupManager.Instance.Message(string.Format(TextManager.Get(TextKey.success_purchase),shopDataMap[purchase.SKU]));
		}
    }

    private void OnProductPurchased(BillingResult result)
    {

        //this flag will tell you if purchase is available
        //result.isSuccess


        //infomation about purchase stored here
        //result.purchase

        //here is how for example you can get product SKU
        //result.purchase.SKU


        if (result.IsSuccess)
        {
            OnProcessingPurchasedProduct(result.Purchase);
        }
        else
        {
            //AndroidMessage.Create("Product Purchase Failed", result.Response.ToString() + " " + result.Message);
            //PopupManager.Instance.Message(result.Response.ToString() + " " + result.Message);
			PopupManager.Instance.Message(TextManager.Get(TextKey.failed_purchase));
        }

        //Debug.Log("Purchased Responce: " + result.Response.ToString() + " " + result.Message);
    }


    private void OnProductConsumed(BillingResult result)
    {

        if (result.IsSuccess)
        {
            OnProcessingConsumeProduct(result.Purchase);
        }
        else
        {
            //AndroidMessage.Create("Product Cousume Failed", result.Response.ToString() + " " + result.Message);
			PopupManager.Instance.Message(TextManager.Get(TextKey.failed_purchase));
        }

        Debug.Log("Cousume Responce: " + result.Response.ToString() + " " + result.Message);
    }


    private void OnBillingConnected(BillingResult result)
    {

        AndroidInAppPurchaseManager.ActionBillingSetupFinished -= OnBillingConnected;


        if (result.IsSuccess)
        {
            //Store connection is Successful. Next we loading product and customer purchasing details
            AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetrieveProductsFinised;
            AndroidInAppPurchaseManager.Client.RetrieveProducDetails();
			if (connectShop) {
				connectShop = false;
				MainManager.Instance.ClickTab (MainTabType.SHOP);
				PopupManager.Instance.CloseLoading ();
			}
        }

        //AndroidMessage.Create("Connection Responce", result.Response.ToString() + " " + result.Message);
        //Debug.Log("Connection Responce: " + result.Response.ToString() + " " + result.Message);
    }




    private void OnRetrieveProductsFinised(BillingResult result)
    {
        AndroidInAppPurchaseManager.ActionRetrieveProducsFinished -= OnRetrieveProductsFinised;
        if (result.IsSuccess)
        {
            UpdateStoreData();
        }
        else
        {
            //AndroidMessage.Create("Connection Responce", result.Response.ToString() + " " + result.Message);
        }
    }

	public List<GoogleProductTemplate> GetStoreItems()
	{
		List<GoogleProductTemplate> list = new List<GoogleProductTemplate> ();
		for (int i = 0; i < shopData.Count; i++) {
			list.Add(AndroidInAppPurchaseManager.Client.Inventory.GetProductDetails (shopData [i].id));
		}
		return list;
	}

    private void UpdateStoreData()
    {
		foreach (GoogleProductTemplate p in AndroidInAppPurchaseManager.Client.Inventory.Products)
        {
            Debug.Log("Loaded product: " + p.Title);
        }
        foreach(KeyValuePair<string,int> pair in shopDataMap)
        {
            if (AndroidInAppPurchaseManager.Client.Inventory.IsProductPurchased(pair.Key))
            {
                Consume(pair.Key);
            }
        }
        //chisking if we already own some consuamble product but forget to consume those
        

        //Check if non-consumable rpduct was purchased, but we do not have local data for it.
        //It can heppens if game was reinstalled or download on oher device
        //This is replacment for restore purchase fnunctionality on IOS


        /*if (AndroidInAppPurchaseManager.Client.Inventory.IsProductPurchased(COINS_BOOST))
        {
            GameDataExample.EnableCoinsBoost();
        }*/


    }
}
