﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : Singleton<SoundManager>
{
	[SerializeField]
	private AudioListener audioListener;
	[SerializeField]
	private AudioSource bgmSource;
	[SerializeField]
	private AudioSource effectSource;
    [SerializeField]
    private AudioClip mainBgmClip;
    [SerializeField]
    private AudioClip[] battleBgmClips;
	[SerializeField]
	private AudioClip battleWinClip;
	[SerializeField]
	private AudioClip battleLoseClip;
    [SerializeField]
	private AudioClip clickClip;
	//[SerializeField]
	private Dictionary<string, AudioClip> clipMap = new Dictionary<string,AudioClip>();
    private bool mute = false;

    private List<AudioSource> effectSources = new List<AudioSource>();

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(this.gameObject);
        if( !PlayerPrefs.HasKey(PrefsKey.BGM) )
        {
            PlayerPrefs.SetInt(PrefsKey.BGM,0);
        }
        mute = PlayerPrefs.GetInt(PrefsKey.BGM) != 0;
    }
    
    public bool GetMute()
    {
        return mute;
    }

    public void Mute()
    {
        Mute(!mute);
    }

    public void Mute(bool mute)
    {
        this.mute = mute;
        //audioListener.enabled = !this.mute;
        
        PlayerPrefs.SetInt(PrefsKey.BGM, this.mute ? 1 : 0);
        if (!mute)
            bgmSource.Play();
        else
            bgmSource.Stop();

		if( mute )
			effectSource.Stop ();
		
        List<int> removeIdx = new List<int>();
        for( int i = 0; i < effectSources.Count; i++ )
        {
            if (effectSources[i] != null) effectSources[i].enabled = !mute;
            else removeIdx.Add(i);
        }
        for( int i = removeIdx.Count-1; i >= 0; i-- )
        {
            effectSources.RemoveAt(removeIdx[i]);
        }
    }

    public void PlayMainBGM()
	{
        
        bgmSource.clip = mainBgmClip;
        if (mute) return;
		bgmSource.loop = true;
        bgmSource.Play();
    }

    public void PlayBattleBGM()
    {
        
        bgmSource.clip = battleBgmClips[Random.Range(0, battleBgmClips.Length)];
        if (mute) return;
		bgmSource.loop = true;
        bgmSource.Play();
    }

	public void PlayWinBGM()
	{
		if (mute)
			return;

		bgmSource.clip = battleWinClip;
		bgmSource.loop = false;
		bgmSource.Play ();
	}

	public void PlayLoseBGM()
	{
		if (mute)
			return;
		
		bgmSource.clip = battleLoseClip;
		bgmSource.loop = false;
		bgmSource.Play ();
	}

	public void PlayClick(GameObject go)
	{
        if (mute) return;
		AudioSource source = go.GetComponent<AudioSource> ();
		if (source == null) {
			source = go.AddComponent<AudioSource> ();
		}
		source.PlayOneShot (clickClip);
        if (!effectSources.Contains(source)) effectSources.Add(source);
    }
	private const string CLIP_PATH = "Sound/Effect/";
	public void PlayEffect(GameObject go, string path)
	{
        if (mute) return;

		if (!clipMap.ContainsKey (path)) {
			clipMap.Add(path,Resources.Load<AudioClip>(CLIP_PATH+path));
		}

		if (go == null) {
			effectSource.PlayOneShot (clipMap [path]);
		} else {
			AudioSource source = go.GetComponent<AudioSource> ();
			if (source == null) {
				source = go.AddComponent<AudioSource> ();
			}
			source.PlayOneShot (clipMap [path]);
			if (!effectSources.Contains(source)) effectSources.Add(source);
		}
	}

	public void PlayEffect(string path)
	{
		PlayEffect (null, path);
	}

}

